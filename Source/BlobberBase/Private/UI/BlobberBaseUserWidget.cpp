// Fill out your copyright notice in the Description page of Project Settings.

#include "BlobberBaseUserWidget.h"

#include "BlobberGameInstance.h"
#include "Character/CharacterSheet.h"
#include "Character/Inventory.h"
#include "Character/Party.h"
#include "Character/PartyMember.h"
#include "Manager/TimeManager.h"
#include "Util/Macros.h"

void UBlobberBaseUserWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (!bInitializedEvents) {
		UBlobberGameInstance* Instance = UBlobberGameInstance::GetBlobberGameInstance();
		if (bAutoconnectTimeManagerEvents) {
			ConnectTimeManagerEvents(Instance->GetTimeManager());
		}
		if (bAutoconnectPlayerPartyEvents) {
			ConnectPartyEvents(Instance->GetParty());
		}
		if (bAutoconnectPlayerPartyMemberEvents) {
			for (UPartyMember* PartyMember : Instance->GetParty()->GetPartyMembers()) {
				ConnectPartyMemberEvents(PartyMember);
			}
		}
		if (bAutoconnectPlayerCharacterSheetEvents) {
			for (UPartyMember* PartyMember : Instance->GetParty()->GetPartyMembers()) {
				ConnectCharacterSheetEvents(PartyMember->GetCharacterSheet());
			}
		}
		if (bAutoconnectPlayerInventoryEvents) {
			for (UPartyMember* PartyMember : Instance->GetParty()->GetPartyMembers()) {
				ConnectInventoryEvents(PartyMember->GetCharacterSheet()->GetInventory());
			}
		}
		bInitializedEvents = true;
	}
}

void UBlobberBaseUserWidget::ConnectTimeManagerEvents(UTimeManager* TimeManager)
{
	if (TimeManager) {
		TimeManager->TimeChangedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnTimeChanged);
		TimeManager->TimerTickedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnTimerTicked);
		TimeManager->TimeUnitAdvancedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnTimeUnitAdvanced);
	} else {
		LOG("UBlobberBaseUserWidget::ConnectTimeManagerEvents: Null TimeManager")
	}
}

void UBlobberBaseUserWidget::ConnectPartyEvents(UParty* Party)
{
	if (Party) {
		Party->GoldChangedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnGoldChanged);
		Party->ActivePartyMemberIndexChangedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnActivePartyMemberIndexChanged);
		Party->PartyStatusBeginDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnPartyStatusBegin);
		Party->PartyStatusUpdatedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnPartyStatusUpdated);
		Party->PartyStatusEndDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnPartyStatusEnd);
	} else {
		LOG("UBlobberBaseUserWidget::ConnectPartyEvents: Null Party")
	}
}

void UBlobberBaseUserWidget::ConnectPartyMemberEvents(UPartyMember* PartyMember)
{
	if (PartyMember) {
		PartyMember->LevelUpDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnLevelUp);
		PartyMember->ClassChangedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnCharacterClassChanged);
		PartyMember->ExperienceChangedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnExperienceChanged);
		PartyMember->StatPointsChangedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnStatPointsChanged);
		PartyMember->SkillPointsChangedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnSkillPointsChanged);
	} else {
		LOG("UBlobberBaseUserWidget::ConnectPartyMemberEvents: Null PartyMember")
	}
}

void UBlobberBaseUserWidget::ConnectCharacterSheetEvents(UCharacterSheet* CharacterSheet)
{
	if (CharacterSheet) {
		CharacterSheet->ResourceChangedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnResourceChanged);
		CharacterSheet->BaseMaxResourceChangedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnBaseMaxResourceChanged);
		CharacterSheet->BaseStatChangedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnBaseStatChanged);
		CharacterSheet->BaseSkillLevelChangedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnBaseSkillLevelChanged);
		CharacterSheet->BaseSkillMasteryChangedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnBaseSkillMasteryChanged);
		CharacterSheet->StatusBeginDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnStatusBegin);
		CharacterSheet->StatusUpdatedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnStatusUpdated);
		CharacterSheet->StatusEndDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnStatusEnd);
		CharacterSheet->EquipmentChangedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnEquipmentChanged);
	} else {
		LOG("UBlobberBaseUserWidget::ConnectCharacterSheetEvents: Null CharacterSheet")
	}
}

void UBlobberBaseUserWidget::ConnectInventoryEvents(UInventory* Inventory)
{
	if (Inventory) {
		Inventory->InventoryChangedDelegate.AddDynamic(this, &UBlobberBaseUserWidget::OnInventoryChanged);
	} else {
		LOG("UBlobberBaseUserWidget::ConnectInventoryEvents: Null Inventory")
	}
}
