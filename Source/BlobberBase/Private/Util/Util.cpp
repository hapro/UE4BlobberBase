// Fill out your copyright notice in the Description page of Project Settings.

#include "Util.h"

int32 UUtil::RollDice(int32 NumDice, int32 DiceSides)
{
	int32 Total = 0;
	for (int32 i = 0; i < NumDice; ++i) {
		Total += FMath::RandRange(1, DiceSides);
	}
	return Total;
}
