// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Data/Damage.h"
#include "Data/StatusEffect.h"
#include "Item/Item.h"
#include "UnitTests.generated.h"

class UBlobberConfiguration;
class UBlobberGameInstance;
class UCharacterClass;
class UDamageElement;
class UDamageStyle;
class UEquipItemType;
class UEquipSlot;
class UMasteryLevel;
class UParty;
class UPartyMember;
class UResource;
class USkill;
class UStat;
class UTimeManager;

UCLASS()
class BLOBBERBASE_API UnitTests : public UObject
{
	GENERATED_BODY()

public:
	UnitTests();

	void TestAll();

	void TestStartup();
	void TestLevelUp();
	void TestTime();
	void TestInventory();
	void TestCombat();

private:
	UBlobberGameInstance* GameInstance = nullptr;
	UBlobberConfiguration* Config = nullptr;

	UPROPERTY()
	UCharacterClass* Adventurer = nullptr;
	UPROPERTY()
	UCharacterClass* Hero = nullptr;

	UPROPERTY()
	UStat* Strength = nullptr;
	UPROPERTY()
	UStat* Dexterity = nullptr;
	UPROPERTY()
	UStat* ResistPhys = nullptr;

	UPROPERTY()
	USkill* Alchemy = nullptr;
	UPROPERTY()
	USkill* Chain = nullptr;
	UPROPERTY()
	USkill* Sword = nullptr;

	UPROPERTY()
	UMasteryLevel* Unlearned = nullptr;
	UPROPERTY()
	UMasteryLevel* Basic = nullptr;
	UPROPERTY()
	UMasteryLevel* Master = nullptr;

	UPROPERTY()
	TSubclassOf<UStatusEffect> Poison = nullptr;
	UPROPERTY()
	TSubclassOf<UStatusEffect> Heroism = nullptr;
	UPROPERTY()
	TSubclassOf<UStatusEffect> PhysShield = nullptr;
	UPROPERTY()
	TSubclassOf<UStatusEffect> ReturnDamage = nullptr;

	UPROPERTY()
	UDamageElement* PhysicalElement = nullptr;
	UPROPERTY()
	UDamageElement* PoisonElement = nullptr;

	UPROPERTY()
	UResource* Health = nullptr;
	UPROPERTY()
	UResource* Mana = nullptr;

	UPROPERTY()
	TSubclassOf<UDamageInstance> DamageInstanceBP = nullptr;

	UPROPERTY()
	UDamageStyle* Melee = nullptr;
	UPROPERTY()
	UDamageStyle* Other = nullptr;

	UPROPERTY()
	TSubclassOf<UItem> Weapon = nullptr;

	UPROPERTY()
	UEquipItemType* HeadItem = nullptr;
	UPROPERTY()
	UEquipItemType* RingItem = nullptr;
	UPROPERTY()
	UEquipItemType* SwordItem = nullptr;

	UPROPERTY()
	UEquipSlot* HeadSlot = nullptr;
	UPROPERTY()
	UEquipSlot* MainHandSlot = nullptr;
	UPROPERTY()
	UEquipSlot* OffHandSlot = nullptr;
	UPROPERTY()
	UEquipSlot* FingerSlot = nullptr;
	UPROPERTY()
	UEquipSlot* Finger2Slot = nullptr;
	UPROPERTY()
	UEquipSlot* Finger3Slot = nullptr;

	UTimeManager* Time = nullptr;

	UParty* Party = nullptr;
	TArray<UPartyMember*> PartyMembers;
	UPartyMember* P = nullptr;
	UPartyMember* P2 = nullptr;
	UPartyMember* P3 = nullptr;
	UPartyMember* P4 = nullptr;
};
