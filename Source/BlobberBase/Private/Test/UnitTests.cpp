// Fill out your copyright notice in the Description page of Project Settings.

#include "UnitTests.h"

#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"

#include "BlobberConfiguration.h"
#include "BlobberGameInstance.h"
#include "Character/Inventory.h"
#include "Character/Party.h"
#include "Character/PartyMember.h"
#include "Character/CharacterSheet.h"
#include "Data/CharacterClass.h"
#include "Data/DamageElement.h"
#include "Data/DamageStyle.h"
#include "Data/EquipItemType.h"
#include "Data/EquipSlot.h"
#include "Data/MasteryLevel.h"
#include "Data/Resource.h"
#include "Data/Skill.h"
#include "Data/Stat.h"
#include "Item/Item.h"
#include "Manager/TimeManager.h"

UnitTests::UnitTests()	
	: Super()
{
	GameInstance = UBlobberGameInstance::GetBlobberGameInstance();
	if (GameInstance) {
		Config = GameInstance->GetBlobberConfiguration();

		if (Config) {
			static ConstructorHelpers::FObjectFinder<UCharacterClass> AdventurerFinder(TEXT("CharacterClass'/Game/UnitTest/GameInfo/Classes/Adventurer.Adventurer'"));
			verify((Adventurer = AdventurerFinder.Object) != nullptr);
			static ConstructorHelpers::FObjectFinder<UCharacterClass> HeroFinder(TEXT("CharacterClass'/Game/UnitTest/GameInfo/Classes/Hero.Hero'"));
			verify((Hero = HeroFinder.Object) != nullptr);

			static ConstructorHelpers::FObjectFinder<UStat> StrengthFinder(TEXT("Stat'/Game/UnitTest/GameInfo/Stats/Strength.Strength'"));
			verify((Strength = StrengthFinder.Object) != nullptr);
			static ConstructorHelpers::FObjectFinder<UStat> DexterityFinder(TEXT("Stat'/Game/UnitTest/GameInfo/Stats/Dexterity.Dexterity'"));
			verify((Dexterity = DexterityFinder.Object) != nullptr);
			static ConstructorHelpers::FObjectFinder<UStat> ResistPhysFinder(TEXT("Stat'/Game/UnitTest/GameInfo/Stats/ResistPhysical.ResistPhysical'"));
			verify((ResistPhys = ResistPhysFinder.Object) != nullptr);

			static ConstructorHelpers::FObjectFinder<USkill> AlchemyFinder(TEXT("Skill'/Game/UnitTest/GameInfo/Skills/Alchemy.Alchemy'"));
			verify((Alchemy = AlchemyFinder.Object) != nullptr);
			static ConstructorHelpers::FObjectFinder<USkill> ChainFinder(TEXT("Skill'/Game/UnitTest/GameInfo/Skills/Chain.Chain'"));
			verify((Chain = ChainFinder.Object) != nullptr);
			static ConstructorHelpers::FObjectFinder<USkill> SwordFinder(TEXT("Skill'/Game/UnitTest/GameInfo/Skills/Sword.Sword'"));
			verify((Sword = SwordFinder.Object) != nullptr);

			verify((Unlearned = Config->UnlearnedMasteryLevel) != nullptr);
			verify((Basic = Config->BasicMasteryLevel) != nullptr);
			static ConstructorHelpers::FObjectFinder<UMasteryLevel> MasterFinder(TEXT("MasteryLevel'/Game/UnitTest/GameInfo/MasteryLevels/Master.Master'"));
			verify((Master = MasterFinder.Object) != nullptr);

			static ConstructorHelpers::FClassFinder<UStatusEffect> HeroismFinder(TEXT("Blueprint'/Game/UnitTest/GameInfo/StatusEffects/Heroism.Heroism_C'"));
			verify((Heroism = HeroismFinder.Class) != nullptr);
			static ConstructorHelpers::FClassFinder<UStatusEffect> PoisonFinder(TEXT("Blueprint'/Game/UnitTest/GameInfo/StatusEffects/Poison.Poison_C'"));
			verify((Poison = PoisonFinder.Class) != nullptr);
			static ConstructorHelpers::FClassFinder<UStatusEffect> PhysShieldFinder(TEXT("Blueprint'/Game/UnitTest/GameInfo/StatusEffects/PhysShield.PhysShield_C'"));
			verify((PhysShield = PhysShieldFinder.Class) != nullptr);
			static ConstructorHelpers::FClassFinder<UStatusEffect> ReturnDamageFinder(TEXT("Blueprint'/Game/UnitTest/GameInfo/StatusEffects/ReturnDamage.ReturnDamage_C'"));
			verify((ReturnDamage = ReturnDamageFinder.Class) != nullptr);

			static ConstructorHelpers::FObjectFinder<UDamageElement> PhysicalElementFinder(TEXT("DamageElement'/Game/UnitTest/GameInfo/DamageElements/Physical.Physical'"));
			verify((PhysicalElement = PhysicalElementFinder.Object) != nullptr);
			static ConstructorHelpers::FObjectFinder<UDamageElement> PoisonElementFinder(TEXT("DamageElement'/Game/UnitTest/GameInfo/DamageElements/Poison.Poison'"));
			verify((PoisonElement = PoisonElementFinder.Object) != nullptr);

			static ConstructorHelpers::FObjectFinder<UResource> HealthFinder(TEXT("Resource'/Game/UnitTest/GameInfo/Resources/Health.Health'"));
			verify((Health = HealthFinder.Object) != nullptr);
			static ConstructorHelpers::FObjectFinder<UResource> ManaFinder(TEXT("Resource'/Game/UnitTest/GameInfo/Resources/Mana.Mana'"));
			verify((Mana = ManaFinder.Object) != nullptr);

			static ConstructorHelpers::FObjectFinder<UDamageStyle> MeleeFinder(TEXT("DamageStyle'/Game/UnitTest/GameInfo/DamageStyles/Melee.Melee'"));
			verify((Melee = MeleeFinder.Object) != nullptr);
			static ConstructorHelpers::FObjectFinder<UDamageStyle> OtherFinder(TEXT("DamageStyle'/Game/UnitTest/GameInfo/DamageStyles/Other.Other'"));
			verify((Other = OtherFinder.Object) != nullptr);

			static ConstructorHelpers::FClassFinder<UDamageInstance> DamageInstanceBPFinder(TEXT("Blueprint'/Game/UnitTest/Blueprints/Character/BP_DamageInstance.BP_DamageInstance_C'"));
			verify((DamageInstanceBP = DamageInstanceBPFinder.Class) != nullptr);

			static ConstructorHelpers::FClassFinder<UItem> WeaponFinder(TEXT("Blueprint'/Game/UnitTest/Blueprints/Item/BP_Weapon.BP_Weapon_C'"));
			verify((Weapon = WeaponFinder.Class) != nullptr);

			static ConstructorHelpers::FObjectFinder<UEquipItemType> HeadItemFinder(TEXT("EquipItemType'/Game/UnitTest/GameInfo/EquipItemTypes/Head.Head'"));
			verify((HeadItem = HeadItemFinder.Object) != nullptr);
			static ConstructorHelpers::FObjectFinder<UEquipItemType> RingItemFinder(TEXT("EquipItemType'/Game/UnitTest/GameInfo/EquipItemTypes/Ring.Ring'"));
			verify((RingItem = RingItemFinder.Object) != nullptr);
			static ConstructorHelpers::FObjectFinder<UEquipItemType> SwordItemFinder(TEXT("EquipItemType'/Game/UnitTest/GameInfo/EquipItemTypes/Sword.Sword'"));
			verify((SwordItem = SwordItemFinder.Object) != nullptr);

			static ConstructorHelpers::FObjectFinder<UEquipSlot> HeadSlotFinder(TEXT("EquipSlot'/Game/UnitTest/GameInfo/EquipSlots/Head.Head'"));
			verify((HeadSlot = HeadSlotFinder.Object) != nullptr);
			static ConstructorHelpers::FObjectFinder<UEquipSlot> MainHandSlotFinder(TEXT("EquipSlot'/Game/UnitTest/GameInfo/EquipSlots/MainHand.MainHand'"));
			verify((MainHandSlot = MainHandSlotFinder.Object) != nullptr);
			static ConstructorHelpers::FObjectFinder<UEquipSlot> OffHandSlotFinder(TEXT("EquipSlot'/Game/UnitTest/GameInfo/EquipSlots/OffHand.OffHand'"));
			verify((OffHandSlot = OffHandSlotFinder.Object) != nullptr);
			static ConstructorHelpers::FObjectFinder<UEquipSlot> FingerSlotFinder(TEXT("EquipSlot'/Game/UnitTest/GameInfo/EquipSlots/Finger.Finger'"));
			verify((FingerSlot = FingerSlotFinder.Object) != nullptr);
			static ConstructorHelpers::FObjectFinder<UEquipSlot> Finger2SlotFinder(TEXT("EquipSlot'/Game/UnitTest/GameInfo/EquipSlots/Finger2.Finger2'"));
			verify((Finger2Slot = Finger2SlotFinder.Object) != nullptr);
			static ConstructorHelpers::FObjectFinder<UEquipSlot> Finger3SlotFinder(TEXT("EquipSlot'/Game/UnitTest/GameInfo/EquipSlots/Finger3.Finger3'"));
			verify((Finger3Slot = Finger3SlotFinder.Object) != nullptr);

			verify((Party = GameInstance->GetParty()) != nullptr);
			PartyMembers = Party->GetPartyMembers();
			verify((P = PartyMembers[0]) != nullptr);
			verify((P2 = PartyMembers[1]) != nullptr);
			verify((P3 = PartyMembers[2]) != nullptr);
			verify((P4 = PartyMembers[3]) != nullptr);

			verify((Time = GameInstance->GetTimeManager()) != nullptr);
		}
	}

	UE_LOG(LogTemp, Warning, TEXT("Initialized UnitTests class"));
}

void UnitTests::TestAll()
{
	UE_LOG(LogTemp, Warning, TEXT("*** Running TestAll ***"));

	TestStartup();
	TestLevelUp();
	TestTime();
	TestInventory();
	TestCombat();

	UE_LOG(LogTemp, Warning, TEXT("*** TestAll finished ***"));
}

void UnitTests::TestStartup()
{
	UE_LOG(LogTemp, Warning, TEXT("Running TestStartup"));

	check(P->GetCharacterClass() == Adventurer);

	check(P->GetCharacterSheet()->GetLevel() == 1);
	check(P->GetCharacterSheet()->GetBaseStat(Strength) == 5);
	check(P->GetCharacterSheet()->GetStat(Strength) == 5);
	check(P->GetCharacterSheet()->GetBaseStat(Dexterity) == 10);
	check(P->GetCharacterSheet()->GetCurrentResource(Health) == P->GetCharacterSheet()->GetMaxResource(Health));
	check(P->GetCharacterSheet()->GetCurrentResource(Mana) == P->GetCharacterSheet()->GetMaxResource(Mana));

	const FCharacterSkillInfo& SwordSkillInfo = P->GetCharacterSheet()->GetBaseSkill(Sword);
	check(SwordSkillInfo.MasteryLevel == Basic);
	check(SwordSkillInfo.SkillLevel == 1);
	const FCharacterSkillInfo& SwordSkillInfo2 = P->GetCharacterSheet()->GetSkill(Sword);
	check(SwordSkillInfo2.MasteryLevel == Basic);
	check(SwordSkillInfo2.SkillLevel == 1);
	check(!P->GetCharacterSheet()->HasBaseSkillLevel(Chain, 1));
	check(!P->GetCharacterSheet()->HasBaseSkillMastery(Alchemy, Basic));

	check(!Party->HasStatusEffect(PhysShield));
	check(!P->GetCharacterSheet()->HasStatusEffect(Poison));

	check(Time->GetCurrentTimeUnit(ETimeUnit::Hours) == 8);
	check(Time->GetCurrentTime()[ETimeUnit::Minutes] == 30);
	check(Time->GetCurrentTime()[ETimeUnit::Days] == 1);
	check(Time->GetNameOfCurrentTime(ETimeUnit::Days).ToString() == TEXT("Day1"));

	verify(!P2->GetCharacterSheet()->GetInventory()->FindItemInInventoryByName(TEXT("Item18")));

	UE_LOG(LogTemp, Warning, TEXT("    TestStartup finished"));
}

void UnitTests::TestLevelUp()
{
	UE_LOG(LogTemp, Warning, TEXT("Running TestLevelUp"));

	P->SetExperience(100);
	check(P->GetCharacterSheet()->GetLevel() == 1);
	Party->GiveExperience(1000, true);
	check(P->GetCharacterSheet()->GetLevel() == 1);
	P->SetExperience(1000);
	check(P->GetCharacterSheet()->GetLevel() == 2);
	check(P->GetSkillPoints() == 2);
	check(P->GetStatPoints() == 4);
	check(P->GetCharacterSheet()->GetCurrentResource(Mana) == 110.0f);

	check(!P->IncreaseStatWithStatPoints(ResistPhys));
	check(P->CanIncreaseStatWithStatPoints(Strength));
	check(P->IncreaseStatWithStatPoints(Strength));
	check(P->IncreaseStatWithStatPoints(Strength));
	check(P->IncreaseStatWithStatPoints(Strength));
	check(P->IncreaseStatWithStatPoints(Strength));
	check(!P->IncreaseStatWithStatPoints(Strength));
	P->SetStatPoints(1);
	check(P->IncreaseStatWithStatPoints(Dexterity));
	check(!P->IncreaseStatWithStatPoints(Dexterity));
	check(P->GetCharacterSheet()->GetBaseStat(Strength) == 10);
	check(P->GetCharacterSheet()->GetBaseStat(Dexterity) == 12);

	check(!P->CanIncreaseSkillWithSkillPoints(Alchemy));
	check(P->CanIncreaseSkillWithSkillPoints(Sword));
	check(!P->IncreaseSkillWithSkillPoints(Alchemy));
	check(P->IncreaseSkillWithSkillPoints(Sword));
	check(!P->IncreaseSkillWithSkillPoints(Sword));
	P->SetSkillPoints(20);
	check(P->IncreaseSkillWithSkillPoints(Sword));
	check(!P->IncreaseSkillMastery(Sword, Master));
	check(P->IncreaseSkillWithSkillPoints(Sword));
	check(P->IncreaseSkillMastery(Sword, Master));
	check(P->GetSkillPoints() == 16);
	check(!P->IncreaseSkillMastery(Alchemy, Master));
	check(P->IncreaseSkillMastery(Alchemy, Basic));
	check(!P->IncreaseSkillMastery(Chain, Basic));

	P->SetExperience(4000+9000+16000);
	check(P->GetCharacterSheet()->GetLevel() == 5);
	check(P->GetCharacterSheet()->GetBaseStat(Strength) == 14);

	check(P->PromoteClass(Hero));
	check(P->GetCharacterSheet()->GetBaseSkill(Chain).SkillLevel == 1);
	check(P->GetCharacterSheet()->GetBaseStat(Dexterity) == 29);
	check(P->IncreaseSkillWithSkillPoints(Alchemy));
	check(P->IncreaseSkillWithSkillPoints(Alchemy));
	check(P->IncreaseSkillWithSkillPoints(Alchemy));
	check(P->IncreaseSkillMastery(Alchemy, Master));

	UE_LOG(LogTemp, Warning, TEXT("    TestLevelUp finished"));
}

void UnitTests::TestTime()
{
	UE_LOG(LogTemp, Warning, TEXT("Running TestTime"));

	check(P->GetCharacterSheet()->AddStatusEffect(Heroism));
	check(P->GetCharacterSheet()->AddStatusEffect(Poison, 120.0f, 10));
	check(Party->AddStatusEffect(PhysShield, 1440.0f, 5));
	check(!Party->AddStatusEffect(Heroism));
	check(!P->GetCharacterSheet()->AddStatusEffect(PhysShield));
	check(P->GetCharacterSheet()->GetStat(Strength) == 21);
	check(P->GetCharacterSheet()->GetStat(ResistPhys) == 15);

	Time->TickTimer(3);
	check(P->GetCharacterSheet()->GetCurrentResource(Health) == P->GetCharacterSheet()->GetMaxResource(Health) - 30.0f);
	check(P->GetCharacterSheet()->GetStatusEffect(Poison)->GetTimeRemaining() == 105.0f);

	Time->AdvanceTime(ETimeUnit::Hours, 1, true);
	check(!P->GetCharacterSheet()->HasStatusEffect(Poison));
	check(P->GetCharacterSheet()->GetConsciousness() == EConsciousness::Dead);
	check(P->GetCharacterSheet()->HasStatusEffect(Heroism));
	check(Time->GetCurrentTimeUnit(ETimeUnit::Hours) == 9)
	Time->AdvanceTime(ETimeUnit::Days, 40, false);
	check(Time->GetCurrentTimeUnit(ETimeUnit::Days) == 5);
	check(Time->GetCurrentTimeUnit(ETimeUnit::Months) == 2);
	check(Time->GetCurrentTimeUnit(ETimeUnit::Weeks) == 2);
	check(Time->GetNameOfCurrentTime(ETimeUnit::Days).ToString() == TEXT("Day5"));
	check(Time->GetDayInMonth() == 11);
	Time->AdvanceTime(ETimeUnit::Years, 2, true);
	check(P->GetCharacterSheet()->HasStatusEffect(Heroism));
	check(!Party->HasStatusEffect(PhysShield));
	check(Time->GetCurrentTimeUnit(ETimeUnit::Years) == 2);

	UE_LOG(LogTemp, Warning, TEXT("    TestTime finished"));
}

void UnitTests::TestInventory()
{
	UE_LOG(LogTemp, Warning, TEXT("Running TestInventory"));

	for (int32 i = 0; i < 20; ++i) {
		UItem* Item = NewObject<UItem>(this);
		Item->Weight = 4.0f;
		Item->ItemName = FString::Printf(TEXT("Item%i"), i);
		Item->ItemDisplayName = FText::FromString(FString::Printf(TEXT("Item%i"), 19 - i));
		check(P2->GetCharacterSheet()->GetInventory()->AddItemToInventory(Item));
		check(P2->GetCharacterSheet()->GetInventory()->FindItemInInventoryByName(FString::Printf(TEXT("Item%i"), i)) != nullptr);
	}
	check(P2->GetCharacterSheet()->GetInventory()->GetInventoryWeight() == 80.0f);
	check(P2->GetCharacterSheet()->GetInventory()->FindItemInInventoryAt(0)->ItemName == TEXT("Item0"));
	P2->GetCharacterSheet()->GetInventory()->SortInventory();
	check(P2->GetCharacterSheet()->GetInventory()->FindItemInInventoryAt(0)->ItemName == TEXT("Item0"));

	UItem* Item = NewObject<UItem>(this);
	Item->Weight = 1.0f;
	check(!P2->GetCharacterSheet()->GetInventory()->AddItemToInventory(Item));
	check(P2->GetCharacterSheet()->GetInventory()->RemoveItemFromInventoryAt(0));
	check(P2->GetCharacterSheet()->GetInventory()->FindItemInInventoryAt(0)->ItemName == TEXT("Item1"));
	check(P2->GetCharacterSheet()->GetInventory()->RemoveItemFromInventory(P2->GetCharacterSheet()->GetInventory()->FindItemInInventoryByName(TEXT("Item1"))));
	check(P2->GetCharacterSheet()->GetInventory()->FindItemInInventoryAt(0)->ItemName == TEXT("Item10"));
	Item->Weight = 100.0f;
	check(!P2->GetCharacterSheet()->GetInventory()->AddItemToInventory(Item));
	Item->Weight = 5.0f;
	check(P2->GetCharacterSheet()->GetInventory()->AddItemToInventory(Item));
	check(P2->GetCharacterSheet()->GetInventory()->GetInventoryWeight() == 77.0f);

	UItem* IronSword = NewObject<UItem>(this, Weapon);
	UEquippableComponent* IronSwordEquippable = UEquippableComponent::GetEquippableComponent(IronSword);
	IronSwordEquippable->ItemType = SwordItem;
	IronSwordEquippable->StatModifiers.Add(Strength, 5);
	IronSwordEquippable->SkillModifiers.Add(Chain, 2);
	IronSwordEquippable->SkillModifiers.Add(Sword, 2);
	UDamageComponent::GetDamageComponent(IronSword)->BaseDamage = 20;
	UDamageComponent::GetDamageComponent(IronSword)->Element = PhysicalElement;
	check(P2->GetCharacterSheet()->EquipItem(MainHandSlot, IronSword) == nullptr);
	check(P2->GetCharacterSheet()->GetStat(Strength) == 10);
	check(P2->GetCharacterSheet()->GetSkill(Chain).SkillLevel == 0);
	check(P2->GetCharacterSheet()->GetSkill(Sword).SkillLevel == 3);
	check(P2->GetCharacterSheet()->EquipItem(MainHandSlot, nullptr) == IronSword);
	check(P2->GetCharacterSheet()->GetStat(Strength) == 5);
	check(P2->GetCharacterSheet()->EquipItem(FingerSlot, IronSword) == IronSword);
	check(P2->GetCharacterSheet()->EquipItem(OffHandSlot, IronSword) == IronSword);
	P2->GetCharacterSheet()->SetBaseSkillMastery(Sword, Master);
	check(P2->GetCharacterSheet()->EquipItem(OffHandSlot, IronSword) == nullptr);
	check(P2->GetCharacterSheet()->GetStat(Strength) == 10);

	TArray<UItem*> Rings;
	for (int32 i = 0; i < 3; ++i) {
		UItem* Ring = NewObject<UItem>(this);
		UEquippableComponent* RingEquippable = Cast<UEquippableComponent>(Ring->AddComponent(UEquippableComponent::StaticClass()));
		RingEquippable->ItemType = RingItem;
		Ring->Weight = 1.0f;
		Ring->ItemName = FString::Printf(TEXT("Ring %i"), i);
		Ring->ItemDisplayName = FText::FromString(Ring->ItemName);
		Rings.Add(Ring);
	}
	UEquippableComponent::GetEquippableComponent(Rings[0])->StatModifiers.Add(Strength, 10);
	UEquippableComponent::GetEquippableComponent(Rings[1])->SkillModifiers.Add(Sword, 1);
	UEquippableComponent::GetEquippableComponent(Rings[2])->StatModifiers.Add(Dexterity, 10);
	check(P2->GetCharacterSheet()->EquipItem(HeadSlot, Rings[0]) == Rings[0]);
	check(P2->GetCharacterSheet()->EquipItem(FingerSlot, Rings[0]) == nullptr);
	check(P2->GetCharacterSheet()->EquipItem(FingerSlot, Rings[1]) == Rings[0]);
	check(P2->GetCharacterSheet()->EquipItem(Finger2Slot, Rings[0]) == nullptr);
	check(P2->GetCharacterSheet()->EquipItem(Finger3Slot, Rings[2]) == nullptr);
	check(P2->GetCharacterSheet()->GetEquippedItem(Finger2Slot) == Rings[0]);
	check(P2->GetCharacterSheet()->GetStat(Strength) == 20);
	check(P2->GetCharacterSheet()->GetSkill(Sword).SkillLevel == 4);
	check(P2->GetCharacterSheet()->GetStat(Dexterity) == 20);

	UE_LOG(LogTemp, Warning, TEXT("    TestInventory finished"));
}

void UnitTests::TestCombat()
{
	UE_LOG(LogTemp, Warning, TEXT("Running TestCombat"));

	UDamage* Damage = UDamage::CreateDamage(nullptr, P2->GetCharacterSheet());
	check(Damage);
	Damage->AddDamageInstance(UDamageInstance::CreateDamageInstance(PhysicalElement, Melee, 20.0f, 1.0f));
	P3->GetCharacterSheet()->TakeDamage(Damage);
	check(P3->GetCharacterSheet()->GetCurrentResource(Health) == 81.0f);
	Damage->Init(nullptr, P2->GetCharacterSheet());
	Damage->AddDamageInstance(UDamageInstance::CreateDamageInstance(PhysicalElement, Melee, 10.0f, 2.0f));
	P3->GetCharacterSheet()->TakeDamage(Damage);
	check(P3->GetCharacterSheet()->GetCurrentResource(Health) == 62.0f);

	UDamageInstance* DamageInstance = NewObject<UDamageInstance>(this, DamageInstanceBP);
	DamageInstance->DamageElement = PoisonElement;
	DamageInstance->DamageStyle = Other;
	DamageInstance->DamageValue = 10.0f;
	Damage->AddDamageInstance(DamageInstance);
	P3->GetCharacterSheet()->AddStatusEffect(ReturnDamage, -1.0f, 10);
	P3->GetCharacterSheet()->TakeDamage(Damage);
	check(P3->GetCharacterSheet()->GetCurrentResource(Health) == 33.0f);
	check(P2->GetCharacterSheet()->GetCurrentResource(Health) == 90.0f);
	Damage->Init(nullptr, P2->GetCharacterSheet());
	Damage->AddDamageInstance(UDamageInstance::CreateDamageInstance(PhysicalElement, Melee, 0.0f, 20.0f));
	P3->GetCharacterSheet()->TakeDamage(Damage);
	check(P3->GetCharacterSheet()->GetCurrentResource(Health) == 33.0f);

	Damage->Init(nullptr, P2->GetCharacterSheet());
	Damage->AddDamageInstance(UStatusInstance::CreateStatusInstance(Poison, 1, 1.0f, 0));
	Damage->AddDamageInstance(UStatusInstance::CreateStatusInstance(Heroism, 1));
	P3->GetCharacterSheet()->TakeDamage(Damage);
	check(!P3->GetCharacterSheet()->HasStatusEffect(Poison));
	check(P3->GetCharacterSheet()->HasStatusEffect(Heroism));

	check((Damage = P2->GetCharacterSheet()->GenerateDamageInstancesForSlot(MainHandSlot, Melee)) != nullptr);
	check(Damage->Instances.Num() == 0);
	P2->GetCharacterSheet()->GenerateDamageInstancesForSlot(OffHandSlot, Melee, Damage);
	check(Damage->Instances.Num() == 2);
	P3->GetCharacterSheet()->TakeDamage(Damage);
	check(P3->GetCharacterSheet()->GetCurrentResource(Health) == 14.0f);
	check(P3->GetCharacterSheet()->HasStatusEffect(Poison));

	UE_LOG(LogTemp, Warning, TEXT("    TestCombat finished"));
}
