// Fill out your copyright notice in the Description page of Project Settings.

#include "BlobberConfiguration.h"

#include "ARFilter.h"
#include "AssetRegistryModule.h"
#include "ModuleManager.h"

#include "Data/MasteryLevel.h"

UBlobberConfiguration::UBlobberConfiguration(const FObjectInitializer& Initializer)
	: Super(Initializer)
{
	CharacterSheetClass = UCharacterSheet::StaticClass();
	PartyMemberClass = UPartyMember::StaticClass();
	PartyClass = UParty::StaticClass();
	InventoryClass = UInventory::StaticClass();
	TimeManagerClass = UTimeManager::StaticClass();

	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(FName("AssetRegistry"));
	IAssetRegistry& AssetRegistry = AssetRegistryModule.Get();
	// Must wait until all assets are discovered before populating list of other GameInfoBase assets.
	if (AssetRegistry.IsLoadingAssets()) {
		AssetRegistry.OnFilesLoaded().AddUObject(this, &UBlobberConfiguration::OnRegistryLoaded);
	} else {
		OnRegistryLoaded();
	}
}

void UBlobberConfiguration::OnRegistryLoaded()
{
	// Find all UMasteryLevel assets and determine what represents "Unlearned" and "Basic".
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(FName("AssetRegistry"));
	IAssetRegistry& AssetRegistry = AssetRegistryModule.Get();

	FARFilter Filter;
	Filter.ClassNames = { TEXT("MasteryLevel") };
	Filter.bRecursiveClasses = true;

	TArray<FAssetData> AssetList;
	AssetRegistry.GetAssets(Filter, AssetList);

	for (const FAssetData& Asset : AssetList) {
		UMasteryLevel* Mastery = Cast<UMasteryLevel>(Asset.GetAsset());
		if (Mastery) {
			if (Mastery->IntVal == -1) {
				if (!UnlearnedMasteryLevel || Mastery->GetName().Compare(UnlearnedMasteryLevel->GetName(), ESearchCase::IgnoreCase) < 0) {
					UnlearnedMasteryLevel = Mastery;
				}
			} else if (Mastery->IntVal == 0) {
				if (!BasicMasteryLevel || Mastery->GetName().Compare(BasicMasteryLevel->GetName(), ESearchCase::IgnoreCase) < 0) {
					BasicMasteryLevel = Mastery;
				}
			}
		}
	}

	// If no valid assets exist, create default MasteryLevels and replace nullptrs with the default ones.
	if (!UnlearnedMasteryLevel) {
		UnlearnedMasteryLevel = NewObject<UMasteryLevel>(this, TEXT("UnlearnedMastery"));
		UnlearnedMasteryLevel->DisplayName = NSLOCTEXT("BlobberConfiguration", "UnlearnedMasteryLevelDisplayName", "Unlearned");
		UnlearnedMasteryLevel->IntVal = -1;
	}

	if (!BasicMasteryLevel) {
		BasicMasteryLevel = NewObject<UMasteryLevel>(this, TEXT("BasicMastery"));
		BasicMasteryLevel->DisplayName = NSLOCTEXT("BlobberConfiguration", "BasicMasteryLevelDisplayName", "Basic");
		BasicMasteryLevel->IntVal = 0;
	}
	
	// Objects get deleted by ObjectInitializer if not set in the CDO. Possible
	// alternative to this hack is to pass in an ObjectInitializer to this
	// function and if set, use it to create default subobjects? Not sure how
	// that will work since we're getting the asset directly from AssetRegistry.
	UBlobberConfiguration* CDO = Cast<UBlobberConfiguration>(GetClass()->GetDefaultObject());
	CDO->UnlearnedMasteryLevel = UnlearnedMasteryLevel;
	CDO->BasicMasteryLevel = BasicMasteryLevel;

	UCharacterSheet::NoSkillInfo.MasteryLevel = UnlearnedMasteryLevel;

	bIsInitialized = true;
	ConfigurationInitializedDelegate.ExecuteIfBound();
}
