// Fill out your copyright notice in the Description page of Project Settings.

#include "TimeManager.h"

#include "BlobberGameInstance.h"
#include "Util/Macros.h"

UTimeManager::UTimeManager(const FObjectInitializer& Initializer)
	: Super(Initializer)
{
	CurrentTime.Add(ETimeUnit::Seconds, 0);
	CurrentTime.Add(ETimeUnit::Minutes, 0);
	CurrentTime.Add(ETimeUnit::Hours, 0);
	CurrentTime.Add(ETimeUnit::Days, 0);
	CurrentTime.Add(ETimeUnit::Weeks, 0);
	CurrentTime.Add(ETimeUnit::Months, 0);
	CurrentTime.Add(ETimeUnit::Years, 0);

	TimeUnitOffset.Add(ETimeUnit::Seconds, 0);
	TimeUnitOffset.Add(ETimeUnit::Minutes, 0);
	TimeUnitOffset.Add(ETimeUnit::Hours, 0);
	TimeUnitOffset.Add(ETimeUnit::Days, 1);
	TimeUnitOffset.Add(ETimeUnit::Weeks, 1);
	TimeUnitOffset.Add(ETimeUnit::Months, 1);
	TimeUnitOffset.Add(ETimeUnit::Years, 0);

	WrapToNextTimeUnit.Add(ETimeUnit::Seconds, 60);
	WrapToNextTimeUnit.Add(ETimeUnit::Minutes, 60);
	WrapToNextTimeUnit.Add(ETimeUnit::Hours, 24);
	WrapToNextTimeUnit.Add(ETimeUnit::Days, 6);
	WrapToNextTimeUnit.Add(ETimeUnit::Weeks, 5);
	WrapToNextTimeUnit.Add(ETimeUnit::Months, 12);

	NamesOfTimeUnitIntervals.Add(ETimeUnit::Days);
	NamesOfTimeUnitIntervals.Add(ETimeUnit::Weeks);
	NamesOfTimeUnitIntervals.Add(ETimeUnit::Months);
	NamesOfTimeUnitIntervals.Add(ETimeUnit::Years);

	TimeUnitsToBroadcastEventFor.Add(ETimeUnit::Seconds, false);
	TimeUnitsToBroadcastEventFor.Add(ETimeUnit::Minutes, false);
	TimeUnitsToBroadcastEventFor.Add(ETimeUnit::Hours, true);
	TimeUnitsToBroadcastEventFor.Add(ETimeUnit::Days, true);
	TimeUnitsToBroadcastEventFor.Add(ETimeUnit::Weeks, true);
	TimeUnitsToBroadcastEventFor.Add(ETimeUnit::Months, true);
	TimeUnitsToBroadcastEventFor.Add(ETimeUnit::Years, true);
}

void UTimeManager::Init(int32 Y, int32 Mon, int32 W, int32 D, int32 H, int32 Min, int32 S, bool bBroadcastEvent)
{
	if (Y < 0 || Mon < 0 || W < 0 || D < 0 || H < 0 || Min < 0 || S < 0) {
		LOG("UTimeManager::Init: Negative values not allowed")
		return;
	}

	CurrentTime[ETimeUnit::Seconds] = S;
	CurrentTime[ETimeUnit::Minutes] = Min;
	CurrentTime[ETimeUnit::Hours] = H;
	CurrentTime[ETimeUnit::Days] = D;
	CurrentTime[ETimeUnit::Weeks] = W;
	CurrentTime[ETimeUnit::Months] = Mon;
	CurrentTime[ETimeUnit::Years] = Y;
	if (bBroadcastEvent) {
		TimeChangedDelegate.Broadcast(this);
	}
}

void UTimeManager::StartTimer()
{
	FTimerManager& TimerManager = UBlobberGameInstance::GetBlobberGameInstance()->GetTimerManager();
	if (!TimerManager.IsTimerActive(TickHandle)) {
		TimerManager.SetTimer(TickHandle, this, &UTimeManager::TickTimerPrivate, RealWorldSecondsPerTimerTick, true);
		OnTimerStarted();
	}
}

void UTimeManager::StopTimer()
{
	FTimerManager& TimerManager = UBlobberGameInstance::GetBlobberGameInstance()->GetTimerManager();
	if (TimerManager.IsTimerActive(TickHandle)) {
		TimerManager.ClearTimer(TickHandle);
		OnTimerStopped();
	}
}

void UTimeManager::TickTimer(int32 NumberOfTimes)
{
	for (int32 i = 0; i < NumberOfTimes; ++i) {
		TickTimerPrivate();
	}
}

void UTimeManager::TickTimerPrivate()
{
	AdvanceTime(UnitIncreasedPerTimerTick, AmountIncreasedPerTimerTick, false);
	TimerTickedDelegate.Broadcast(this, UnitIncreasedPerTimerTick, AmountIncreasedPerTimerTick);
}

void UTimeManager::AdvanceTime(ETimeUnit TimeUnit, int32 Amount, bool bIncrementInTicks)
{
	if (Amount > 0) {
		if (bIncrementInTicks) {
			if (TimeUnit < UnitIncreasedPerTimerTick) {
				// Less than one tick
				AdvanceTime(TimeUnit, Amount, false);
			} else {
				// Convert function parameter units to tick units, then
				// tick as many times as necessary
				while (TimeUnit > UnitIncreasedPerTimerTick) {
					TimeUnit = (ETimeUnit)(((uint8)TimeUnit) - 1);
					Amount *= WrapToNextTimeUnit[TimeUnit];
				}
				int32 NumTicks = Amount / AmountIncreasedPerTimerTick;
				int32 Remainder = Amount % AmountIncreasedPerTimerTick;
				TickTimer(NumTicks);
				// Add any remaining time left that is too small to tick
				AdvanceTime(UnitIncreasedPerTimerTick, Remainder, false);
			}
		} else {
			int32 Remainder = Amount;
			for (ETimeUnit CurrentUnit = TimeUnit; CurrentUnit <= ETimeUnit::Years && Remainder > 0; CurrentUnit = (ETimeUnit)(((uint8)CurrentUnit) + 1)) {
				// Add to current time
				const int32 OldValue = CurrentTime[CurrentUnit];
				CurrentTime[CurrentUnit] += Remainder;
				if (CurrentUnit != ETimeUnit::Years) {
					// Check for overflow
					Remainder = CurrentTime[CurrentUnit] / WrapToNextTimeUnit[CurrentUnit];

					// If there is, subtract the amount overflowed from the current
					// TimeUnit before move to the next one
					CurrentTime[CurrentUnit] -= Remainder * WrapToNextTimeUnit[CurrentUnit];
				}
				if (TimeUnitsToBroadcastEventFor.Contains(CurrentUnit) && TimeUnitsToBroadcastEventFor[CurrentUnit]) {
					TimeUnitAdvancedDelegate.Broadcast(this, CurrentUnit, OldValue, CurrentTime[CurrentUnit]);
				}
			}
			TimeChangedDelegate.Broadcast(this);
		}
	} else if (Amount < 0) {
		LOG("UTimeManager::AdvanceTime: Negative values not allowed")
	}
}

const FText& UTimeManager::GetNameOfCurrentTime(ETimeUnit TimeUnit)
{
	return GetNameOfTimeUnit(TimeUnit, CurrentTime[TimeUnit]);
}

const FText& UTimeManager::GetNameOfTimeUnit(ETimeUnit TimeUnit, int32 Value)
{
	if (NamesOfTimeUnitIntervals.Contains(TimeUnit) && NamesOfTimeUnitIntervals[TimeUnit].Names.Num() >= Value) {
		return NamesOfTimeUnitIntervals[TimeUnit].Names[Value];
	} else {
		static const FText NoText;
		return NoText;
	}
}

TMap<ETimeUnit, int32> UTimeManager::GetCurrentTime() const
{
	TMap<ETimeUnit, int32> ActualTime = CurrentTime;
	for (auto Offset : TimeUnitOffset) {
		ActualTime[Offset.Key] += Offset.Value;
	}
	return ActualTime;
}

int32 UTimeManager::GetDayInMonth() const
{
	return CurrentTime[ETimeUnit::Weeks] * WrapToNextTimeUnit[ETimeUnit::Days] + GetCurrentTimeUnit(ETimeUnit::Days);
}

int32 UTimeManager::GetCurrentTimeUnit(ETimeUnit TimeUnit) const
{
	if (TimeUnitOffset.Contains(TimeUnit)) {
		return CurrentTime[TimeUnit] + TimeUnitOffset[TimeUnit];
	} else {
		return CurrentTime[TimeUnit];
	}
}
