// Fill out your copyright notice in the Description page of Project Settings.

#include "BlobberGameInstance.h"

#include "BlobberConfiguration.h"
#include "Macros.h"
#include "Data/CharacterClass.h"
#include "Manager/TimeManager.h"
#include "Test/UnitTests.h"

UBlobberGameInstance* UBlobberGameInstance::BlobberGameInstance = nullptr;
bool UBlobberGameInstance::bEnableBlobberEvents = true;

UBlobberGameInstance::UBlobberGameInstance()
	: Super()
{
	BlobberGameInstance = this;
	BlobberConfigurationClass = UBlobberConfiguration::StaticClass();
}

void UBlobberGameInstance::Init()
{
	Super::Init();

	if (!BlobberConfigurationClass) {
		LOG("UBlobberGameInstance::Init: BlobberConfigurationClass invalid. Failed to initialize.")
		return;
	}

	BlobberConfiguration = NewObject<UBlobberConfiguration>(this, BlobberConfigurationClass);
	if (BlobberConfiguration->bIsInitialized) {
		OnConfigurationInitialized();
	} else {
		BlobberConfiguration->ConfigurationInitializedDelegate.BindUObject(this, &UBlobberGameInstance::OnConfigurationInitialized);
	}
}

void UBlobberGameInstance::OnConfigurationInitialized()
{
	// Add a short delay after the config is loaded to make sure any other
	// classes waiting on the AssetRegistry have finished processing.
	FTimerHandle Timer;
	GetTimerManager().SetTimer(Timer, this, &UBlobberGameInstance::InitializeGameObjects, 0.05f);
}

void UBlobberGameInstance::InitializeGameObjects()
{
	UCharacterClass::AllClassesSetDefaultMaxSkillMastery(BlobberConfiguration->UnlearnedMasteryLevel);
	TimeManager = NewObject<UTimeManager>(this, BlobberConfiguration->TimeManagerClass);

	if (BlobberConfiguration->PartyClass) {
		Party = NewObject<UParty>(this, BlobberConfiguration->PartyClass);
		Party->InitPrivate();
	} else {
		LOG("UBlobberGameInstance::Init: PartyClass not specified in BlobberConfiguration. Failed to initialize.")
	}

	if (BlobberConfiguration->bRunUnitTests) {
		UnitTests* Tests = NewObject<UnitTests>(this);
		Tests->AddToRoot();
		Tests->TestAll();
		Tests->RemoveFromRoot();
	}

	TimeManager->StartTimer();
}

void UBlobberGameInstance::InitializeLevel()
{
}
