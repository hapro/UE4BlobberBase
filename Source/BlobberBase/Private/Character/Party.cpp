// Fill out your copyright notice in the Description page of Project Settings.

#include "Party.h"

#include "Kismet/GameplayStatics.h"

#include "BlobberGameInstance.h"
#include "BlobberConfiguration.h"
#include "Macros.h"
#include "Character/CharacterSheet.h"
#include "Character/PartyMember.h"
#include "Data/MasteryLevel.h"
#include "Data/Skill.h"
#include "Data/StatusEffect.h"
#include "Manager/TimeManager.h"

UParty::UParty()
	: Super()
{

}

void UParty::InitPrivate()
{
	UBlobberConfiguration* Config = UBlobberGameInstance::GetBlobberGameInstance()->GetBlobberConfiguration();

	if (Config) {
		if (Config->PartyMemberClass) {
			UTimeManager* TimeManager = UBlobberGameInstance::GetBlobberGameInstance()->GetTimeManager();
			if (TimeManager && TimeManager->bTickStatusEffectsOnTimerTick) {
				TimeManager->TimerTickedDelegate.AddDynamic(this, &UParty::TickStatusEffectsFromTimeManager);
			}

			if (Config->PIEDefaultPartyMembers.Num() > 0) {
				for (UCharacterClass* CharacterClass : Config->PIEDefaultPartyMembers) {
					UPartyMember* PartyMember = NewObject<UPartyMember>(this, Config->PartyMemberClass);
					PartyMembers.Add(PartyMember);
					PartyMember->InitPrivate(CharacterClass, this, PartyMembers.Num() - 1);
				}
			} else {
				LOG("UParty::InitPrivate: No PIEDefaultPartyMembers found in BlobberConfiguration.")
			}
			Init();
		} else {
			LOG("UParty::InitPrivate: PartyMemberClass not specified in BlobberConfiguration. Failed to initialize.")
		}
	} else {
		LOG("UParty::InitPrivate: BlobberConfiguration not found. Failed to initialize.")
	}
}

void UParty::SetActivePartyMemberIndex(int32 PartyMember)
{
	if (PartyMember < 0 || PartyMember >= PartyMembers.Num()) {
		LOG("UParty::SetActivePartyMember: Invalid index given")
	} else {
		const int32 OldIndex = ActivePartyMemberIndex;
		ActivePartyMemberIndex = PartyMember;
		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			ActivePartyMemberIndexChangedDelegate.Broadcast(this, OldIndex, ActivePartyMemberIndex);
		}
	}
}

void UParty::SetGold(int32 Amount, EUpdateValueType UpdateType)
{
	const int32 OldValue = Gold;

	switch (UpdateType) {
	default:
	case EUpdateValueType::Set:
		Gold = Amount;
		break;
	case EUpdateValueType::Add:
		Gold += Amount;
		break;
	}

	Gold = FMath::Max(0, Gold);

	if (OldValue != Gold) {
		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			GoldChangedDelegate.Broadcast(this, Gold);
		}
	}
}

void UParty::GiveExperience(int32 Amount, bool bSplitExperience, bool bGiveIfUnconscious)
{
	TArray<UPartyMember*> MembersToGiveExpTo = PartyMembers;
	if (!bGiveIfUnconscious) {
		for (UPartyMember* Member : PartyMembers) {
			if (Member->GetCharacterSheet()->GetConsciousness() != EConsciousness::Alive) {
				MembersToGiveExpTo.Remove(Member);
			}
		}
	}

	if (bSplitExperience && MembersToGiveExpTo.Num() > 0) {
		Amount /= MembersToGiveExpTo.Num();
	}

	for (UPartyMember* Member : MembersToGiveExpTo) {
		Member->SetExperience(Amount);
	}
}

FCharacterSkillInfo UParty::GetActivePartyMemberSkill(USkill* Skill, bool bIncludePartySkills) const
{
	if (Skill) {
		if (Skill->bIsPartySkill && bIncludePartySkills) {
			FCharacterSkillInfo MaxSkillInfo;
			// Party skills return the highest mastery first, and if two members have
			// the same mastery, then the highest skill level is returned.
			for (int32 i = 0; i < PartyMembers.Num(); ++i) {
				const FCharacterSkillInfo& PartyMemberSkill = PartyMembers[i]->GetCharacterSheet()->GetSkill(Skill);
				if (MaxSkillInfo.MasteryLevel->IntVal == PartyMemberSkill.MasteryLevel->IntVal) {
					if (MaxSkillInfo.SkillLevel < PartyMemberSkill.SkillLevel) {
						MaxSkillInfo = PartyMemberSkill;
					}
				} else if (MaxSkillInfo.MasteryLevel->IntVal < PartyMemberSkill.MasteryLevel->IntVal) {
					MaxSkillInfo = PartyMemberSkill;
				}
			}
			return MaxSkillInfo;
		} else {
			return GetActivePartyMember()->GetCharacterSheet()->GetSkill(Skill);
		}
	} else {
		LOG("UParty::GetActivePartyMemberSkill: Null Skill")
	}
	return UCharacterSheet::NoSkillInfo;
}

int32 UParty::GetIndexOfPartyMember(UPartyMember* PartyMember) const
{
	if (!PartyMember) {
		LOG("UParty::GetIndexOfPartyMember: Null PartyMember")
		return -1;
	} else {
		int32 Index = -1;
		PartyMembers.Find(PartyMember, Index);
		return Index;
	}
}

bool UParty::AddStatusEffect(TSubclassOf<UStatusEffect> Status, float Duration, int32 Strength, EStatusOverwriteType Overwrite)
{
	if (!Status) {
		LOG("UParty::AddStatus: Null Status");
		return false;
	}

	if (Duration == 0) {
		LOG("UParty::AddStatus: Can't add status with no duration");
		return false;
	}

	UStatusEffect* ExistingStatus = GetStatusEffect(Status);
	if (ExistingStatus) {
		const bool bNewDurationLonger = Duration >= ExistingStatus->GetTimeRemaining();
		const bool bNewStrengthHigher = Strength >= ExistingStatus->GetStrength();
		bool bShouldOverwrite = false;
		switch (Overwrite) {
		case EStatusOverwriteType::NoOverwrite:
			return false;
		case EStatusOverwriteType::AlwaysOverwrite:
			bShouldOverwrite = true;
			break;
		case EStatusOverwriteType::OverwriteIfDurationEqualOrHigher:
			bShouldOverwrite = bNewDurationLonger;
			break;
		case EStatusOverwriteType::OverwriteIfStrengthEqualOrHigher:
			bShouldOverwrite = bNewStrengthHigher;
			break;
		case EStatusOverwriteType::OverwriteIfStrengthAndDurationEqualOrHigher:
			bShouldOverwrite = bNewDurationLonger && bNewStrengthHigher;
			break;
		}
		if (bShouldOverwrite) {
			ExistingStatus->UpdateStatus(Strength, Duration);
			if (UBlobberGameInstance::BlobberEventsEnabled()) {
				PartyStatusUpdatedDelegate.Broadcast(this, ExistingStatus);
			}
			return true;
		} else {
			return false;
		}
	} else {
		UStatusEffect* NewStatus = NewObject<UStatusEffect>(this, Status);
		if (!NewStatus->bPartyStatus) {
			LOG("UParty::AddStatus: Can't add non-party status to party");
			return false;
		}
		NewStatus->Init(Strength, Duration, nullptr, this);
		StatusEffects.Add(NewStatus);
		NewStatus->OnStatusBegin();
		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			PartyStatusBeginDelegate.Broadcast(this, NewStatus);
		}
		return true;
	}
}

UStatusEffect* UParty::GetStatusEffect(TSubclassOf<UStatusEffect> Status) const
{
	if (!Status) {
		LOG("UParty::GetStatusEffect: Null Status");
		return nullptr;
	}

	for (UStatusEffect* StatusEffect : StatusEffects) {
		if (StatusEffect->GetClass()->IsChildOf(Status)) {
			return StatusEffect;
		}
	}
	return nullptr;
}

bool UParty::HasStatusEffect(TSubclassOf<UStatusEffect> Status) const
{
	if (!Status) {
		LOG("UParty::HasStatus: Null Status");
		return false;
	}

	return GetStatusEffect(Status) != nullptr;
}

void UParty::TickStatusEffects(float DeltaTime)
{
	TArray<UStatusEffect*> ToTick = StatusEffects;
	TArray<UStatusEffect*> ToRemove;
	for (UStatusEffect* Status : ToTick) {
		// The character might die from a status ticking which could remove other
		// status effects, so we must ensure this effect is still valid before
		// processing it.
		if (StatusEffects.Contains(Status)) {
			const bool bIsPermanent = Status->IsPermanent();
			Status->Tick(DeltaTime);
			if (UBlobberGameInstance::BlobberEventsEnabled()) {
				PartyStatusUpdatedDelegate.Broadcast(this, Status);
			}
			if (!bIsPermanent && Status->GetTimeRemaining() <= 0.0f) {
				ToRemove.Add(Status);
			}
		}
	}

	for (UStatusEffect* Status : ToRemove) {
		RemoveStatusEffect(Status);
	}
}

void UParty::TickStatusEffectsFromTimeManager(UTimeManager* TimeManager, ETimeUnit TimeUnit, int32 TimeAdvanced)
{
	TickStatusEffects((float)TimeAdvanced);
}

bool UParty::RemoveStatusEffectFromClass(TSubclassOf<UStatusEffect> Status)
{
	if (!Status) {
		LOG("UParty::RemoveStatusEffectFromClass: Null Status");
		return false;
	}

	UStatusEffect* ExistingStatus = GetStatusEffect(Status);
	if (ExistingStatus) {
		return RemoveStatusEffect(ExistingStatus);
	} else {
		return false;
	}
}

bool UParty::RemoveStatusEffect(UStatusEffect* Status)
{
	if (!Status) {
		LOG("UParty::RemoveStatusEffect: Null Status");
		return false;
	}

	int32 Index = -1;
	if (StatusEffects.Find(Status, Index)) {
		Status->OnStatusEnd();
		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			PartyStatusEndDelegate.Broadcast(this, Status);
		}
		StatusEffects.RemoveAt(Index);
		return true;
	} else {
		return false;
	}
}
