// Fill out your copyright notice in the Description page of Project Settings.

#include "BlobberPlayerController.h"

#include "GameFramework/CharacterMovementComponent.h"

#include "Character/PlayerCharacter.h"

ABlobberPlayerController::ABlobberPlayerController(const FObjectInitializer& Initializer)
	: Super(Initializer)
{

}

void ABlobberPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveForward", this, &ABlobberPlayerController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ABlobberPlayerController::MoveRight);
	InputComponent->BindAxis("TurnRight", this, &ABlobberPlayerController::TurnRight);
	InputComponent->BindAxis("TurnUp", this, &ABlobberPlayerController::TurnUp);

	InputComponent->BindAction("Quit", IE_Pressed, this, &ABlobberPlayerController::ExitGame);
}

void ABlobberPlayerController::MoveForward(float Val)
{
	if (Val != 0.0f) {
		FRotator Rotation = GetControlRotation();
		if (GetCharacter()->GetCharacterMovement()->IsMovingOnGround() || GetCharacter()->GetCharacterMovement()->IsFalling()) {
			Rotation.Pitch = 0.0f;
		}

		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::X);
		GetCharacter()->AddMovementInput(Direction, Val);
	}
}

void ABlobberPlayerController::MoveRight(float Val)
{
	if (Val != 0.0f) {
		FRotator Rotation = GetControlRotation();
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::Y);
		GetCharacter()->AddMovementInput(Direction, Val);
	}
}

void ABlobberPlayerController::TurnUp(float Val)
{
	if (Val != 0.0f) {
		AddPitchInput(Val);
	}
}

void ABlobberPlayerController::TurnRight(float Val)
{
	if (Val != 0.0f) {
		AddYawInput(Val);
	}
}

void ABlobberPlayerController::ExitGame()
{
	FGenericPlatformMisc::RequestExit(false);
}
