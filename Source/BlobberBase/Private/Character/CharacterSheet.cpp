// Fill out your copyright notice in the Description page of Project Settings.

#include "CharacterSheet.h"

#include "BlobberGameInstance.h"
#include "BlobberConfiguration.h"
#include "Character/Inventory.h"
#include "Data/CharacterClass.h"
#include "Data/Damage.h"
#include "Data/DamageElement.h"
#include "Data/EquipItemType.h"
#include "Data/EquipSlot.h"
#include "Data/MasteryLevel.h"
#include "Data/Skill.h"
#include "Data/Stat.h"
#include "Data/StatusEffect.h"
#include "Item/Item.h"
#include "Item/ItemComponent.h"
#include "Manager/TimeManager.h"
#include "Util/Macros.h"

FCharacterSkillInfo UCharacterSheet::NoSkillInfo;

UCharacterSheet::UCharacterSheet(const FObjectInitializer& Initializer)
	: Super(Initializer)
{
	const UBlobberConfiguration* Config = UBlobberGameInstance::GetBlobberGameInstance()->GetBlobberConfiguration();
	if (Config) {
		Inventory = Cast<UInventory>(Initializer.CreateDefaultSubobject(this, TEXT("Inventory"), UInventory::StaticClass(), Config->InventoryClass, true, false, false));
	}
}

void UCharacterSheet::InitPrivate(UCharacterClass* CharacterClass, UPartyMember* InPartyMember)
{
	UTimeManager* TimeManager = UBlobberGameInstance::GetBlobberGameInstance()->GetTimeManager();
	if (TimeManager && TimeManager->bTickStatusEffectsOnTimerTick) {
		TimeManager->TimerTickedDelegate.AddDynamic(this, &UCharacterSheet::TickStatusEffectsFromTimeManager);
	}

	PartyMember = InPartyMember;

	for (const auto& StatInfo : CharacterClass->StatInfo) {
		BaseStats.Add(StatInfo.Key, StatInfo.Value.DefaultValue);
	}

	for (const auto& ResourceInfo : CharacterClass->ResourceInfo) {
		BaseMaxResources.Add(ResourceInfo.Key, ResourceInfo.Value.DefaultValue);
		CurrentResources.Add(ResourceInfo.Key, GetMaxResource(ResourceInfo.Key));
	}

	const UBlobberConfiguration* Config = UBlobberGameInstance::GetBlobberGameInstance()->GetBlobberConfiguration();
	for (USkill* Skill : CharacterClass->StartingSkills) {
		if (Skill) {
			BaseSkills.Add(Skill, FCharacterSkillInfo(1, Config->BasicMasteryLevel));
		} else {
			LOG("UCharacterSheet::InitPrivate: Null starting skill");
		}
	}
	if (!HealthResource) {
		HealthResource = Config->DefaultHealthResource;
	}

	Init();
	// Re-initialize resources after init in case values change
	for (auto& Resource : CurrentResources) {
		Resource.Value = GetMaxResource(Resource.Key);
	}
}

UDamage* UCharacterSheet::GenerateDamageInstancesForSlot_Implementation(UEquipSlot* Slot, UDamageStyle* Style, UDamage* Damage)
{
	if (!Damage) {
		Damage = UDamage::CreateDamage(nullptr, this);
	}

	if (!Slot) {
		LOG("UCharacterSheet::GenerateDamageInstancesForSlot: Null Slot");
		return Damage;
	}
		
	UItem* Item = GetEquippedItem(Slot);
	if (Item) {
		UDamageComponent* DamageComponent = UDamageComponent::GetDamageComponent(Item);
		if (DamageComponent) {
			DamageComponent->GenerateDamageInstances(Damage, Style, this);
		}
	}

	return Damage;
}

void UCharacterSheet::TakeDamage_Implementation(UDamage* Damage)
{
	for (UDamageInstanceBase* Instance : Damage->Instances) {
		Instance->ApplyDamageInstance(this);
	}
}

float UCharacterSheet::MitigateDamage_Implementation(UDamageInstance* Instance) const
{
	float CritDamage = Instance->DamageValue * Instance->Multiplier;
	if (Instance->DamageElement && Instance->DamageElement->ResistedBy) {
		CritDamage *= 1.0f - (float)GetStat(Instance->DamageElement->ResistedBy) / 100.0f;
	}
	return CritDamage;
}

int32 UCharacterSheet::GetBaseStat(UStat* Stat) const
{
	if (Stat) {
		const int32& StatValue = const_cast<UCharacterSheet*>(this)->BaseStats.FindOrAdd(Stat);
		return StatValue;
	} else {
		LOG("UCharacterSheet::GetBaseStat: Null Stat");
		return 0;
	}
}

int32 UCharacterSheet::GetStat_Implementation(UStat* Stat) const
{
	if (!Stat) {
		LOG("UCharacterSheet::GetStat: Null Stat");
		return 0;
	}

	int32 Value = GetBaseStat(Stat);
	
	// Add status effect bonuses
	for (auto Status : StatusEffects) {
		Value = Status->ModifyGetStat(Stat, Value);
	}

	// Add party status effect bonuses
	if (PartyMember.IsValid()) {
		for (auto Status : PartyMember->GetParty()->GetStatusEffects()) {
			Value = Status->ModifyGetStat(Stat, Value);
		}
	}

	// Add equipment bonuses
	for (auto Item : Equipment) {
		if (Item.Value) {
			UEquippableComponent* Equippable = UEquippableComponent::GetEquippableComponent(Item.Value);
			if (Equippable) {
				if (Equippable->StatModifiers.Contains(Stat)) {
					Value += Equippable->StatModifiers[Stat];
				}
			}
		}
	}

	return Value;
}

void UCharacterSheet::SetBaseStat(UStat* Stat, int32 Value, EUpdateValueType UpdateType)
{
	if (Stat) {
		int32& StatValue = BaseStats.FindOrAdd(Stat);
		const int32 OldValue = StatValue;

		switch (UpdateType) {
		default:
		case EUpdateValueType::Set:
			StatValue = Value;
			break;
		case EUpdateValueType::Add:
			StatValue += Value;
			break;
		}

		if (OldValue != StatValue) {
			if (UBlobberGameInstance::BlobberEventsEnabled()) {
				BaseStatChangedDelegate.Broadcast(this, Stat, StatValue);
			}
		}
	} else {
		LOG("UCharacterSheet::SetBaseStat: Null Stat");
	}
}

float UCharacterSheet::GetCurrentResource(UResource* Resource) const
{
	if (Resource) {
		const float& ResourceValue = const_cast<UCharacterSheet*>(this)->CurrentResources.FindOrAdd(Resource);
		return ResourceValue;
	} else {
		LOG("UCharacterSheet::GetCurrentResourceResource: Null Resource");
		return 0.0f;
	}
}

float UCharacterSheet::GetBaseMaxResource(UResource* Resource) const
{
	if (Resource) {
		const float& ResourceValue = const_cast<UCharacterSheet*>(this)->BaseMaxResources.FindOrAdd(Resource);
		return ResourceValue;
	} else {
		LOG("UCharacterSheet::GetBaseMaxResource: Null Resource");
		return 0.0f;
	}
}

float UCharacterSheet::GetMaxResource_Implementation(UResource* Resource) const
{
	if (Resource) {
		return GetBaseMaxResource(Resource);
	} else {
		LOG("UCharacterSheet::GetMaxResource: Null Resource");
		return 0;
	}
}

const FCharacterSkillInfo& UCharacterSheet::GetBaseSkill(USkill* Skill) const
{
	if (!Skill) {
		LOG("UCharacterSheet::GetBaseSkill: Null Skill");
		return NoSkillInfo;
	}

	if (!BaseSkills.Contains(Skill)) {
		const_cast<UCharacterSheet*>(this)->BaseSkills.Add(Skill, NoSkillInfo);
	}

	return *BaseSkills.Find(Skill);
}

FCharacterSkillInfo& UCharacterSheet::GetBaseSkill(USkill* Skill)
{
	if (!Skill) {
		LOG("UCharacterSheet::GetBaseSkill: Null Skill");
		return NoSkillInfo;
	}

	if (!BaseSkills.Contains(Skill)) {
		BaseSkills.Add(Skill, NoSkillInfo);
	}

	return BaseSkills[Skill];
}

FCharacterSkillInfo UCharacterSheet::GetSkill_Implementation(USkill* Skill) const
{
	if (!Skill) {
		LOG("UCharacterSheet::GetSkill: Null Skill");
		return NoSkillInfo;
	}

	FCharacterSkillInfo Value = GetBaseSkill(Skill);

	UBlobberConfiguration* Config = UBlobberGameInstance::GetBlobberGameInstance()->GetBlobberConfiguration();
	// Check config to see if unlearned skills are allowed to be modified above 0
	if (Value.SkillLevel > 0 || Config->bAllowSkillModifiersOnUnlearnedSkills) {
		// Add status effect bonuses
		for (auto Status : StatusEffects) {
			Value = Status->ModifyGetSkill(Skill, Value);
		}

		// Add party status effect bonuses
		if (PartyMember.IsValid()) {
			for (auto Status : PartyMember->GetParty()->GetStatusEffects()) {
				Value = Status->ModifyGetSkill(Skill, Value);
			}
		}

		// Add equipment bonuses
		for (auto Item : Equipment) {
			if (Item.Value) {
				UEquippableComponent* Equippable = UEquippableComponent::GetEquippableComponent(Item.Value);
				if (Equippable) {
					if (Equippable->SkillModifiers.Contains(Skill)) {
						Value.SkillLevel += Equippable->SkillModifiers[Skill];
					}
				}
			}
		}
	}

	return Value;
}

void UCharacterSheet::SetBaseSkill(USkill* Skill, int32 Value, UMasteryLevel* Mastery)
{
	if (!Skill) {
		LOG("UCharacterSheet::SetBaseSkill: Null Skill");
		return;
	} else if (!Mastery) {
		LOG("UCharacterSheet::SetBaseSkill: Null Mastery");
		return;
	}

	FCharacterSkillInfo& SkillValue = GetBaseSkill(Skill);
	int32 OldValue = SkillValue.SkillLevel;
	UMasteryLevel* OldMastery = SkillValue.MasteryLevel;

	SkillValue = FCharacterSkillInfo(Value, Mastery);

	if (SkillValue.SkillLevel != OldValue) {
		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			BaseSkillLevelChangedDelegate.Broadcast(this, Skill, SkillValue.SkillLevel);
		}
	}
	if (SkillValue.MasteryLevel != OldMastery) {
		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			BaseSkillMasteryChangedDelegate.Broadcast(this, Skill, SkillValue.MasteryLevel);
		}
	}
}

void UCharacterSheet::SetBaseSkillLevel(USkill* Skill, int32 Value, EUpdateValueType UpdateType)
{
	if (!Skill) {
		LOG("UCharacterSheet::SetBaseSkillLevel: Null Skill");
		return;
	}

	FCharacterSkillInfo& SkillValue = GetBaseSkill(Skill);
	int32 NewValue = Value;

	switch (UpdateType) {
	default:
	case EUpdateValueType::Set:
		break;
	case EUpdateValueType::Add:
		NewValue += SkillValue.SkillLevel;
		break;
	}

	SetBaseSkill(Skill, NewValue, SkillValue.MasteryLevel);
}

void UCharacterSheet::SetBaseSkillMastery(USkill* Skill, UMasteryLevel* Mastery)
{
	if (!Skill) {
		LOG("UCharacterSheet::ChangeSkillMastery: Null Skill");
		return;
	} else if (!Mastery) {
		LOG("UCharacterSheet::ChangeSkillMastery: Null Mastery");
		return;
	}

	FCharacterSkillInfo& SkillValue = GetBaseSkill(Skill);
	SetBaseSkill(Skill, 1, Mastery);
}

bool UCharacterSheet::HasBaseSkillLevel(USkill* Skill, int32 Level) const
{
	if (!Skill) {
		LOG("UCharacterSheet::HasSkillLevel: Null Skill");
		return false;
	}

	return GetBaseSkill(Skill).SkillLevel >= Level;
}

bool UCharacterSheet::HasBaseSkillMastery(USkill* Skill, UMasteryLevel* Mastery) const
{
	if (!Skill) {
		LOG("UCharacterSheet::HasSkillMastery: Null Skill");
		return false;
	} else if (!Mastery) {
		LOG("UCharacterSheet::HasSkillMastery: Null Mastery");
		return false;
	}

	return GetBaseSkill(Skill).MasteryLevel->IntVal >= Mastery->IntVal;
}

void UCharacterSheet::SetBaseMaxResource(UResource* Resource, float Value, EUpdateValueType UpdateType, bool bUpdateCurrentResource)
{
	if (!Resource) {
		LOG("UCharacterSheet::SetBaseMaxResource: Null Resource");
		return;
	}

	float& BaseMaxResource = BaseMaxResources[Resource];
	const float OldBaseMaxResource = BaseMaxResource;

	switch (UpdateType) {
	default:
	case EUpdateValueType::Set:
		BaseMaxResource = Value;
		break;
	case EUpdateValueType::Add:
		BaseMaxResource += Value;
		break;
	}

	if (OldBaseMaxResource != BaseMaxResource) {
		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			BaseMaxResourceChangedDelegate.Broadcast(this, Resource, BaseMaxResource);
		}

		if (bUpdateCurrentResource) {
			SetCurrentResource(Resource, Value, false, false, UpdateType);
		}
	}
}

void UCharacterSheet::SetCurrentResource(UResource* Resource, float Value, bool bClampToMaxResource, bool bUpdateConsciousness, EUpdateValueType UpdateType)
{
	if (!Resource) {
		LOG("UCharacterSheet::SetCurrentResource: Null Resource");
		return;
	}

	float& CurrentResource = CurrentResources[Resource];
	const float OldResource = CurrentResource;

	switch (UpdateType) {
	default:
	case EUpdateValueType::Set:
		CurrentResource = Value;
		break;
	case EUpdateValueType::Add:
		CurrentResource += Value;
		break;
	}

	if (bClampToMaxResource) {
		CurrentResource = FMath::Min(CurrentResource, GetMaxResource(Resource));
	}

	if (CurrentResource != OldResource) {
		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			ResourceChangedDelegate.Broadcast(this, Resource, CurrentResource);
		}

		if (bUpdateConsciousness && Resource == HealthResource) {
			const bool bWasDead = (Consciousness == EConsciousness::Dead);
			const bool bWasUnconscious = (Consciousness == EConsciousness::Unconscious) || bWasDead;
			const bool bIsUnconscious = CurrentResource <= 0.0f;
			const bool bIsDead = CurrentResource <= GetDeathThreshold();
			if (bWasDead != bIsDead) {
				SetConsciousness(EConsciousness::Dead);
			} else if (bIsUnconscious) {
				SetConsciousness(EConsciousness::Unconscious);
			} else {
				SetConsciousness(EConsciousness::Alive);
			}
		}
	}
}

void UCharacterSheet::SetConsciousness(EConsciousness NewConsciousness)
{
	if (NewConsciousness != Consciousness) {
		Consciousness = NewConsciousness;

		if (Consciousness == EConsciousness::Dead) {
			for (int32 i = StatusEffects.Num() - 1; i >= 0; --i) {
				if (StatusEffects[i]->bRemoveOnDeath) {
					RemoveStatusEffect(StatusEffects[i]);
				}
			}
		}

		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			ConsciousnessChangedDelegate.Broadcast(this, Consciousness);
		}
	}
}

bool UCharacterSheet::AddStatusEffect(TSubclassOf<UStatusEffect> Status, float Duration, int32 Strength, EStatusOverwriteType Overwrite, bool bAllowPartyStatus)
{
	if (!Status) {
		LOG("UCharacterSheet::AddStatus: Null Status");
		return false;
	}

	if (Duration == 0) {
		LOG("UCharacterSheet::AddStatus: Can't add status with no duration");
		return false;
	}

	UStatusEffect* ExistingStatus = GetStatusEffect(Status);
	if (ExistingStatus) {
		const bool bNewDurationLonger = Duration >= ExistingStatus->GetTimeRemaining();
		const bool bNewStrengthHigher = Strength >= ExistingStatus->GetStrength();
		bool bShouldOverwrite = false;
		switch (Overwrite) {
		case EStatusOverwriteType::NoOverwrite:
			return false;
		case EStatusOverwriteType::AlwaysOverwrite:
			bShouldOverwrite = true;
			break;
		case EStatusOverwriteType::OverwriteIfDurationEqualOrHigher:
			bShouldOverwrite = bNewDurationLonger;
			break;
		case EStatusOverwriteType::OverwriteIfStrengthEqualOrHigher:
			bShouldOverwrite = bNewStrengthHigher;
			break;
		case EStatusOverwriteType::OverwriteIfStrengthAndDurationEqualOrHigher:
			bShouldOverwrite = bNewDurationLonger && bNewStrengthHigher;
			break;
		}
		if (bShouldOverwrite) {
			ExistingStatus->UpdateStatus(Strength, Duration);
			if (UBlobberGameInstance::BlobberEventsEnabled()) {
				StatusUpdatedDelegate.Broadcast(this, ExistingStatus);
			}
			return true;
		} else {
			return false;
		}
	} else {
		UStatusEffect* NewStatus = NewObject<UStatusEffect>(this, Status);
		if (!bAllowPartyStatus && NewStatus->bPartyStatus) {
			LOG("UCharacterSheet::AddStatus: Can't add party status to character sheet");
			return false;
		}
		NewStatus->Init(Strength, Duration, this);
		StatusEffects.Add(NewStatus);
		NewStatus->OnStatusBegin();
		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			StatusBeginDelegate.Broadcast(this, NewStatus);
		}
		return true;
	}
}

UStatusEffect* UCharacterSheet::GetStatusEffect(TSubclassOf<UStatusEffect> Status) const
{
	if (!Status) {
		LOG("UCharacterSheet::GetStatusEffect: Null Status");
		return nullptr;
	}

	for (UStatusEffect* StatusEffect : StatusEffects) {
		if (StatusEffect->GetClass()->IsChildOf(Status)) {
			return StatusEffect;
		}
	}
	return nullptr;
}

bool UCharacterSheet::HasStatusEffect(TSubclassOf<UStatusEffect> Status) const
{
	if (!Status) {
		LOG("UCharacterSheet::HasStatus: Null Status");
		return false;
	}

	return GetStatusEffect(Status) != nullptr;
}

void UCharacterSheet::TickStatusEffects(float DeltaTime)
{
	TArray<UStatusEffect*> ToTick = StatusEffects;
	TArray<UStatusEffect*> ToRemove;
	for (UStatusEffect* Status : ToTick) {
		// The character might die from a status ticking which could remove other
		// status effects, so we must ensure this effect is still valid before
		// processing it.
		if (StatusEffects.Contains(Status)) {
			const bool bIsPermanent = Status->IsPermanent();
			Status->Tick(DeltaTime);
			if (UBlobberGameInstance::BlobberEventsEnabled()) {
				StatusUpdatedDelegate.Broadcast(this, Status);
			}
			if (!bIsPermanent && Status->GetTimeRemaining() <= 0.0f) {
				ToRemove.Add(Status);
			}
		}
	}

	for (UStatusEffect* Status : ToRemove) {
		RemoveStatusEffect(Status);
	}
}

void UCharacterSheet::TickStatusEffectsFromTimeManager(UTimeManager* TimeManager, ETimeUnit TimeUnit, int32 TimeAdvanced)
{
	TickStatusEffects((float)TimeAdvanced);
}

bool UCharacterSheet::RemoveStatusEffectFromClass(TSubclassOf<UStatusEffect> Status)
{
	if (!Status) {
		LOG("UCharacterSheet::RemoveStatusEffectFromClass: Null Status");
		return false;
	}

	UStatusEffect* ExistingStatus = GetStatusEffect(Status);
	if (ExistingStatus) {
		return RemoveStatusEffect(ExistingStatus);
	} else {
		return false;
	}
}

bool UCharacterSheet::RemoveStatusEffect(UStatusEffect* Status)
{
	if (!Status) {
		LOG("UCharacterSheet::RemoveStatusEffect: Null Status");
		return false;
	}

	int32 Index = -1;
	if (StatusEffects.Find(Status, Index)) {
		Status->OnStatusEnd();
		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			StatusEndDelegate.Broadcast(this, Status);
		}
		StatusEffects.RemoveAt(Index);
		return true;
	} else {
		return false;
	}
}


UItem* UCharacterSheet::EquipItem_Implementation(UEquipSlot* Slot, UItem* Item)
{
	if (!Slot) {
		LOG("UCharacterSheet::EquipItem: Null Slot")
			return nullptr;
	}

	if (CanEquipItem(Slot, Item)) {
		UItem* OldItem = GetEquippedItem(Slot);
		Equipment[Slot] = Item;
		if (Item) {
			UEquippableComponent::GetEquippableComponent(Item)->OnEquip(this);
		}
		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			EquipmentChangedDelegate.Broadcast(this, Slot, Item, OldItem);
		}
		return OldItem;
	}
	return Item;
}

bool UCharacterSheet::CanEquipItem_Implementation(UEquipSlot* Slot, UItem* Item) const
{
	if (!Slot) {
		LOG("UCharacterSheet::CanEquipItem: Null Slot")
			return false;
	}

	if (!Item) {
		return true;
	}

	UEquippableComponent* Equippable = UEquippableComponent::GetEquippableComponent(Item);
	if (!Equippable) {
		return false;
	}

	if (!Equippable->ItemType || !Equippable->ItemType->RequiredSkill || HasBaseSkillLevel(Equippable->ItemType->RequiredSkill, 1)) {
		return Equippable->ItemType->Slots.Contains(Slot);
	}

	return false;
}

UItem* UCharacterSheet::GetEquippedItem(UEquipSlot* Slot) const
{
	if (!Slot) {
		LOG("UCharacterSheet::GetEquippedItem: Null Slot")
			return false;
	}

	if (!Equipment.Contains(Slot)) {
		const_cast<UCharacterSheet*>(this)->Equipment.Add(Slot, nullptr);
	}
	return Equipment[Slot];
}
