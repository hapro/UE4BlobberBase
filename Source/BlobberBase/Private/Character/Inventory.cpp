// Fill out your copyright notice in the Description page of Project Settings.

#include "Inventory.h"

#include "BlobberGameInstance.h"
#include "Item/Item.h"
#include "Item/ItemComponent.h"
#include "Macros.h"

UInventory::UInventory()
	: Super()
{

}

bool UInventory::AddItemToInventory_Implementation(UItem* Item)
{
	if (!Item) {
		LOG("UPartyMember::AddItemToInventory: Null Item")
			return false;
	}

	if (CanAddItemToInventory(Item)) {
		Inventory.Add(Item);
		InventoryWeight += Item->Weight;
		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			InventoryChangedDelegate.Broadcast(this);
		}
		return true;
	}

	return false;
}

bool UInventory::CanAddItemToInventory_Implementation(UItem* Item)
{
	if (!Item) {
		LOG("UPartyMember::CanAddItemToInventory: Null Item")
			return false;
	}

	if (MaxInventoryItems < 0 || MaxInventoryItems > Inventory.Num()) {
		const float MaxInventoryWeight = GetMaxInventoryWeight();
		if (MaxInventoryWeight < 0.0f || MaxInventoryWeight >= InventoryWeight + Item->Weight) {
			return true;
		}
	}

	return false;
}

bool UInventory::RemoveItemFromInventoryAt(int32 Index)
{
	if (Index >= 0 && Index < Inventory.Num()) {
		UItem* Item = Inventory[Index];
		Inventory.RemoveAt(Index);
		InventoryWeight -= Item->Weight;
		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			InventoryChangedDelegate.Broadcast(this);
		}
		return true;
	}
	return false;
}

bool UInventory::RemoveItemFromInventory(UItem* Item)
{
	int32 Index;
	Inventory.Find(Item, Index);
	return RemoveItemFromInventoryAt(Index);
}

UItem* UInventory::FindItemInInventoryByName(const FString& Name) const
{
	for (UItem* Item : Inventory) {
		if (Item->GetItemName() == Name) {
			return Item;
		}
	}
	return nullptr;
}

UItem* UInventory::FindItemInInventoryAt(int32 Index) const
{
	if (Index >= 0 && Index < Inventory.Num()) {
		return Inventory[Index];
	}
	return nullptr;
}

void UInventory::SortInventory_Implementation()
{
	Inventory.Sort([](const UItem& LHS, const UItem& RHS) {
		// FText seems to have some oddities with comparisons
		//return LHS.GetItemDisplayName().CompareTo(RHS.GetItemDisplayName());
		return LHS.GetItemName() < RHS.GetItemName();
	});
}
