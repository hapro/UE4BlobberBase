// Fill out your copyright notice in the Description page of Project Settings.

#include "PartyMember.h"

#include "BlobberGameInstance.h"
#include "BlobberConfiguration.h"
#include "Macros.h"
#include "Character/CharacterSheet.h"
#include "Data/CharacterClass.h"
#include "Data/MasteryLevel.h"
#include "Data/Skill.h"
#include "Data/Stat.h"

UPartyMember::UPartyMember(const FObjectInitializer& Initializer)
	: Super(Initializer)
{
}

void UPartyMember::InitPrivate(UCharacterClass* InCharacterClass, UParty* InParty, int32 Index)
{
	UBlobberConfiguration* Config = UBlobberGameInstance::GetBlobberGameInstance()->GetBlobberConfiguration();

	Party = InParty;
	IndexInParty = Index;

	if (Config) {
		CharacterClass = InCharacterClass;
		CharacterSheet = NewObject<UCharacterSheet>(this, Config->CharacterSheetClass);
		CharacterSheet->InitPrivate(CharacterClass, this);

		Init();
	} else {
		LOG("UPartyMember::InitPrivate: BlobberConfiguration not found. Failed to initialize.")
	}
}

void UPartyMember::SetExperience(int32 Amount, bool bLevelUp, EUpdateValueType UpdateType)
{
	const int32 OldValue = Experience;

	switch (UpdateType) {
	default:
	case EUpdateValueType::Set:
		Experience = Amount;
		break;
	case EUpdateValueType::Add:
		Experience += ModifyExperience(Amount);
		break;
	}

	if (OldValue != Experience) {
		if (bLevelUp) {
			while (Experience >= GetExperienceRequiredForNextLevel()) {
				LevelUp();
			}
		}

		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			ExperienceChangedDelegate.Broadcast(this, Experience);
		}
	}
}

int32 UPartyMember::GetExperienceRequiredForNextLevel_Implementation() const
{
	const int32 Level = CharacterSheet->GetLevel();
	return Level * Level * 1000;
}

void UPartyMember::LevelUp_Implementation()
{
	Experience -= GetExperienceRequiredForNextLevel();
	CharacterSheet->LevelUp();
	for (const auto& StatInfo : CharacterClass->StatInfo) {
		const int32 StatGain = UCharacterClass::GetStatGainForLevel(StatInfo.Value.GainPerLevel, CharacterSheet->GetLevel());
		CharacterSheet->SetBaseStat(StatInfo.Key, StatGain);
	}
	for (const auto& ResourceInfo : CharacterClass->ResourceInfo) {
		CharacterSheet->SetBaseMaxResource(ResourceInfo.Key, ResourceInfo.Value.GainPerLevel);
	}
	SetStatPoints(GetStatPointsForLevel(CharacterSheet->GetLevel()));
	SetSkillPoints(GetSkillPointsForLevel(CharacterSheet->GetLevel()));
	if (UBlobberGameInstance::BlobberEventsEnabled()) {
		LevelUpDelegate.Broadcast(this, CharacterSheet->GetLevel());
	}
}

int32 UPartyMember::GetStatPointsForLevel_Implementation(int32 Level) const
{
	return Level / 4 + 4;
}

int32 UPartyMember::GetSkillPointsForLevel_Implementation(int32 Level) const
{
	return Level / 2 + 1;
}

void UPartyMember::SetCharacterClass(UCharacterClass* InCharacterClass)
{
	if (!InCharacterClass) {
		LOG("UPartyMember::SetCharacterClass: Null Class")
	} else {
		CharacterClass = InCharacterClass;
		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			ClassChangedDelegate.Broadcast(this, CharacterClass);
		}
	}
}

bool UPartyMember::CanIncreaseSkillMastery_Implementation(USkill* Skill, UMasteryLevel* Mastery) const
{
	if (!Skill) {
		LOG("UPartyMember::CanIncreaseSkillMastery: Null Skill");
		return false;
	} else if (!Mastery) {
		LOG("UPartyMember::CanIncreaseSkillMastery: Null Mastery");
		return false;
	}

	const FCharacterSkillInfo& SkillInfo = CharacterSheet->GetBaseSkill(Skill);
	return (CharacterClass->MaxSkillMastery[Skill]->IntVal >= Mastery->IntVal) &&
		(Mastery->IntVal == SkillInfo.MasteryLevel->IntVal + 1) &&
		(SkillInfo.SkillLevel >= Mastery->SkillLevelAvailableAt);
}

bool UPartyMember::IncreaseSkillMastery(USkill* Skill, UMasteryLevel* Mastery)
{
	if (!Skill) {
		LOG("UPartyMember::IncreaseSkillMastery: Null Skill");
		return false;
	}

	if (CanIncreaseSkillMastery(Skill, Mastery)) {
		CharacterSheet->SetBaseSkillMastery(Skill, Mastery);
		return true;
	}
	return false;
}

bool UPartyMember::PromoteClass_Implementation(UCharacterClass* NewClass, bool bRequirePrerequisiteClass, bool bLearnStartingSkills, bool bReadjustStats)
{
	if (!NewClass) {
		LOG("UPartyMember::PromoteClass: Null Class")
		return false;
	}

	if (bRequirePrerequisiteClass && NewClass->PrerequisiteClass != CharacterClass) {
		return false;
	}

	if (bReadjustStats) {
		for (const auto& StatInfo : CharacterClass->StatInfo) {
			// Calculate the current stat bonus for the current level from the
			// old class, and then subtract that from the same calculation for
			// the new class. This tells the difference in stat gain between
			// the two classes to properly adjust the base value, regardless of
			// any other permanent stat modifications.
			const int32 OldCumulative = UCharacterClass::GetCumulativeStatGainForLevel(StatInfo.Value.GainPerLevel, CharacterSheet->GetLevel());
			const int32 OldValue = StatInfo.Value.DefaultValue + OldCumulative;

			const int32 NewCumulative = UCharacterClass::GetCumulativeStatGainForLevel(NewClass->StatInfo[StatInfo.Key].GainPerLevel, CharacterSheet->GetLevel());
			const int32 NewValue = NewClass->StatInfo[StatInfo.Key].DefaultValue + NewCumulative;

			CharacterSheet->SetBaseStat(StatInfo.Key, NewValue - OldValue);
		}

		for (const auto& ResourceInfo : CharacterClass->ResourceInfo) {
			const float OldCumulative = UCharacterClass::GetCumulativeResourceGainForLevel(ResourceInfo.Value.GainPerLevel, CharacterSheet->GetLevel());
			const float OldValue = ResourceInfo.Value.DefaultValue + OldCumulative;

			const int32 NewCumulative = UCharacterClass::GetCumulativeResourceGainForLevel(NewClass->ResourceInfo[ResourceInfo.Key].GainPerLevel, CharacterSheet->GetLevel());
			const int32 NewValue = NewClass->ResourceInfo[ResourceInfo.Key].DefaultValue + NewCumulative;

			CharacterSheet->SetBaseMaxResource(ResourceInfo.Key, NewValue - OldValue);
		}
	}

	if (bLearnStartingSkills) {
		UBlobberConfiguration* Config = UBlobberGameInstance::GetBlobberGameInstance()->GetBlobberConfiguration();
		for (USkill* Skill : NewClass->StartingSkills) {
			if (!CharacterSheet->HasBaseSkillMastery(Skill, Config->BasicMasteryLevel)) {
				CharacterSheet->SetBaseSkillMastery(Skill, Config->BasicMasteryLevel);
			}
		}
	}

	SetCharacterClass(NewClass);

	return true;
}

bool UPartyMember::CanIncreaseStatWithStatPoints(UStat* Stat) const
{
	if (!Stat) {
		LOG("UPartyMember::CanIncreaseStatWithStatPoints: Null Stat")
		return false;
	}

	return Stat->bCanUpgradeWithStatPoints && StatPoints >= GetCostToIncreaseStat(Stat);
}

bool UPartyMember::CanIncreaseSkillWithSkillPoints(USkill* Skill) const
{
	if (!Skill) {
		LOG("UPartyMember::CanIncreaseSkillWithSkillPoints: Null Skill")
			return false;
	}

	return CharacterSheet->GetBaseSkill(Skill).SkillLevel > 0 && SkillPoints >= GetCostToIncreaseSkill(Skill);
}

int32 UPartyMember::GetCostToIncreaseStat_Implementation(UStat* Stat) const
{
	if (!Stat) {
		LOG("UPartyMember::GetCostToIncreaseStat: Null Stat")
		return 0;
	}

	return 1;
}

int32 UPartyMember::GetCostToIncreaseSkill_Implementation(USkill* Skill) const
{
	if (!Skill) {
		LOG("UPartyMember::GetCostToIncreaseSkill: Null Skill")
		return 0;
	}

	return CharacterSheet->GetBaseSkill(Skill).SkillLevel;
}

bool UPartyMember::IncreaseStatWithStatPoints(UStat* Stat)
{
	if (!Stat) {
		LOG("UPartyMember::IncreaseStatWithStatPoints: Null Stat")
		return false;
	}

	if (CanIncreaseStatWithStatPoints(Stat)) {
		SetStatPoints(-GetCostToIncreaseStat(Stat));
		CharacterSheet->SetBaseStat(Stat, 1);
		return true;
	} else {
		return false;
	}
}

bool UPartyMember::IncreaseSkillWithSkillPoints(USkill* Skill)
{
	if (!Skill) {
		LOG("UPartyMember::IncreaseSkillWithSkillPoints: Null Skill")
		return false;
	}

	if (CanIncreaseSkillWithSkillPoints(Skill)) {
		SetSkillPoints(-GetCostToIncreaseSkill(Skill));
		CharacterSheet->SetBaseSkillLevel(Skill, 1);
		return true;
	} else {
		return false;
	}
}

void UPartyMember::SetStatPoints(int32 Amount, EUpdateValueType UpdateType)
{
	const int32 OldValue = StatPoints;

	switch (UpdateType) {
	default:
	case EUpdateValueType::Set:
		StatPoints = Amount;
		break;
	case EUpdateValueType::Add:
		StatPoints += Amount;
		break;
	}

	StatPoints = FMath::Max(0, StatPoints);

	if (OldValue != StatPoints) {
		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			StatPointsChangedDelegate.Broadcast(this, StatPoints);
		}
	}
}

void UPartyMember::SetSkillPoints(int32 Amount, EUpdateValueType UpdateType)
{
	const int32 OldValue = SkillPoints;

	switch (UpdateType) {
	default:
	case EUpdateValueType::Set:
		SkillPoints = Amount;
		break;
	case EUpdateValueType::Add:
		SkillPoints += Amount;
		break;
	}

	SkillPoints = FMath::Max(0, SkillPoints);

	if (OldValue != SkillPoints) {
		if (UBlobberGameInstance::BlobberEventsEnabled()) {
			SkillPointsChangedDelegate.Broadcast(this, SkillPoints);
		}
	}
}
