// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerCharacter.h"

#include "Classes/Camera/CameraComponent.h"

#include "BlobberGameInstance.h"

APlayerCharacter::APlayerCharacter(const FObjectInitializer& Initializer)
	: Super(Initializer)
{
	FirstPersonCameraComponent = Initializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("FPCamera"));
	FirstPersonCameraComponent->SetupAttachment(RootComponent);
	FirstPersonCameraComponent->SetRelativeLocation(FVector(0.0f, 0.0f, BaseEyeHeight));
	FirstPersonCameraComponent->bUsePawnControlRotation = true;
}

void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	Party = UBlobberGameInstance::GetBlobberGameInstance()->GetParty();
}
