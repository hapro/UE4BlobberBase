// Fill out your copyright notice in the Description page of Project Settings.

#include "Item.h"

#include "BlobberConfiguration.h"
#include "BlobberGameInstance.h"
#include "Util/Macros.h"

UItem::UItem()
	: Super()
{
}

void UItem::PostInitProperties()
{
	Super::PostInitProperties();

	for (auto Component : Components) {
		Component->Item = this;
	}
}

int32 UItem::GetIDPrice_Implementation() const
{
	if (bIsIdentified) {
		return 0;
	} else {
		return GetBuyPrice() / 10;
	}
}

UItemComponent* UItem::GetComponentAt(int32 Index) const
{
	if (Index >= 0 && Index < Components.Num()) {
		return Components[Index];
	}
	return nullptr;
}

UItemComponent* UItem::GetComponent(TSubclassOf<UItemComponent> ComponentClass) const
{
	for (int32 i = 0; i < Components.Num(); ++i) {
		if (Components[i]->GetClass()->IsChildOf(ComponentClass)) {
			return Components[i];
		}
	}
	return nullptr;
}

TArray<UItemComponent*> UItem::GetComponents(TSubclassOf<UItemComponent> ComponentClass) const
{
	TArray<UItemComponent*> AllComponentsOfClass;

	for (int32 i = 0; i < Components.Num(); ++i) {
		if (Components[i]->GetClass()->IsChildOf(ComponentClass)) {
			AllComponentsOfClass.Add(Components[i]);
		}
	}

	return AllComponentsOfClass;
}

UItemComponent* UItem::AddComponent(TSubclassOf<UItemComponent> ComponentClass)
{
	UItemComponent* Component = NewObject<UItemComponent>(this, ComponentClass);
	Component->Item = this;
	Components.Add(Component);
	return Component;
}

bool UItem::RemoveComponentOfType(TSubclassOf<UItemComponent> ComponentClass)
{
	for (int32 i = 0; i < Components.Num(); ++i) {
		if (Components[i]->GetClass()->IsChildOf(ComponentClass)) {
			Components.RemoveAt(i);
			return true;
		}
	}
	return false;
}

bool UItem::RemoveAllComponentsOfType(TSubclassOf<UItemComponent> ComponentClass)
{
	bool bRemovedOne = false;
	for (int32 i = Components.Num() - 1; i >= 0; --i) {
		if (Components[i]->GetClass()->IsChildOf(ComponentClass)) {
			Components.RemoveAt(i);
			bRemovedOne = true;
		}
	}
	return bRemovedOne;
}

bool UItem::RemoveComponent(UItemComponent* Component)
{
	int32 Index = Components.Find(Component);
	return RemoveComponentAt(Index);
}

bool UItem::RemoveComponentAt(int32 Index)
{
	if (Index >= 0 && Index < Components.Num()) {
		Components.RemoveAt(Index);
		return true;
	}
	return false;
}

bool UItem::HasComponent(TSubclassOf<UItemComponent> ComponentClass) const
{
	for (int32 i = 0; i < Components.Num(); ++i) {
		if (Components[i]->GetClass()->IsChildOf(ComponentClass)) {
			return true;
		}
	}
	return false;
}
