// Fill out your copyright notice in the Description page of Project Settings.

#include "ItemComponent.h"

#include "Data/Damage.h"
#include "Data/DamageElement.h"
#include "Data/DamageStyle.h"
#include "Item/Item.h"

UConsumableComponent* UConsumableComponent::GetConsumableComponent(UItem* ConsumableItem)
{
	return Cast<UConsumableComponent>(ConsumableItem->GetComponent(UConsumableComponent::StaticClass()));
}

UEquippableComponent* UEquippableComponent::GetEquippableComponent(UItem* EquippableItem)
{
	return Cast<UEquippableComponent>(EquippableItem->GetComponent(UEquippableComponent::StaticClass()));
}

UDamageComponent* UDamageComponent::GetDamageComponent(UItem* DamageItem)
{
	return Cast<UDamageComponent>(DamageItem->GetComponent(UDamageComponent::StaticClass()));
}

UItemComponent::UItemComponent()
	: Super()
{

}

UConsumableComponent::UConsumableComponent()
	: Super()
{

}

void UConsumableComponent::Consume(UCharacterSheet* CharacterSheet)
{
	OnConsume(CharacterSheet);
}

UEquippableComponent::UEquippableComponent()
	: Super()
{
}

void UDamageComponent::GenerateDamageInstances_Implementation(UDamage* Damage, UDamageStyle* Style, UCharacterSheet* CharacterSheet)
{
	Damage->AddDamageInstance(UDamageInstance::CreateDamageInstance(Element, Style, GetDamage(), 1.0f));
}

UDamageComponent::UDamageComponent()
	: Super()
{
}
