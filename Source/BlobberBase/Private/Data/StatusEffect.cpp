// Fill out your copyright notice in the Description page of Project Settings.

#include "StatusEffect.h"

void UStatusEffect::Init(int32 NewStrength, float NewDuration, UCharacterSheet* InCharacterSheet, UParty* InParty)
{
	Strength = NewStrength;
	TimeRemaining = NewDuration;
	CharacterSheet = InCharacterSheet;
	Party = InParty;
}

void UStatusEffect::Tick(float DeltaTime)
{
	if (!IsPermanent()) {
		SetTimeRemaining(TimeRemaining - DeltaTime);
	}
	OnStatusTick(DeltaTime);
}

void UStatusEffect::SetTimeRemaining(float NewDuration)
{
	const float OldTimeRemaining = TimeRemaining;
	TimeRemaining = NewDuration;
	OnStatusUpdated(Strength, Strength, OldTimeRemaining, TimeRemaining);
}

void UStatusEffect::SetStrength(int32 NewStrength)
{
	const int32 OldStrength = Strength;
	Strength = NewStrength;
	OnStatusUpdated(OldStrength, Strength, TimeRemaining, TimeRemaining);
}

void UStatusEffect::UpdateStatus(int32 NewStrength, float NewDuration)
{
	const int32 OldStrength = Strength;
	const int32 OldTimeRemaining = TimeRemaining;
	Strength = NewStrength;
	TimeRemaining = NewDuration;
	OnStatusUpdated(OldStrength, Strength, OldTimeRemaining, TimeRemaining);
}
