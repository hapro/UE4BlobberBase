// Fill out your copyright notice in the Description page of Project Settings.

#include "Damage.h"

#include "BlobberConfiguration.h"
#include "BlobberGameInstance.h"
#include "Character/CharacterSheet.h"

UDamage* UDamage::CreateDamage(AActor* DamageCausingActor,
							   UCharacterSheet* DamageCausingObject)
{
	UDamage* Damage = NewObject<UDamage>();
	Damage->Init(DamageCausingActor, DamageCausingObject);
	return Damage;
}

void UDamage::Init(AActor* DamageCausingActor, UCharacterSheet* DamageCausingObject)
{
	Instances.Empty();
	this->DamageCausingActor = DamageCausingActor;
	this->DamageCausingObject = DamageCausingObject;
}

UDamageInstance* UDamageInstance::CreateDamageInstance(UDamageElement* DamageElement, UDamageStyle* DamageStyle,
													   float DamageValue, float Multiplier,
													   UResource* ResourceToDamage, bool bIsFeedback)
{
	UDamageInstance* DamageInstance = NewObject<UDamageInstance>();
	DamageInstance->Init(DamageElement, DamageStyle, DamageValue, Multiplier, ResourceToDamage, bIsFeedback);
	return DamageInstance;
}

void UDamageInstance::Init(UDamageElement* DamageElement, UDamageStyle* DamageStyle,
						   float DamageValue, float Multiplier,
						   UResource* ResourceToDamage, bool bIsFeedback)
{
	this->ResourceToDamage = ResourceToDamage;
	if (!this->ResourceToDamage) {
		const UBlobberConfiguration* Config = UBlobberGameInstance::GetBlobberGameInstance()->GetBlobberConfiguration();
		this->ResourceToDamage = Config->DefaultHealthResource;
	}
	this->DamageElement = DamageElement;
	this->DamageStyle = DamageStyle;
	this->DamageValue = DamageValue;
	this->Multiplier = Multiplier;
	this->bIsFeedback = bIsFeedback;
}

UStatusInstance* UStatusInstance::CreateStatusInstance(TSubclassOf<UStatusEffect> Status, int32 StatusStrength,
													   float StatusDuration, int32 Accuracy)
{
	UStatusInstance* StatusInstance = NewObject<UStatusInstance>();
	StatusInstance->Init(Status, StatusStrength, StatusDuration, Accuracy);
	return StatusInstance;
}

void UStatusInstance::Init(TSubclassOf<UStatusEffect> Status, int32 StatusStrength,
						   float StatusDuration, int32 Accuracy)
{
	this->Status = Status;
	this->StatusStrength = StatusStrength;
	this->StatusDuration = StatusDuration;
	this->Accuracy = Accuracy;
}

void UDamage::AddDamageInstance(UDamageInstanceBase* Instance)
{
	Instances.Add(Instance);
	Instance->DamageContainer = this;
}

void UDamageInstance::ApplyDamageInstance_Implementation(UCharacterSheet* Target)
{
	const float FinalDamage = Target->MitigateDamage(this);
	UResource* Resource = ResourceToDamage ? ResourceToDamage : UBlobberGameInstance::GetBlobberGameInstance()->GetBlobberConfiguration()->DefaultHealthResource;
	Target->SetCurrentResource(Resource, -FinalDamage, false, true, EUpdateValueType::Add);
}

void UStatusInstance::ApplyDamageInstance_Implementation(UCharacterSheet* Target)
{
	if (FMath::RandRange(1, 100) <= Accuracy) {
		Target->AddStatusEffect(Status, StatusDuration, StatusStrength);
	}
}
