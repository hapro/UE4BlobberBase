// Fill out your copyright notice in the Description page of Project Settings.

#include "CharacterClass.h"

#include "ARFilter.h"
#include "AssetRegistryModule.h"
#include "ModuleManager.h"

#include "Data/MasteryLevel.h"
#include "Data/Resource.h"
#include "Data/Skill.h"
#include "Data/Stat.h"

namespace {
	// Duplicated non-templated sort functions :(
	bool StatAlphaSort(const UStat& A, const UStat& B) {
		// &(*P) seems to return nullptr if P is a nullptr. Workaround for references?
		// They could be null after an asset is deleted.
		if (!&A) {
			return true;
		} else if (!&B) {
			return false;
		} else {
			return A.GetName().Compare(B.GetName(), ESearchCase::IgnoreCase) < 0;
		}
	}

	bool SkillAlphaSort(const USkill& A, const USkill& B) {
		if (!&A) {
			return true;
		} else if (!&B) {
			return false;
		} else {
			return A.GetName().Compare(B.GetName(), ESearchCase::IgnoreCase) < 0;
		}
	}

	bool ResourceAlphaSort(const UResource& A, const UResource& B) {
		if (!&A) {
			return true;
		} else if (!&B) {
			return false;
		} else {
			return A.GetName().Compare(B.GetName(), ESearchCase::IgnoreCase) < 0;
		}
	}

	bool AssetAlphaSort(const FAssetData& A, const FAssetData& B) {
		return A.AssetName.Compare(B.AssetName) < 0;
	}
}

UCharacterClass::UCharacterClass(const FObjectInitializer& Initializer)
	: Super(Initializer)
{
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(FName("AssetRegistry"));
	IAssetRegistry& AssetRegistry = AssetRegistryModule.Get();
	// Must wait until all assets are discovered before populating list of other GameInfoBase assets.
	if (AssetRegistry.IsLoadingAssets()) {
		AssetRegistry.OnFilesLoaded().AddUObject(this, &UCharacterClass::OnRegistryLoaded);
	} else {
		OnRegistryLoaded();
	}
}

int32 UCharacterClass::GetStatGainForLevel(float GainPerLevel, int32 Level)
{
	return GetCumulativeStatGainForLevel(GainPerLevel, Level) - GetCumulativeStatGainForLevel(GainPerLevel, Level - 1);
}

int32 UCharacterClass::GetCumulativeStatGainForLevel(float GainPerLevel, int32 Level)
{
	return (int32)(GainPerLevel * (Level - 1));
}

float UCharacterClass::GetCumulativeResourceGainForLevel(float GainPerLevel, int32 Level)
{
	return GainPerLevel * (Level - 1);
}

void UCharacterClass::InitAllFields()
{
	// Store original values of each field, so we can reapply them after setting CDO.
	const auto DuplicateMaxSkillMastery = MaxSkillMastery;
	const auto DuplicateStatInfo = StatInfo;
	const auto DuplicateResourceInfo = ResourceInfo;

	MaxSkillMastery.Empty();
	StatInfo.Empty();

	// Find list of all UStat, and USkill assets in Content Browser.
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(FName("AssetRegistry"));
	IAssetRegistry& AssetRegistry = AssetRegistryModule.Get();

	FARFilter Filter;
	Filter.ClassNames = { TEXT("Stat"), TEXT("Skill"), TEXT("Resource") };
	Filter.bRecursiveClasses = true;

	TArray<FAssetData> AssetList;
	// This data could be stored as static and processed in Default___CharacterClass's UObject,
	// but I think the performance hit is negligible and it's easier this way.
	AssetRegistry.GetAssets(Filter, AssetList);

	TArray<UStat*> Stats;
	TArray<USkill*> Skills;
	TArray<UResource*> Resources;

	// Split assets into separate arrays.
	for (const FAssetData& Asset : AssetList) {
		UObject* Obj = Asset.GetAsset();
		if (Obj->GetClass()->IsChildOf(UStat::StaticClass())) {
			Stats.Add(Cast<UStat>(Obj));
		} else if (Obj->GetClass()->IsChildOf(USkill::StaticClass())) {
			Skills.Add(Cast<USkill>(Obj));
		} else if (Obj->GetClass()->IsChildOf(UResource::StaticClass())) {
			Resources.Add(Cast<UResource>(Obj));
		}
	}

	// Populate stats and skills.
	for (UStat* Stat : Stats) {
		StatInfo.Emplace(Stat);
	}

	for (USkill* Skill : Skills) {
		MaxSkillMastery.Add(Skill, nullptr);
	}

	for (UResource* Resource : Resources) {
		ResourceInfo.Emplace(Resource);
	}

	// Sort alphabetically.
	StatInfo.KeySort(StatAlphaSort);
	MaxSkillMastery.KeySort(SkillAlphaSort);
	ResourceInfo.KeySort(ResourceAlphaSort);

	// Update CDOs.
	Cast<UCharacterClass>(GetClass()->GetDefaultObject())->StatInfo = StatInfo;
	Cast<UCharacterClass>(GetClass()->GetDefaultObject())->MaxSkillMastery = MaxSkillMastery;
	Cast<UCharacterClass>(GetClass()->GetDefaultObject())->ResourceInfo = ResourceInfo;

	// Reapply any values that were set before re-initialization.
	for (const auto& Iter : StatInfo) {
		if (DuplicateStatInfo.Contains(Iter.Key)) {
			StatInfo[Iter.Key] = DuplicateStatInfo[Iter.Key];
		}
	}
	for (const auto& Iter : MaxSkillMastery) {
		if (DuplicateMaxSkillMastery.Contains(Iter.Key)) {
			MaxSkillMastery[Iter.Key] = DuplicateMaxSkillMastery[Iter.Key];
		}
	}
	for (const auto& Iter : ResourceInfo) {
		if (DuplicateResourceInfo.Contains(Iter.Key)) {
			ResourceInfo[Iter.Key] = DuplicateResourceInfo[Iter.Key];
		}
	}
}

void UCharacterClass::OnRegistryLoaded()
{
	InitAllFields();

	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(FName("AssetRegistry"));
	IAssetRegistry& AssetRegistry = AssetRegistryModule.Get();

	// Listen for new assets being added/removed in the Content Browser.
	AssetRegistry.OnAssetAdded().AddUObject(this, &UCharacterClass::OnAssetAdded);
	AssetRegistry.OnAssetRemoved().AddUObject(this, &UCharacterClass::OnAssetRemoved);
}

void UCharacterClass::OnAssetAdded(const FAssetData& Asset)
{
	if (Asset.AssetClass == TEXT("Stat")) {
		UStat* Stat = Cast<UStat>(Asset.GetAsset());
		StatInfo.Add(Stat);
		StatInfo.KeySort(StatAlphaSort);
	} else if (Asset.AssetClass == TEXT("Skill")) {
		USkill* Skill = Cast<USkill>(Asset.GetAsset());
		MaxSkillMastery.Add(Skill, nullptr);
		MaxSkillMastery.KeySort(SkillAlphaSort);
	} else if (Asset.AssetClass == TEXT("Resource")) {
		UResource* Resource = Cast<UResource>(Asset.GetAsset());
		ResourceInfo.Add(Resource);
		ResourceInfo.KeySort(ResourceAlphaSort);
	}
}

void UCharacterClass::OnAssetRemoved(const FAssetData& Asset)
{
	if (Asset.AssetClass == TEXT("Stat")) {
		for (auto Iter = StatInfo.CreateIterator(); Iter; ++Iter) {
			if (!Iter.Key()) {
				Iter.RemoveCurrent();
			}
		}
	} else if (Asset.AssetClass == TEXT("Skill")) {
		for (auto Iter = MaxSkillMastery.CreateIterator(); Iter; ++Iter) {
			if (!Iter.Key()) {
				Iter.RemoveCurrent();
			}
		}
	} else if (Asset.AssetClass == TEXT("Resource")) {
		for (auto Iter = ResourceInfo.CreateIterator(); Iter; ++Iter) {
			if (!Iter.Key()) {
				Iter.RemoveCurrent();
			}
		}
	}
}

void UCharacterClass::AllClassesSetDefaultMaxSkillMastery(UMasteryLevel* MasteryLevel)
{
	for (TObjectIterator<UCharacterClass> Iter; Iter; ++Iter) {
		for (auto& Mastery : Iter->MaxSkillMastery) {
			if (!Mastery.Value) {
				Mastery.Value = MasteryLevel;
			}
		}
	}
}

#if WITH_EDITOR
void UCharacterClass::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	if (PropertyChangedEvent.Property) {
		if (PropertyChangedEvent.Property->GetFName() == GET_MEMBER_NAME_CHECKED(UCharacterClass, bForceResync)) {
			bForceResync = false;
			InitAllFields();
		}
	}
}
#endif
