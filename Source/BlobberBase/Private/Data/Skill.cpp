// Fill out your copyright notice in the Description page of Project Settings.

#include "Skill.h"

#include "ARFilter.h"
#include "AssetRegistryModule.h"
#include "ModuleManager.h"

#include "Data/MasteryLevel.h"

namespace {
	bool MasteryIntSort(const UMasteryLevel& A, const UMasteryLevel& B) {
		if (!&A) {
			return true;
		} else if (!&B) {
			return false;
		} else {
			return A.IntVal < B.IntVal;
		}
	}
}

USkill::USkill(const FObjectInitializer& Initializer)
	: Super(Initializer)
{
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(FName("AssetRegistry"));
	IAssetRegistry& AssetRegistry = AssetRegistryModule.Get();
	// Must wait until all assets are discovered before populating list of other GameInfoBase assets.
	if (AssetRegistry.IsLoadingAssets()) {
		AssetRegistry.OnFilesLoaded().AddUObject(this, &USkill::OnRegistryLoaded);
	} else {
		OnRegistryLoaded();
	}
}

void USkill::InitMasteryDescriptions()
{
	const auto DuplicateMasteryDescriptions = MasteryDescriptions;
	MasteryDescriptions.Empty();

	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(FName("AssetRegistry"));
	IAssetRegistry& AssetRegistry = AssetRegistryModule.Get();

	FARFilter Filter;
	Filter.ClassNames = { TEXT("MasteryLevel") };
	Filter.bRecursiveClasses = true;

	TArray<FAssetData> AssetList;
	AssetRegistry.GetAssets(Filter, AssetList);

	// Split assets into separate arrays.
	for (const FAssetData& Asset : AssetList) {
		UObject* Obj = Asset.GetAsset();
		if (Obj->GetClass()->IsChildOf(UMasteryLevel::StaticClass())) {
			MasteryDescriptions.Add(Cast<UMasteryLevel>(Obj));
		}
	}
	MasteryDescriptions.KeySort(MasteryIntSort);

	// Update CDOs.
	Cast<USkill>(GetClass()->GetDefaultObject())->MasteryDescriptions = MasteryDescriptions;

	// Reapply any values that were set before re-initialization.
	for (const auto& Iter : MasteryDescriptions) {
		if (DuplicateMasteryDescriptions.Contains(Iter.Key)) {
			MasteryDescriptions[Iter.Key] = DuplicateMasteryDescriptions[Iter.Key];
		}
	}
}

void USkill::OnRegistryLoaded()
{
	InitMasteryDescriptions();

	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(FName("AssetRegistry"));
	IAssetRegistry& AssetRegistry = AssetRegistryModule.Get();

	// Listen for new assets being added/removed in the Content Browser.
	AssetRegistry.OnAssetAdded().AddUObject(this, &USkill::OnAssetAdded);
	AssetRegistry.OnAssetRemoved().AddUObject(this, &USkill::OnAssetRemoved);
}

void USkill::OnAssetAdded(const FAssetData& Asset)
{
	if (Asset.AssetClass == TEXT("MasteryLevel")) {
		UMasteryLevel* Mastery = Cast<UMasteryLevel>(Asset.GetAsset());
		MasteryDescriptions.Add(Mastery);
		MasteryDescriptions.KeySort(MasteryIntSort);
	}
}

void USkill::OnAssetRemoved(const FAssetData& Asset)
{
	if (Asset.AssetClass == TEXT("MasteryLevel")) {
		for (auto Iter = MasteryDescriptions.CreateIterator(); Iter; ++Iter) {
			if (!Iter.Key()) {
				Iter.RemoveCurrent();
			}
		}
	}
}

#if WITH_EDITOR
void USkill::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	if (PropertyChangedEvent.Property) {
		if (PropertyChangedEvent.Property->GetFName() == GET_MEMBER_NAME_CHECKED(USkill, bForceResync)) {
			bForceResync = false;
			InitMasteryDescriptions();
		}
	}
}
#endif
