// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BlobberBaseGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BLOBBERBASE_API ABlobberBaseGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABlobberBaseGameModeBase();
	
	virtual void BeginPlay() override;
};
