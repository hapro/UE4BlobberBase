// Fill out your copyright notice in the Description page of Project Settings.

#include "BlobberBaseGameModeBase.h"

#include "BlobberGameInstance.h"
#include "Character/BlobberPlayerController.h"
#include "Character/PlayerCharacter.h"

ABlobberBaseGameModeBase::ABlobberBaseGameModeBase()
	: Super()
{
	DefaultPawnClass = APlayerCharacter::StaticClass();
	PlayerControllerClass = ABlobberPlayerController::StaticClass();
}

void ABlobberBaseGameModeBase::BeginPlay()
{
	GetWorldTimerManager().SetTimerForNextTick(UBlobberGameInstance::GetBlobberGameInstance(), &UBlobberGameInstance::InitializeLevel);
}
