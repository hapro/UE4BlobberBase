// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Util/Enums.h"
#include "TimeManager.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTimeChangedDelegate, UTimeManager*, TimeManager);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FTimerTickedDelegate, UTimeManager*, TimeManager, ETimeUnit, TimeUnit, int32, TimeAdvanced);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FTimeUnitAdvancedDelegate, UTimeManager*, TimeManager, ETimeUnit, TimeUnit, int32, OldValue, int32, NewValue);

// Helper struct to store an array in a map.
USTRUCT(BlueprintType)
struct FTimeUnitNames
{
	GENERATED_USTRUCT_BODY()

	// A list of names that correspond to time values for a given TimeUnit.
	// Using weeks as an example, if there are 5 weeks per month then this array
	// should have 5 entries representing the display name of each week.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<FText> Names;
};

// A Time of Day manager that can represent anywhere from seconds to years.
// The lowest interval of change can be any unit, rather than only 1 second at a time.
// Similarly, the real-world time to increase 1 interval (tick) can be customized.
UCLASS(Blueprintable, BlueprintType)
class BLOBBERBASE_API UTimeManager : public UObject
{
	GENERATED_BODY()
	
public:
	UTimeManager(const FObjectInitializer& Initializer);

	// Forcefully set the time to a specific value, which generally should only
	// be used for initialization and loading. AdvanceTime might be more
	// appropriate in the middle of the game. Large values will not be
	// wrapped to the next TimeUnit, so be careful.
	UFUNCTION(BlueprintCallable, Category = "Time")
	void Init(int32 Y, int32 Mon, int32 W, int32 D, int32 H, int32 Min, int32 S, bool bBroadcastEvent = true);

	// Start the timer, which calls TickTimer every RealWorldSecondsPerTimerTick seconds.
	UFUNCTION(BlueprintCallable, Category = "Time")
	void StartTimer();

	// Custom functionality for immediately after the timer starts can go here.
	UFUNCTION(BlueprintNativeEvent)
	void OnTimerStarted();
	virtual void OnTimerStarted_Implementation() {}

	// Stop the timer. Timers don't run when the game is paused, so this should only
	// be used in cases where game time isn't advanced, but the game is still active.
	UFUNCTION(BlueprintCallable, Category = "Time")
	void StopTimer();

	// Custom functionality for immediately after the timer stops can go here.
	UFUNCTION(BlueprintNativeEvent)
	void OnTimerStopped();
	virtual void OnTimerStopped_Implementation() {}

	// Increment the current time by AmountIncreasedPerTimerTick units, and
	// broadcast TimerTickedDelegate for anything listening to it. FTimeChangedDelegate
	// will be broadcast immediately before each tick event.
	UFUNCTION(BlueprintCallable, Category = "Time")
	void TickTimer(int32 NumberOfTimes = 1);

	// Advance time by some specified value. If IncrementInTicks is true, then
	// this function will calculate how many ticks are necessary to achieve the
	// desired time, then tick that many times, and finally add the remaining
	// time. If IncrementInTicks is false, it simply adds the time without
	// broadcasting the events (but TimeChangedDelegate still sends).
	UFUNCTION(BlueprintCallable, Category = "Time")
	void AdvanceTime(ETimeUnit TimeUnit, int32 Amount, bool bIncrementInTicks);

	// Return the current day relative to the month rather than the week. So if
	// it's the 3rd day of the 4th week, this would return 3 + 4 * DaysPerWeek.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Time")
	int32 GetDayInMonth() const;

	// If NamesOfTimeUnitIntervals has a valid entry for the current time, return it.
	// Otherwise, return an empty Text object.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Time")
	const FText& GetNameOfCurrentTime(ETimeUnit TimeUnit);

	// If NamesOfTimeUnitIntervals has a valid entry for the specified time, return it.
	// Otherwise, return an empty Text object.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Time")
	const FText& GetNameOfTimeUnit(ETimeUnit TimeUnit, int32 Value);

protected:
	// Calls AdvanceTime while emitting TimerTickedDelegate.
	void TickTimerPrivate();
	
/// MEMBER VARIABLES ///
public:
	// If true, CharacterSheet and Party classes will automatically register
	// themselves to listen to TimerTickedDelegate and tick all their status
	// effects each time the event is received. If false, you should manually
	// be ticking each status effect somehow at your interval of choice.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Advance")
	bool bTickStatusEffectsOnTimerTick = true;

protected:
	// The current date and time in the game world.
	UPROPERTY(EditDefaultsOnly, BlueprintGetter = GetCurrentTime, Category = "Time")
	TMap<ETimeUnit, int32> CurrentTime;

	// Amount to add to CurrentTime before returning, since CurrentTime stores
	// all TimeUnits starting at 0. So, if you want Days to go from 1-30 instead
	// of 0-29, Days should be set to 1.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Time")
	TMap<ETimeUnit, int32> TimeUnitOffset;

	// The number of units required to overflow to the next unit. For instance,
	// if Seconds is set to 60, then 60 seconds will equal 1 minute. Because of
	// how this works, months with different number of days is not possible.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Time")
	TMap<ETimeUnit, int32> WrapToNextTimeUnit;

	// An optional display name for various times. This could be used to give each
	// day of the week a name, each month a name, etc. If used, the number of names
	// for a TimeUnit should equal its WrapToNextTimeUnit value. 7 days == 7 names.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Time")
	TMap<ETimeUnit, FTimeUnitNames> NamesOfTimeUnitIntervals;

	// When the timer ticks to a new minute, hour, day, etc., this map determines
	// if a TimeUnitAdvancedDelegate will be broadcast for that TimeUnit.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Time")
	TMap<ETimeUnit, bool> TimeUnitsToBroadcastEventFor;

	// When the timer ticks, increase this time unit by AmountIncreasedPerTimerTick.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Advance")
	ETimeUnit UnitIncreasedPerTimerTick = ETimeUnit::Minutes;

	// When the timer ticks, increase UnitIncreasedPerTimerTick by this amount.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Advance")
	int32 AmountIncreasedPerTimerTick = 5;

	// Tick the timer this often.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Advance")
	float RealWorldSecondsPerTimerTick = 2.5f;

	FTimerHandle TickHandle;

/// GETTERS AND SETTERS ///
public:
	// Returns the combination of CurrentTime and TimeUnitToOffset.
	UFUNCTION(BlueprintGetter)
	TMap<ETimeUnit, int32> GetCurrentTime() const;

	// GetCurrentTime for a specific TimeUnit.
	UFUNCTION(BlueprintCallable, Category = "Time")
	int32 GetCurrentTimeUnit(ETimeUnit TimeUnit) const;

/// DELEGATES ///
public:
	// The time has changed by some amount.
	UPROPERTY(BlueprintAssignable)
	FTimeChangedDelegate TimeChangedDelegate;
	// The timer has ticked and the time advanced by AmountIncreasedPerTimerTick.
	// You will get this event immediately after TimeChangedDelegate.
	UPROPERTY(BlueprintAssignable)
	FTimerTickedDelegate TimerTickedDelegate;
	// A time unit in the current time has advanced to a new value.
	UPROPERTY(BlueprintAssignable)
	FTimeUnitAdvancedDelegate TimeUnitAdvancedDelegate;
};
