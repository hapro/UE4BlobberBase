// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Enums.generated.h"

UENUM(BlueprintType)
enum class EUpdateValueType : uint8
{
	Set,
	Add
};

// When adding a status effect, this describes what to do if the status already exists.
UENUM(BlueprintType)
enum class EStatusOverwriteType : uint8
{
	// Keep the existing status no matter what.
	NoOverwrite,
	// Use the new status no matter what.
	AlwaysOverwrite,
	// Use the new status if its duration is longer than the current.
	OverwriteIfDurationEqualOrHigher,
	// Use the new status if its strength is higher than the current.
	OverwriteIfStrengthEqualOrHigher,
	// Use the new status if its both stronger and longer than the current.
	OverwriteIfStrengthAndDurationEqualOrHigher
};

// Intervals of time represented by UTimeManager, and potentially other systems too.
UENUM(BlueprintType)
enum class ETimeUnit : uint8
{
	Seconds,
	Minutes,
	Hours,
	Days,
	Weeks,
	Months,
	Years
};
