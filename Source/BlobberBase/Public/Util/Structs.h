// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Structs.generated.h"

class UMasteryLevel;

// A skill's proficiency is represented by a level and a mastery.
USTRUCT(BlueprintType)
struct FCharacterSkillInfo
{
	GENERATED_USTRUCT_BODY()

	FCharacterSkillInfo() {}
	FCharacterSkillInfo(int32 Skill, UMasteryLevel* Mastery) {
		SkillLevel = Skill;
		MasteryLevel = Mastery;
	}

	// The level of this skill.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 SkillLevel = 0;

	// The mastery of this skill.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UMasteryLevel* MasteryLevel = nullptr;
};
