#pragma once

#include "CoreMinimal.h"

#define LOG(message) if (GEngine) { GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT(message)); }
