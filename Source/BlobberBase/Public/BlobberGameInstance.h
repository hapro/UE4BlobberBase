// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "BlobberConfiguration.h"
#include "BlobberGameInstance.generated.h"

class UParty;
class UTimeManager;

/**
 * 
 */
UCLASS()
class BLOBBERBASE_API UBlobberGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UBlobberGameInstance();

	virtual void Init() override;

	// Callback for when BlobberConfiguration has finished loading
	UFUNCTION()
	void OnConfigurationInitialized();

	// Create Party, Managers, etc.
	UFUNCTION()
	void InitializeGameObjects();

	// Callback on the first tick after BeginPlay.
	void InitializeLevel();

	// Get BlobberGameInstance without the extra Cast.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Game Instance")
	static UBlobberGameInstance* GetBlobberGameInstance() { return BlobberGameInstance; }
	
	// BP to use for configuration settings.
	UPROPERTY(EditDefaultsOnly, NoClear, Category = "Config")
	TSubclassOf<UBlobberConfiguration> BlobberConfigurationClass;

	UFUNCTION(BlueprintGetter)
	UParty* GetParty() const { return Party; }

	UFUNCTION(BlueprintGetter)
	UBlobberConfiguration* GetBlobberConfiguration() const { return BlobberConfiguration; }

	UFUNCTION(BlueprintGetter)
	UTimeManager* GetTimeManager() const { return TimeManager; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Events")
	static bool BlobberEventsEnabled() { return bEnableBlobberEvents; }

	UFUNCTION(BlueprintCallable, Category = "Events")
	static void SetBlobberEventsEnabled(bool bEnable) { bEnableBlobberEvents = bEnable; }

protected:
	// Permanent reference to the player's party that persists across levels.
	// This pointer will persist even if the party members change or are loaded.
	UPROPERTY(BlueprintGetter = GetParty, Category = "Party")
	UParty* Party = nullptr;

	// Retrieve configuration settings.
	UPROPERTY(BlueprintGetter = GetBlobberConfiguration, Category = "Config")
	UBlobberConfiguration* BlobberConfiguration = nullptr;

	// The global time of day manager, if TimeManagerClass was set in BlobberConfiguration
	UPROPERTY(BlueprintGetter = GetTimeManager, Category = "Manager")
	UTimeManager* TimeManager = nullptr;

private:
	static UBlobberGameInstance* BlobberGameInstance;
	static bool bEnableBlobberEvents;
};
