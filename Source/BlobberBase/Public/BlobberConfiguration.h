// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Character/CharacterSheet.h"
#include "Character/Inventory.h"
#include "Character/Party.h"
#include "Character/PartyMember.h"
#include "Manager/TimeManager.h"
#include "BlobberConfiguration.generated.h"

DECLARE_DELEGATE(FConfigurationInitializedDelegate);

class UCharacterClass;
class UResource;

// Set a wide range of game-specific variables. Values are not meant to be
// modified after initialization. Used as a singleton inside BlobberGameInstance.
UCLASS(Blueprintable)
class BLOBBERBASE_API UBlobberConfiguration : public UObject
{
	GENERATED_BODY()
	
public:
	UBlobberConfiguration(const FObjectInitializer& Initializer);
	
	// Custom class to use for CharacterSheet.
	UPROPERTY(EditDefaultsOnly, NoClear, Category = "Classes")
	TSubclassOf<UCharacterSheet> CharacterSheetClass = nullptr;

	// Custom class to use for Party.
	UPROPERTY(EditDefaultsOnly, NoClear, Category = "Classes")
	TSubclassOf<UParty> PartyClass = nullptr;

	// Custom class to use for PartyMember.
	UPROPERTY(EditDefaultsOnly, NoClear, Category = "Classes")
	TSubclassOf<UPartyMember> PartyMemberClass = nullptr;

	// Custom class to use for Inventory.
	UPROPERTY(EditDefaultsOnly, NoClear, Category = "Classes")
	TSubclassOf<UInventory> InventoryClass = nullptr;

	// Custom class to use for TimeManager.
	UPROPERTY(EditDefaultsOnly, Category = "Classes")
	TSubclassOf<UTimeManager> TimeManagerClass = nullptr;

	// The resource to use to control when things die (eg Health).
	UPROPERTY(EditDefaultsOnly, NoClear, Category = "Classes")
	UResource* DefaultHealthResource = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Party")
	TArray<UCharacterClass*> CharacterCreateDefaultPartyMembers;

	UPROPERTY(EditDefaultsOnly, Category = "Party")
	TArray<UCharacterClass*> PIEDefaultPartyMembers;

	// Whether skill bonuses from status effects, equipment, etc. should
	// increase skills if the skill hasn't been learned yet. If false, an
	// unlearned skill will always return a skill level of 0.
	UPROPERTY(EditDefaultsOnly, Category = "Party Members")
	bool bAllowSkillModifiersOnUnlearnedSkills = false;

	// Run a series of unit tests to verify base functionality is correct.
	// WARNING: This will crash if certain default blueprints and other
	// variables are modified.
	UPROPERTY(EditDefaultsOnly, Category = "Debug")
	bool bRunUnitTests = true;

	// All MasteryLevel assets are scanned on initialization, and the
	// alphabetically-first asset with IntVal == -1 is chosen. If such an asset
	// does not exist, a default one is created.
	UPROPERTY(BlueprintReadOnly, Category = "Mastery Levels")
	UMasteryLevel* UnlearnedMasteryLevel = nullptr;

	// All MasteryLevel assets are scanned on initialization, and the
	// alphabetically-first asset with IntVal == 0 is chosen. If such an asset
	// does not exist, a default one is created.
	UPROPERTY(BlueprintReadOnly, Category = "Mastery Levels")
	UMasteryLevel* BasicMasteryLevel = nullptr;

	bool bIsInitialized = false;

	FConfigurationInitializedDelegate ConfigurationInitializedDelegate;

private:
	void OnRegistryLoaded();
};
