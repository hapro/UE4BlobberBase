// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data/GameInfoBase.h"
#include "MasteryLevel.generated.h"

// MasteryLevel represents how well a character knows a skill, which is
// different from skill level. Generally something like "Invalid", "Basic",
// "Expert", and "Master". When a character reaches a certain skill level,
// they can advance to a new mastery level.
UCLASS(BlueprintType)
class BLOBBERBASE_API UMasteryLevel : public UGameInfoBase
{
	GENERATED_BODY()
	
public:
	// An integer for programmers to use to indicate skill level. This can be used
	// in > or < checks (eg if SkillMasteryLevel.IntVal > BasicMasteryLevel.IntVal ).
	// -1 is recommended to be used as the "invalid" mastery level (the player cannot
	// learn the skill). 0 is recommended to be used as the "basic" mastery level. Certain
	// functionality will be disabled if this is not followed.
	// NOTE: This should NEVER appear on screen to the player.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Mastery")
	int32 IntVal = 0;

	// The minimum skill level required to achieve this mastery level.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Mastery")
	int32 SkillLevelAvailableAt = 0;
};
