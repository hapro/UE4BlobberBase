// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Spell.generated.h"

/**
 * 
 */
UCLASS()
class BLOBBERBASE_API USpell : public UObject
{
	GENERATED_BODY()
	
/// MEMBER VARIABLES ///
public:
	// The name of the asset to display to users.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FText DisplayName;

	// The description of the asset to display to users.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (MultiLine = true))
	FText Description;
	
	
};
