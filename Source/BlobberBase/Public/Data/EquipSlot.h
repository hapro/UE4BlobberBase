// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data/GameInfoBase.h"
#include "EquipSlot.generated.h"

// An asset for a specific equipment slot that may hold multiple
// types. "finger1" and "finger2" might be different slots, so the
// EquippableComponent should be sure to include all valid slots.
UCLASS(BlueprintType)
class BLOBBERBASE_API UEquipSlot : public UGameInfoBase
{
	GENERATED_BODY()
	
public:
};
