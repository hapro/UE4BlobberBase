// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data/GameInfoBase.h"
#include "EquipItemType.generated.h"

class USkill;
class UEquipSlot;

// Asset representing an item family that an Equippable belongs to. An optional
// skill can be added meaning that in order to equip an item of this type,
// the skill must be learned first.
UCLASS(BlueprintType)
class BLOBBERBASE_API UEquipItemType : public UGameInfoBase
{
	GENERATED_BODY()
	
public:
	// The skill that must be learned in order to equip items of this type.
	// A sword might require a "Sword" or "Melee" skill.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Equip")
	USkill* RequiredSkill = nullptr;

	// Where items of this type can be equipped.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Equip")
	TArray<UEquipSlot*> Slots;
};
