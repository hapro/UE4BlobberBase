// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Data/StatusEffect.h"
#include "Damage.generated.h"

class UCharacterSheet;
class UDamageInstanceBase;
class UDamageElement;
class UDamageStyle;
class UResource;

// Container class tht stores a list of DamageInstances and is processed
// by a CharacterSheet to apply all of its instances. Once this happens,
// the Damage object should be discarded.
UCLASS(Blueprintable, BlueprintType)
class BLOBBERBASE_API UDamage : public UObject
{
	GENERATED_BODY()

public:
	// Helper function to create an initialized Damage object.
	UFUNCTION(BlueprintCallable, Category = "Damage")
	static UDamage* CreateDamage(AActor* DamageCausingActor = nullptr,
								 UCharacterSheet* DamageCausingObject = nullptr);

	// Reset and re-initialize all variables.
	UFUNCTION(BlueprintCallable, Category = "Damage")
	void Init(AActor* DamageCausingActor = nullptr, UCharacterSheet* DamageCausingObject = nullptr);

	// Add a damage instance, accessible through the Instances variable. This
	// function automatically assigns DamageContainer on the Instance.
	UFUNCTION(BlueprintCallable, Category = "Damage")
	void AddDamageInstance(UDamageInstanceBase* Instance);

	// A list of all instances to apply when this Damage is processed.
	UPROPERTY(BlueprintReadOnly, Category = "Damage")
	TArray<UDamageInstanceBase*> Instances;

	// The actor that triggered this damage. This may be null if
	// DamageCausingObject is used instead.
	UPROPERTY(BlueprintReadWrite, Category = "Damage")
	AActor* DamageCausingActor = nullptr;

	// The character that triggered this damage. This may be null if
	// DamageCausingActor is used instead.
	UPROPERTY(BlueprintReadWrite, Category = "Damage")
	UCharacterSheet* DamageCausingObject = nullptr;
};

// Base class for some sort of on-hit effect, whether it's damage, status, etc.
UCLASS(abstract, Blueprintable, BlueprintType)
class BLOBBERBASE_API UDamageInstanceBase : public UObject
{
	GENERATED_BODY()
	
public:
	// Do something to a target character when executed. This could set the
	// target's health ot a new value, give them a status effect, or whatever
	// else you can think of.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Damage")
	void ApplyDamageInstance(UCharacterSheet* Target);
	virtual void ApplyDamageInstance_Implementation(UCharacterSheet* Target) {}

	// Get a reference to the Damage object that holds this instance.
	UPROPERTY(BlueprintReadOnly, Category = "Damage")
	TWeakObjectPtr<UDamage> DamageContainer = nullptr;
};

// Damage that modify the target's health after some mitigation.
UCLASS()
class BLOBBERBASE_API UDamageInstance : public UDamageInstanceBase
{
	GENERATED_BODY()

public:
	virtual void ApplyDamageInstance_Implementation(UCharacterSheet* Target) override;

	// Helper function to create an initialized DamageInstance object.
	UFUNCTION(BlueprintCallable, Category = "Damage")
	static UDamageInstance* CreateDamageInstance(UDamageElement* DamageElement = nullptr,
												 UDamageStyle* DamageStyle = nullptr,
												 float DamageValue = 0.0f, float Multiplier = 1.0f,
												 UResource* ResourceToDamage = nullptr,
												 bool bIsFeedback = false);

	// Reset and re-initialize all variables.
	UFUNCTION(BlueprintCallable, Category = "Damage")
	void Init(UDamageElement* DamageElement = nullptr, UDamageStyle* DamageStyle = nullptr,
			  float DamageValue = 0.0f, float Multiplier = 1.0f,
			  UResource* ResourceToDamage = nullptr, bool bIsFeedback = false);

	// The resource to adjust. If nullptr/None, BlobberConfiguration's
	// DefaultHealthResource will be used instead.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
	UResource* ResourceToDamage = nullptr;

	// Which type of damage this will inflict.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
	UDamageElement* DamageElement = nullptr;

	// How this damage was generated.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
	UDamageStyle* DamageStyle = nullptr;

	// The amount of damage to inflict.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
	float DamageValue = 0.0f;

	// A multiplier to apply to DamageValue before inflicting damage. This can be
	// overridden to either apply Multiplier before or after mitigation.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
	float Multiplier = 1.0f;

	// Whether this damage is "feedback" damage or not. If a status effect or some
	// other method inflicts damage upon receiving damage, this can be used to
	// stop infinite loops.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
	bool bIsFeedback = false;
};

// A status effect that can be inflicted with some percent chance of success.
UCLASS()
class BLOBBERBASE_API UStatusInstance : public UDamageInstanceBase
{
	GENERATED_BODY()

public:
	virtual void ApplyDamageInstance_Implementation(UCharacterSheet* Target) override;

	// Helper function to create an initialized StatusInstance object.
	UFUNCTION(BlueprintCallable, Category = "Damage")
	static UStatusInstance* CreateStatusInstance(TSubclassOf<UStatusEffect> Status, int32 StatusStrength = 1,
												 float StatusDuration = 1.0f, int32 Accuracy = 100);

	// Reset and re-initialize all variables.
	UFUNCTION(BlueprintCallable, Category = "Damage")
	void Init(TSubclassOf<UStatusEffect> Status, int32 StatusStrength = 1,
			  float StatusDuration = 1.0f, int32 Accuracy = 100);

	// The status effect to inflict.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
	TSubclassOf<UStatusEffect> Status;

	// The strength of the status effect, if it is inflicted.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
	int32 StatusStrength = 1;

	// The duration of the status effect, if it is inflicted.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
	float StatusDuration = 1.0f;

	// The percent chance (from 1 to 100) of inflicting this status. This can be
	// overridden and reused for different functionality, such as doing a saving
	// throw vs a specific stat.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
	int32 Accuracy = 100;
};
