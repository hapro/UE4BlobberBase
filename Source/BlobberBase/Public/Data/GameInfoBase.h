// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "GameInfoBase.generated.h"

// Base asset for all BlobberBase config assets. Contains
// no functionality besides common UPROPERTY members.
UCLASS(Abstract, Blueprintable)
class BLOBBERBASE_API UGameInfoBase : public UDataAsset
{
	GENERATED_BODY()
	
public:
	// The name of the asset to display to users.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FText DisplayName;

	// The description of the asset to display to users.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (MultiLine = true))
	FText Description;
};
