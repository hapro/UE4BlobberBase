// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data/GameInfoBase.h"
#include "DamageElement.generated.h"

class UStat;

// An element that can be optionally resisted by some stat. The mitigation
// function can be overridden in CharacterSheet.
UCLASS(BlueprintType)
class BLOBBERBASE_API UDamageElement : public UGameInfoBase
{
	GENERATED_BODY()
	
public:
	// When damage of this element is dealt, this stat is used to mitigate it.
	// The mitigation function can be overridden in CharacterSheet.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Resistance")
	UStat* ResistedBy = nullptr;
};
