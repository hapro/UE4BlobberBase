// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Util/Structs.h"
#include "StatusEffect.generated.h"

class UCharacterSheet;
class UParty;

// A status effect to act on either an individual character or the entire party.
// A number of events can be overridden for the status effect to trigger code on,
// or a status effect can be queried elsewhere from the player - for instance,
// when getting an armor stat, check for the stoneskin status.
UCLASS(BlueprintType, Blueprintable)
class BLOBBERBASE_API UStatusEffect : public UObject
{
	GENERATED_BODY()
	
public:
	// Initialize the status effect with values. Should only be called immediately
	// after creation (otherwise use the Set functions). Either InCharacterSheet or
	// InParty should be set, but not both. You should rely on this pointer rather
	// than PartyStatus to decide whether this status is acting on a character or
	// party (because you might have it on an enemy which doesn't have a party).
	UFUNCTION(BlueprintCallable, Category = "Status")
	void Init(int32 NewStrength, float NewDuration, UCharacterSheet* InCharacterSheet = nullptr, UParty* InParty = nullptr);

	// Tick this status, reducing its duration and calling OnStatusTick, potentially
	// removing the status if its time is up. DeltaTime is not the same as in Actor's
	// Tick function - for a status effect, DeltaTime can be whatever concept you want
	// to keep track of how long they last. By default, DeltaTime will be equal to
	// TimeManager's value per tick.
	UFUNCTION(BlueprintCallable, Category = "Status")
	void Tick(float DeltaTime);

	// Checks if this status effect is permanent or not. Any status with a duration
	// of -1.0 is considered permanent. You should be careful about checking this
	// value after a status' final tick when it has been queued for removal, as its
	// duration could be less than -1 even though its not permanent.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Status")
	bool IsPermanent() const { return TimeRemaining <= -1.0f; }

	// Callback after the status has been added to its target.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Status")
	void OnStatusBegin();
	virtual void OnStatusBegin_Implementation() {}

	// Callback just before the status has been removed from its target.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Status")
	void OnStatusEnd();
	void OnStatusEnd_Implementation() {}

	// Callback every time Tick is called, after its duration has updated.
	UFUNCTION(BlueprintNativeEvent)
	void OnStatusTick(float DeltaTime);
	virtual void OnStatusTick_Implementation(float DeltaTime) {}

	// Callback for when the status's duration or strength has been updated.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Status")
	void OnStatusUpdated(int32 OldStrength, int32 NewStrength, float OldTimeRemaining, float NewTimeRemaining);
	virtual void OnStatusUpdated_Implementation(int32 OldStrength, int32 NewStrength, float OldTimeRemaining, float NewTimeRemaining) {}

	// Called from the default implementation of CharacterSheet::GetStat().
	// Given a stat and a current Value, returns the new value that the stat
	// should be set to.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Status")
	int32 ModifyGetStat(UStat* Stat, int32 Value);
	virtual int32 ModifyGetStat_Implementation(UStat* Stat, int32 Value) { return Value; }

	// Called from the default implementation of CharacterSheet::GetSkill().
	// Given a skill and a current Value, returns the new value that the skill
	// should be set to. Generally, only the SkillLevel should be being updated.
	UFUNCTION(BlueprintNativeEvent)
	FCharacterSkillInfo ModifyGetSkill(USkill* Skill, FCharacterSkillInfo& Value);
	virtual FCharacterSkillInfo ModifyGetSkill_Implementation(USkill* Skill, FCharacterSkillInfo& Value) { return Value; }

/// MEMBER VARIABLES ///
public:
	// The name of the asset to display to users.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FText DisplayName;

	// The description of the asset to display to users.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (MultiLine = true))
	FText Description;

	// If true, this status is meant to affect the entire party at once. This is
	// not a strict guideline however, as party statuses can still technically be
	// placed on individual characters if desired.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	bool bPartyStatus = false;

	// If true, when the character dies, the status will automatically be
	// removed. This does not do anything if it is a PartyStatus, and does
	// not remove when the character becomes unconscious (unless they also die).
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	bool bRemoveOnDeath = true;

	// If this status is acting on a character, this points to that character.
	// If this is null, then Party should not be.
	UPROPERTY(BlueprintReadOnly, Category = "Status")
	UCharacterSheet* CharacterSheet = nullptr;

	// If this status is acting on a party, this points to that party.
	// If this is null, then CharacterSheet should not be.
	UPROPERTY(BlueprintReadOnly, Category = "Status")
	UParty* Party = nullptr;

protected:
	// How much time is left until this status expires. The units that this represents
	// can be anything desired as long as it matches with whatever units is being
	// passed through the Tick function. By default, it's recommended to sync this
	// with the TimeManager's tick amount.
	UPROPERTY(BlueprintGetter = GetTimeRemaining, BlueprintSetter = SetTimeRemaining, Category = "Status")
	float TimeRemaining = 0.0f;

	// How strong this status is, whose purpose will vary depending on what the status
	// actually does. A poison effect with strength 5 might remove 5 health per tick.
	UPROPERTY(BlueprintGetter = GetStrength, BlueprintSetter = SetStrength, Category = "Status")
	int32 Strength = 0;

/// GETTERS AND SETTERS ///
public:
	UFUNCTION(BlueprintGetter)
	float GetTimeRemaining() const { return TimeRemaining; }

	UFUNCTION(BlueprintSetter)
	void SetTimeRemaining(float NewDuration);

	UFUNCTION(BlueprintGetter)
	int32 GetStrength() const { return Strength; }

	UFUNCTION(BlueprintSetter)
	void SetStrength(int32 NewStrength);

	// Refresh this status with a new strength and duration.
	UFUNCTION(BlueprintCallable, Category = "Status")
	void UpdateStatus(int32 NewStrength, float NewDuration);
};
