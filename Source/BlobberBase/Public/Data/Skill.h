// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameInfoBase.h"
#include "Skill.generated.h"

class UMasteryLevel;

// A skill that a character can learn. Skills can either be individual skills
// or party skills. Skills are assigned a UMasteryLevel when learned by
// a character.
UCLASS(BlueprintType)
class BLOBBERBASE_API USkill : public UGameInfoBase
{
	GENERATED_BODY()
	
public:
	USkill(const FObjectInitializer& Initializer);

#if WITH_EDITOR
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

	// A Party Skill means that when a skill check arises, the game
	// will automatically find the party member with the highest skill
	// level and mastery level for that skill to use for the check.
	// If not a party skill, the currently-active member's skill is used.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Skill")
	bool bIsPartySkill = false;

	// Text descriptions of the benefits of each mastery level for this skill, for
	// display in the UI.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Skill")
	TMap<UMasteryLevel*, FText> MasteryDescriptions;

	// Reinitialize MasteryDescriptions in case some entries are missing or broken.
	// Generally, this shouldn't be necessary as everything should update automatically.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Debug", AdvancedDisplay)
	bool bForceResync = false;

private:
	void InitMasteryDescriptions();

	// Asset Manager callbacks.
	void OnRegistryLoaded();
	void OnAssetAdded(const FAssetData& Asset);
	void OnAssetRemoved(const FAssetData& Asset);
};
