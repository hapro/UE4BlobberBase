// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data/GameInfoBase.h"
#include "DamageStyle.generated.h"

// The medium of which this damage was transmitted. Melee, ranged,
// spell, other, etc.
UCLASS(BlueprintType)
class BLOBBERBASE_API UDamageStyle : public UGameInfoBase
{
	GENERATED_BODY()
	
public:
};
