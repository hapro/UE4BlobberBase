// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameInfoBase.h"
#include "Stat.generated.h"

// A stat for a character. Can have many functions depending on
// what the game creator chooses.
UCLASS(BlueprintType)
class BLOBBERBASE_API UStat : public UGameInfoBase
{
	GENERATED_BODY()
	
public:
	// Whether stat points can be used to increase this stat.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Stats")
	bool bCanUpgradeWithStatPoints = true;
};
