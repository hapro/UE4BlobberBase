// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameInfoBase.h"

#include "IAssetRegistry.h"

#include "CharacterClass.generated.h"

class UMasteryLevel;
class UResource;
class USkill;
class UStat;

// Helper struct to store all stat and resource info together for a UCharacterClass.
USTRUCT(BlueprintType)
struct FClassStatInfo
{
	GENERATED_USTRUCT_BODY()

	// The initial starting value for this stat or resource.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 DefaultValue = 0;

	// How much the stat/resource should increase automatically upon leveling up.
	// For stats, the float value will be truncated to an integer and added to
	// generate the new stat value. Then, the remaining decimals will be
	// cumulatively added each level up. When this accumulation reaches 1.0,
	// the character will gain 1 extra point.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float GainPerLevel = 0.0f;
};

// A class that a UCharacterSheet can be assigned. Using the Asset Manager,
// certain member variables will automatically be adjusted, along with the CDO,
// as other DataAssets are created and deleted.
UCLASS(BlueprintType)
class BLOBBERBASE_API UCharacterClass : public UGameInfoBase
{
	GENERATED_BODY()
	
public:
	UCharacterClass(const FObjectInitializer& Initializer);
	
	// Reinitialize StatInfo and MaxSkillMastery in case some entries are missing or broken.
	// Generally, this shouldn't be necessary as everything should update automatically.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Debug", AdvancedDisplay)
	bool bForceResync = false;

	// If this class is a promotion from another class, specify that class here. This is not
	// strictly necessary in order to set a new class, but it enables some convenience functions.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Class", meta = (DisplayThumbnail = "false"))
	UCharacterClass* PrerequisiteClass = nullptr;

	// Data regarding stats for this class
	// WARNING: Entries should not be added, modified, or removed from this map. Adding
	// new Stats will update the map automatically. Use ForceResync if there is a problem.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Stats", meta = (DisplayThumbnail = "false"))
	TMap<UStat*, FClassStatInfo> StatInfo;

	// Data regarding resources for this class
	// WARNING: Entries should not be added, modified, or removed from this map. Adding
	// new Resources will update the map automatically. Use ForceResync if there is a problem.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Stats", meta = (DisplayThumbnail = "false"))
	TMap<UResource*, FClassStatInfo> ResourceInfo;

	// The highest level of mastery this class can achieve in each skill.
	// On initialization, All MasteryLevel assets will be searched, and if one is found with an
	// IntVal of -1, it will be used to replace all skills set to null/None. -1 means "unable to learn".
	// WARNING: Entries should not be added, modified, or removed from this map. Adding
	// new Skills will update the map automatically. Use ForceResync if there is a problem.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Skills", meta = (DisplayThumbnail = "false"))
	TMap<USkill*, UMasteryLevel*> MaxSkillMastery;

	// A list of skills for this class to start with, and the initial mastery in each of those skills.
	// Generally, each skill should start with a "basic" mastery whose IntVal is 0.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Skills", meta = (DisplayThumbnail = "false"))
	TArray<USkill*> StartingSkills;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
#endif
	// After BlobberConfiguration initializes, replace any null MasteryLevels with a default one.
	static void AllClassesSetDefaultMaxSkillMastery(UMasteryLevel* MasteryLevel);

	// Computes the bonus stats for the requested level, taking decimals into account.
	UFUNCTION(BlueprintCallable, Category = "Stats")
	static int32 GetStatGainForLevel(float GainPerLevel, int32 Level);
	// Computes the cumulative bonus for a stat for the requested level, taking decimals into account.
	UFUNCTION(BlueprintCallable, Category = "Stats")
	static int32 GetCumulativeStatGainForLevel(float GainPerLevel, int32 Level);
	// Computes the cumulative bonus for a resource for the requested level.
	UFUNCTION(BlueprintCallable, Category = "Stats")
	static float GetCumulativeResourceGainForLevel(float GainPerLevel, int32 Level);

private:
	// Initialize all fields requiring data from the asset manager.
	void InitAllFields();

	// Asset Manager callbacks.
	void OnRegistryLoaded();
	void OnAssetAdded(const FAssetData& Asset);
	void OnAssetRemoved(const FAssetData& Asset);
};
