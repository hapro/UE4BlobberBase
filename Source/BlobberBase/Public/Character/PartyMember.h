// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Character/Party.h"
#include "Util/Enums.h"
#include "Util/Structs.h"
#include "PartyMember.generated.h"

class UCharacterClass;
class UCharacterSheet;
class UMasteryLevel;
class UParty;
class USkill;
class UStat;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FLevelUpDelegate, UPartyMember*, PartyMember, int32, Level);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FCharacterClassChangedDelegate, UPartyMember*, PartyMember, UCharacterClass*, CharacterClass);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FExperienceChangedDelegate, UPartyMember*, PartyMember, int32, CurrentExperience);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FStatPointsChangedDelegate, UPartyMember*, PartyMember, int32, CurrentStatPoints);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FSkillPointsChangedDelegate, UPartyMember*, PartyMember, int32, CurrentSkillPoints);

// Container for a CharacterSheet, plus some additional information that the
// player characters need but may not apply to enemies.
UCLASS(BlueprintType, Blueprintable)
class BLOBBERBASE_API UPartyMember : public UObject
{
	GENERATED_BODY()
	
public:
	UPartyMember(const FObjectInitializer& Initializer);

	/// INITIALIZATION ///

	void InitPrivate(UCharacterClass* CharacterClass, UParty* InParty, int32 Index);

	// Called after this PartyMember has finished initializing, but
	// not all PartyMembers in the Party may be finished at this point.
	// Any additional setup should be done here.
	UFUNCTION(BlueprintNativeEvent)
	void Init();
	virtual void Init_Implementation() {}

	/// EXPERIENCE ///

	// Give this PartyMember some experience, and then level up if possible.
	// If adding experience, override ModifyExperience in order to change the
	// value of Amount before it is added as experience. When setting,
	// ModifyExperience is not called. The level up process can be skipped by
	// setting LevelUp to false
	UFUNCTION(BlueprintCallable, Category = "Level")
	void SetExperience(int32 Amount, bool bLevelUp = true, EUpdateValueType UpdateType = EUpdateValueType::Add);

	// When GiveExperience is called, ModifyExperience has a chance to tweak
	// the amount of experience that will be gained before it is added to the
	// party member. Given an input amount of experience, returns how much
	// experience should actually be given to the party member.
	// The default implementation returns the same value.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintPure, Category = "Level")
	int32 ModifyExperience(int32 Amount) const;
	int32 ModifyExperience_Implementation(int32 Amount) const { return Amount; }

	// Calculate and return the experience required to reach the next level.
	// This value should only return the amount necessary to go from X to X+1
	// (ie it is not cumulative).
	// The default implementation returns CurrentLevel^2 * 1000.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Level")
	int32 GetExperienceRequiredForNextLevel() const;
	int32 GetExperienceRequiredForNextLevel_Implementation() const;

	/// LEVEL UP ///

	// When Experience >= GetExperienceRequiredForNextLevel, the party member
	// levels up and receives each stat's GainPerLevel as a bonus. They also
	// receive skill and stat points defined by Get*PointsForLevel.
	// Additional bonuses may be added by overriding this function. When
	// overriding, it is recommended to call the parent implementation in order
	// for the OnLevelUp event to be triggered. CharacterSheet->LevelUp should
	// also be called to increase the actual level number variable.
	// When leveling up, the party member's Experience is reduced by the amount
	// necessary to level up. This is because Blueprints don't allow for higher
	// than 32-bit integers, so very high values might otherwise break.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Level")
	void LevelUp();
	virtual void LevelUp_Implementation();

	// Amount to add to StatPoints upon reaching a new level. The new level
	// is supplied as a parameter. Returns the number of stat points to gain.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Stats")
	int32 GetStatPointsForLevel(int32 Level) const;
	virtual int32 GetStatPointsForLevel_Implementation(int32 Level) const;

	// Amount to add to SkillPoints upon reaching a new level. The new level
	// is supplied as a parameter. Returns the number of skill points to gain.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Skills")
	int32 GetSkillPointsForLevel(int32 Level) const;
	virtual int32 GetSkillPointsForLevel_Implementation(int32 Level) const;

	/// UPGRADES ///

	// Add or remove stat points, not to be confused with CharacterSheet->SetBaseStat.
	// Cannot be set below 0.
	UFUNCTION(BlueprintCallable, Category = "Stats")
	void SetStatPoints(int32 Amount, EUpdateValueType UpdateType = EUpdateValueType::Add);

	// Add or remove skill points, not to be confused with CharacterSheet->SetBaseSkill.
	// Cannot be set below 0.
	UFUNCTION(BlueprintCallable, Category = "Skills")
	void SetSkillPoints(int32 Amount, EUpdateValueType UpdateType = EUpdateValueType::Add);
	
	// Check to see if a stat can be increase with the current amount of
	// stat points that the party member has. Override GetCostToIncreaseStat
	// to customize the amount required.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Stats")
	bool CanIncreaseStatWithStatPoints(UStat* Stat) const;

	// Check to see if a skill can be increase with the current amount of
	// skill points that the party member has. Override GetCostToIncreaseSkill
	// to customize the amount required.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Skills")
	bool CanIncreaseSkillWithSkillPoints(USkill* Skill) const;

	// Returns the number of stat points required to permanently increase
	// the supplied stat by 1 point.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintPure, Category = "Stats")
	int32 GetCostToIncreaseStat(UStat* Stat) const;
	virtual int32 GetCostToIncreaseStat_Implementation(UStat* Stat) const;

	// Returns the number of skill points required to permanently increase
	// the supplied skill by 1 point.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintPure, Category = "Skills")
	int32 GetCostToIncreaseSkill(USkill* Skill) const;
	virtual int32 GetCostToIncreaseSkill_Implementation(USkill* Skill) const;

	// Permanently increase a stat by 1 using StatPoints. The number of stat
	// points used is determined by GetCostToIncreaseStat. If there are not
	// enough points, false will be returned and no stats will change.
	UFUNCTION(BlueprintCallable, Category = "Stats")
	bool IncreaseStatWithStatPoints(UStat* Stat);

	// Permanently increase a skill by 1 using SkillPoints. The number of skill
	// points used is determined by GetCostToIncreaseSkill. If there are not
	// enough points, false will be returned and no skills will change.
	UFUNCTION(BlueprintCallable, Category = "Skills")
	bool IncreaseSkillWithSkillPoints(USkill* Skill);

	// Returns true if the supplied skill can reach the supplied mastery level.
	// Default implementation requires that the new mastery's IntVal is exactly
	// 1 higher than the current mastery (unlearned IntVal == -1). It also checks
	// to make sure the party member's class allows learning the skill at the
	// requested mastery, and that the party member's skill level is high enough.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintPure, Category = "Skills")
	bool CanIncreaseSkillMastery(USkill* Skill, UMasteryLevel* Mastery) const;
	virtual bool CanIncreaseSkillMastery_Implementation(USkill* Skill, UMasteryLevel* Mastery) const;

	// Increase a skill's mastery to a new level, if CanIncreaseSkillMastery
	// returns true. If the mastery was increased, this function returns true.
	UFUNCTION(BlueprintCallable, Category = "Skills")
	bool IncreaseSkillMastery(USkill* Skill, UMasteryLevel* Mastery);

	// Change a party member's class. If bRequirePrerequisiteClass, then the
	// current class must equal the new class's PrerequisiteClass value. Upon
	// assigning the new class, all of the new class's StartingSkills will be
	// automatically learned. If ReadjustStats is true, all base stats will be
	// modified by the new class's GainPerLevel and DefaultValue per stat. For
	// example, if the new class's Gain is 4 and the old class's is 1, the stat
	// will immediately be increased by an additional 3 for each level. Returns
	// true if the class was changed.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Class")
	bool PromoteClass(UCharacterClass* NewClass, bool bRequirePrerequisiteClass = true,
					  bool bLearnStartingSkills = true, bool bReadjustStats = true);
	virtual bool PromoteClass_Implementation(UCharacterClass* NewClass, bool bRequirePrerequisiteClass = true,
											 bool bLearnStartingSkills = true, bool bReadjustStats = true);

/// MEMBER VARIABLES ///
protected:
	// The class of this party member. Be careful as calling SetCharacterClass
	// directly will not automatically adjust stats or skills like PromoteClass
	// does.
	UPROPERTY(BlueprintGetter = GetCharacterClass, BlueprintSetter = SetCharacterClass, Category = "Class")
	UCharacterClass* CharacterClass = nullptr;

	// The character sheet of this party member.
	UPROPERTY(BlueprintGetter = GetCharacterSheet, Category = "Character Sheet")
	UCharacterSheet* CharacterSheet = nullptr;

	// The amount of experience the party member has gained for the current level.
	// Use SetExperience instead of setting this directly.
	UPROPERTY(BlueprintGetter = GetExperience, Category = "Level")
	int32 Experience = 0;

	// The available skill points used by IncreaseSkillWithSkillPoints.
	UPROPERTY(BlueprintGetter = GetSkillPoints, Category = "Level")
	int32 SkillPoints = 0;

	// The available stat points used by IncreaseStatWithStatPoints.
	UPROPERTY(BlueprintGetter = GetStatPoints, Category = "Level")
	int32 StatPoints = 0;

private:
	// A convenience reference back to the Party this object is a member of.
	TWeakObjectPtr<UParty> Party = nullptr;

	// A convenience value equivalent to Party->GetIndexOfPartyMember(this).
	UPROPERTY(BlueprintGetter = GetIndexInParty, Category = "Party")
	int32 IndexInParty = -1;

/// GETTERS AND SETTERS ///
public:
	UFUNCTION(BlueprintGetter)
	UCharacterClass* GetCharacterClass() const { return CharacterClass; }

	UFUNCTION(BlueprintSetter)
	void SetCharacterClass(UCharacterClass* InCharacterClass);

	UFUNCTION(BlueprintGetter)
	UCharacterSheet* GetCharacterSheet() const { return CharacterSheet; }

	UFUNCTION(BlueprintGetter)
	int32 GetExperience() const { return Experience; }

	UFUNCTION(BlueprintGetter)
	int32 GetSkillPoints() const { return SkillPoints; }

	UFUNCTION(BlueprintGetter)
	int32 GetStatPoints() const { return StatPoints; }

	// A convenience reference back to the Party this object is a member of.
	UFUNCTION(BlueprintCallable, Category = "Party")
	UParty* GetParty() const { return Party.Get(); }

	UFUNCTION(BlueprintGetter)
	int32 GetIndexInParty() const { return IndexInParty; }

/// DELEGATES ///
public:
	// Called when the party member levels up.
	UPROPERTY(BlueprintAssignable)
	FLevelUpDelegate LevelUpDelegate;
	// Called when the party member's class is updated.
	UPROPERTY(BlueprintAssignable)
	FCharacterClassChangedDelegate ClassChangedDelegate;
	// Called when the party member gains experience.
	UPROPERTY(BlueprintAssignable)
	FExperienceChangedDelegate ExperienceChangedDelegate;
	// Called when the party member's stat points change.
	UPROPERTY(BlueprintAssignable)
	FStatPointsChangedDelegate StatPointsChangedDelegate;
	// Called when the party member's skill points change.
	UPROPERTY(BlueprintAssignable)
	FSkillPointsChangedDelegate SkillPointsChangedDelegate;
};
