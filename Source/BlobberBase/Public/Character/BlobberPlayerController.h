// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BlobberPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class BLOBBERBASE_API ABlobberPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ABlobberPlayerController(const FObjectInitializer& Initializer);

protected:
	virtual void SetupInputComponent() override;

	void MoveForward(float Val);
	void MoveRight(float Val);
	void TurnUp(float Val);
	void TurnRight(float Val);

	void ExitGame();
};
