// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Inventory.generated.h"

class UCharacterSheet;
class UItem;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FInventoryChangedDelegate, UInventory*, Inventory);

// An array of items held by the player. Items can be limited by number of
// items held, weight of items held, or subclassed to introduce new ways of
// storing the items (like a grid inventory).
UCLASS(Blueprintable, BlueprintType)
class BLOBBERBASE_API UInventory : public UObject
{
	GENERATED_BODY()
	
public:
	UInventory();
	
	// Add an item to the inventory. By default, this just appends it to the
	// array if CanAddItemToInventory returns true. This function also updates
	// InventoryWeight when items are added.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Inventory")
	bool AddItemToInventory(UItem* Item);
	virtual bool AddItemToInventory_Implementation(UItem* Item);

	// Whether or not an item can be added to the inventory. The default
	// implementation checks MaxInventoryItems and GetMaxInventoryWeight to
	// make sure both conditions are not exceeded.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Inventory")
	bool CanAddItemToInventory(UItem* Item);
	virtual bool CanAddItemToInventory_Implementation(UItem* Item);

	// Remove an item from the inventory at the specified index. This should
	// generally be used instead of modifying Inventory directly, as this also
	// updates InventoryWeight and broadcasts an event.
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	bool RemoveItemFromInventoryAt(int32 Index);

	// Remove an item from the inventory. This should generally be used instead
	// of modifying Inventory directly, as this also updates InventoryWeight and
	// broadcasts an event.
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	bool RemoveItemFromInventory(UItem* Item);

	// Search the inventory for an item by name, and return the first item found
	// with that name, or null/None if not found. Note that ItemName is used, not
	// ItemDisplayName.
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	UItem* FindItemInInventoryByName(const FString& Name) const;

	// Retrieve an item from the inventory at the specified index.
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	UItem* FindItemInInventoryAt(int32 Index) const;

	// Sort the inventory by some algorithm. By default, this sorts items by their
	// ItemName (not ItemDisplayName). In an actual game, an enum should probably
	// be made and then this function will run a sort algorithm depending on
	// the value of the enum, whether it be for name, weight, rarity, etc.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Inventory")
	void SortInventory();
	virtual void SortInventory_Implementation();

	// Returns the maximum weight this party member can hold in their inventory.
	// By default, -1 is returned, meaning an infinite weight can be held.
	// Note that equipped items may or may not contribute to InventoryWeight
	// depending on implementation.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Inventory")
	float GetMaxInventoryWeight() const;
	virtual float GetMaxInventoryWeight_Implementation() const { return -1.0f; }

/// MEMBER VARIABLES ///
protected:
	UPROPERTY()
	UCharacterSheet* CharacterSheet = nullptr;

	// The maximum number of items that can be held in the inventory. -1
	// indicates that there is no limit to the number of items. Note that if
	// this value is modified during runtime to a value less than the current
	// number of inventory items, nothing will happen. You are responsible
	// for choosing which items to remove, if at all.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Inventory")
	int32 MaxInventoryItems = -1;

	// The current weight of all inventory items, cached to avoid recomputing.
	// This is updated automatically in AddItemToInventory and
	// RemoveItemFromInventory.
	UPROPERTY(BlueprintReadWrite, BlueprintGetter = GetInventoryWeight, Category = "Inventory")
	float InventoryWeight = 0.0f;

	// All items held by the party member. Equipment may or may not be part
	// of the inventory, at the developer's discretion. This array should not
	// be modified directly unless you're sure you know what you're doing.
	// Otherwise, use AddItemToInventory and RemoveItemFromInventory. Any
	// modifications must also update InventoryWeight.
	UPROPERTY(BlueprintReadWrite, Category = "Inventory")
	TArray<UItem*> Inventory;

/// GETTERS AND SETTERS ///
public:
	UFUNCTION(BlueprintGetter)
	float GetInventoryWeight() const { return InventoryWeight; }

/// DELEGATES ///
public:
	// An item was added or removed from the inventory.
	UPROPERTY(BlueprintAssignable)
	FInventoryChangedDelegate InventoryChangedDelegate;
};
