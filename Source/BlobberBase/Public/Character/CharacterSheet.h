// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Character/PartyMember.h"
#include "Data/StatusEffect.h"
#include "Util/Enums.h"
#include "Util/Structs.h"
#include "CharacterSheet.generated.h"

class UCharacterClass;
class UDamage;
class UDamageInstance;
class UEquipSlot;
class UInventory;
class UItem;
class UMasteryLevel;
class UPartyMember;
class UResource;
class USkill;
class UStat;
class UTimeManager;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FResourceChangedDelegate, UCharacterSheet*, CharacterSheet, UResource*, Resource, float, CurrentValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FBaseMaxResourceChangedDelegate, UCharacterSheet*, CharacterSheet, UResource*, Resource, float, NewMax);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FBaseStatChangedDelegate, UCharacterSheet*, CharacterSheet, UStat*, Stat, int32, CurrentValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FBaseSkillLevelChangedDelegate, UCharacterSheet*, CharacterSheet, USkill*, Skill, int32, CurrentValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FBaseSkillMasteryChangedDelegate, UCharacterSheet*, CharacterSheet, USkill*, Skill, UMasteryLevel*, CurrentMastery);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FStatusBeginDelegate, UCharacterSheet*, CharacterSheet, UStatusEffect*, Status);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FStatusUpdatedDelegate, UCharacterSheet*, CharacterSheet, UStatusEffect*, Status);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FStatusEndDelegate, UCharacterSheet*, CharacterSheet, UStatusEffect*, Status);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FEquipmentChangedDelegate, UCharacterSheet*, CharacterSheet, UEquipSlot*, Slot, UItem*, NewItem, UItem*, OldItem);

// How alive the character currently is.
UENUM(BlueprintType)
enum class EConsciousness : uint8
{
	// Super dead, when HP <= GetDeathThreshold().
	Dead,
	// Mostly dead, when HP <= 0.
	Unconscious,
	// Not dead, when HP > 0.
	Alive
};
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FConsciousnessChangedDelegate, UCharacterSheet*, CharacterSheet, EConsciousness, NewStatus);

// Stores all common data for all characters in the game, including
// health, stats, skills, etc. 
UCLASS(BlueprintType, Blueprintable)
class BLOBBERBASE_API UCharacterSheet : public UObject
{
	GENERATED_BODY()
	
public:
	UCharacterSheet(const FObjectInitializer& Initializer);

	// Returned for invalid cases such as requesting an unknown skill.
	// Has IntVal == -1.
	static FCharacterSkillInfo NoSkillInfo;

	/// INITIALIZATION ///

	virtual void InitPrivate(UCharacterClass* CharacterClass, UPartyMember* InPartyMember = nullptr);

	// Called after all fields for this object have finished initializing.
	// Any additional setup should be done here.
	UFUNCTION(BlueprintNativeEvent, Category = "Character Sheet")
	void Init();
	virtual void Init_Implementation() {}

	/// DAMAGE ///

	// Populate a Damage object with DamageInstances from a given slot. If the
	// Damage passed in is nullptr/None, a new Damage object will be created.
	// If the slot is empty or has no DamageComponent, no damage instances will
	// be created.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Damage")
	UDamage* GenerateDamageInstancesForSlot(UEquipSlot* Slot, UDamageStyle* Style, UDamage* Damage = nullptr);
	virtual UDamage* GenerateDamageInstancesForSlot_Implementation(UEquipSlot* Slot, UDamageStyle* Style, UDamage* Damage = nullptr);

	// Processes a damage object, calling ApplyDamageInstance on each instance.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Damage")
	void TakeDamage(UDamage* Damage);
	virtual void TakeDamage_Implementation(UDamage* Damage);

	// Given an incoming damage, calculates how much the character's health
	// should be reduced by.
	// Default implementation does Damage * Multiplier, and then multiplies
	// that by (1 - ResistStat / 100)
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintPure, Category = "Damage")
	float MitigateDamage(UDamageInstance* Instance) const;
	virtual float MitigateDamage_Implementation(UDamageInstance* Instance) const;

	// Determines the HP value at which the character changes from Unconscious to Dead.
	// Default implementation is 0, meaning the character goes straight from Alive to Dead.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintPure, Category = "Damage")
	float GetDeathThreshold() const;
	virtual float GetDeathThreshold_Implementation() const { return 0.0f; }

	/// RESOURCES ///

	// Set a resource to a new value, or add to the existing value. Setting
	// ClampToMaxResource will force the value to never go above MaxResource,
	// but going above may be desired in some cases. By default, Consciousness
	// will update every time the player's health changes, but special behavior
	// can be overridden for things like healing a dead character.
	UFUNCTION(BlueprintCallable, Category = "Stats")
	void SetCurrentResource(UResource* Resource, float Value, bool bClampToMaxResource = true, bool bUpdateConsciousness = true, EUpdateValueType UpdateType = EUpdateValueType::Set);

	// Get the current value of a resource.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Stats")
	float GetCurrentResource(UResource* Resource) const;

	// Get the max value for a resource without any modifiers applied.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Stats")
	float GetBaseMaxResource(UResource* Resource) const;

	// Permanently set a base max resource to a new value. If UpdateCurrentResource
	// is true, SetCurrentResource will be called with the same Value and UpdateType
	// supplied to this function.
	UFUNCTION(BlueprintCallable, Category = "Stats")
	void SetBaseMaxResource(UResource* Resource, float Value, EUpdateValueType UpdateType = EUpdateValueType::Add, bool bUpdateCurrentResource = true);

	// Get the maximum of a resource that a character can have. Be careful as
	// this function does not modify CurrentResources, meaning you could return
	// a MaxResource less than the current value. Be sure to update
	// CurrentResources when necessary.
	// Default implementation returns 100 for all resources.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintPure, Category = "Stats")
	float GetMaxResource(UResource* Resource) const;
	float GetMaxResource_Implementation(UResource* Resource) const;

	/// STATS ///

	// Get the value for a stat without any modifiers applied.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Stats")
	int32 GetBaseStat(UStat* Stat) const;

	// Get a stat with modifiers applied. Default modifiers include equipment,
	// status effects, and, if PartyMember is not null, party status effects.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintPure, Category = "Stats")
	int32 GetStat(UStat* Stat) const;
	virtual int32 GetStat_Implementation(UStat* Stat) const;

	// Permanently set a base stat to a new value.
	UFUNCTION(BlueprintCallable, Category = "Stats")
	void SetBaseStat(UStat* Stat, int32 Value, EUpdateValueType UpdateType = EUpdateValueType::Add);

	// Retrieve a list of all stats currently assigned a value in BaseStats.
	// This list can be used with GetBaseStat or GetStat as desired.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Stats")
	TArray<UStat*> GetAllStats() const { TArray<UStat*> Keys; BaseStats.GenerateKeyArray(Keys); return Keys; }

	/// SKILLS ///

	// Get the value for a skill without any modifiers applied.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Skills")
	const FCharacterSkillInfo& GetBaseSkill(USkill* Skill) const;
	FCharacterSkillInfo& GetBaseSkill(USkill* Skill);

	// Get a skill with modifiers applied. Default modifiers include equipment,
	// status effects, and, if PartyMember is not null, party status effects.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintPure, Category = "Skills")
	FCharacterSkillInfo GetSkill(USkill* Skill) const;
	virtual FCharacterSkillInfo GetSkill_Implementation(USkill* Skill) const;

	// Permanently set to a base skill level and mastery.
	UFUNCTION(BlueprintCallable, Category = "Skills")
	void SetBaseSkill(USkill* Skill, int32 Value, UMasteryLevel* Mastery);

	// Permanently set a base skill level to a new value.
	UFUNCTION(BlueprintCallable, Category = "Skills")
	void SetBaseSkillLevel(USkill* Skill, int32 Value, EUpdateValueType UpdateType = EUpdateValueType::Add);

	// Permanently set a base skill mastery to a new mastery level.
	// The supplied mastery level should usually be equal to the current
	// Mastery->IntVal + 1, but the function does not enforce this.
	UFUNCTION(BlueprintCallable, Category = "Skills")
	void SetBaseSkillMastery(USkill* Skill, UMasteryLevel* Mastery);

	// Returns true if the character's base skill level is greater than or equal
	// to the requested skill level. This can also be used to check whether the
	// skill has been learned at all - unlearned skills are level 0.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Skills")
	bool HasBaseSkillLevel(USkill* Skill, int32 Level) const;

	// Checks the skill's mastery level, and returns true if its IntVal is
	// greater than or equal to the requested mastery level's IntVal. If
	// the skill is unlearned, its IntVal will be -1.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Skills")
	bool HasBaseSkillMastery(USkill* Skill, UMasteryLevel* Mastery) const;

	// Retrieve a list of all skills currently assigned a value in BaseSkills.
	// This list can be used with GetBaseSkill or GetSkill as desired.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Stats")
	TArray<USkill*> GetAllSkills() const { TArray<USkill*> Keys; BaseSkills.GenerateKeyArray(Keys); return Keys; }

	/// STATUS ///

	// Append a new status effect to the character, or update an existing one.
	// Durations <= -1.0f will be considered permanent and will remain until
	// RemoveStatus() is explicitly called. Otherwise, Duration can be a unit of
	// your choosing, although the default recommendation is for it to be minutes
	// using the TimeManager system.
	// Strength is also an arbitrary assignment that the StatusEffect can use
	// however it wants.
	// Overwrite indicates what should happen if the character already has the
	// status effect, as only one status of each type can be applied at once.
	// AllowPartyStatus can be set to true if you also want to allow party-only
	// status effects to affect individual characters for some reason. If false,
	// this is strictly enforced.
	// The function returns true if the status effect was added or updated.
	UFUNCTION(BlueprintCallable, Category = "Status")
	bool AddStatusEffect(TSubclassOf<UStatusEffect> Status, float Duration = -1.0f, int32 Strength = 1,
						 EStatusOverwriteType Overwrite = EStatusOverwriteType::OverwriteIfStrengthAndDurationEqualOrHigher,
						 bool bAllowPartyStatus = false);

	// Retrieve a status effect, or null/None if not affected by the status.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Status")
	UStatusEffect* GetStatusEffect(TSubclassOf<UStatusEffect> Status) const;

	// Returns true if affected by the status, otherwise false.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Status")
	bool HasStatusEffect(TSubclassOf<UStatusEffect> Status) const;

	// Tick all status effects, causing each one to execute its own Tick function.
	// DeltaTime indicates the duration that should be subtracted from each status.
	// By default, it's recommended that this be equivalent to TimeManager's tick
	// value, which will also be called automatically if
	// TimeManager::TickStatusEffectsOnTimerTick is true. Any statuses whose
	// TimeRemaining is <= 0 and are not permanent are removed.
	// Note that statuses are ticked in their order in the StatusEffects array,
	// and it is possible that one status may cause another to be removed before
	// it is ticked.
	UFUNCTION(BlueprintCallable, Category = "Status")
	void TickStatusEffects(float DeltaTime);

	// Callback for the TimeManager which just calls TickStatusEffect(TimeAdvanced).
	UFUNCTION()
	void TickStatusEffectsFromTimeManager(UTimeManager* TimeManager, ETimeUnit TimeUnit, int32 TimeAdvanced);

	// Remove the status effect corresponding to the supplied subclass. If the
	// status doesn't exist, then this will return false. This function does not
	// care whether the status's TimeRemaining is <= 0 - it will always remove it.
	UFUNCTION(BlueprintCallable, Category = "Status")
	bool RemoveStatusEffectFromClass(TSubclassOf<UStatusEffect> Status);

	// Remove the status effect. If the status isn't for this character, then
	// this will return false. This function does not care whether the status's
	// TimeRemaining is <= 0 - it will always remove it.
	UFUNCTION(BlueprintCallable, Category = "Status")
	bool RemoveStatusEffect(UStatusEffect* Status);

	/// EQUIPMENT ///

	// Equip an item is the specified slot, unequipping the current item in
	// that slot, if there is one. Passing in null/None will unequip the
	// current item. The item returned by this function is either the item
	// that was previously equipped, or the item attempting to be equipped
	// if it can't be equipped, indicating that it failed.
	// The default implementation calls CanEquipItem, and if it succeeds, sets
	// the new item and updates InventoryWeight.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Inventory")
	UItem* EquipItem(UEquipSlot* Slot, UItem* Item);
	virtual UItem* EquipItem_Implementation(UEquipSlot* Slot, UItem* Item);

	// Whether an item can be equipped in the specified slot or not.
	// The default implementation makes sure the item has an EquippableComponent,
	// if the party member has the required skill to equip it, and if the item
	// can be equipped in the slot type.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintPure, Category = "Inventory")
	bool CanEquipItem(UEquipSlot* Slot, UItem* Item) const;
	virtual bool CanEquipItem_Implementation(UEquipSlot* Slot, UItem* Item) const;

	// Retrieives the equipped item for a particular slot. If no item is equipped,
	// null/None will be returned.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Inventory")
	UItem* GetEquippedItem(UEquipSlot* Slot) const;
	
/// MEMBER VARIABLES ///
protected:
	// How much of a resource the character has.
	UPROPERTY()
	TMap<UResource*, float> CurrentResources;

	// How much of a resource the character has can hold before modifiers.
	// Use GetBaseMaxResource or GetMaxResource.
	UPROPERTY()
	TMap<UResource*, float> BaseMaxResources;

	// Base stat values before any modifiers are applied. Use GetBaseStat or GetStat.
	UPROPERTY()
	TMap<UStat*, int32> BaseStats;

	// Base skill values before any modifiers are applied. Use GetBaseSkill or GetSkill.
	UPROPERTY()
	TMap<USkill*, FCharacterSkillInfo> BaseSkills;

	// The character's level. The player's party members may level up, while
	// enemies only have a fixed level. Experience is handled in PartyMember.
	UPROPERTY(BlueprintReadWrite, BlueprintGetter = GetLevel, Category = "Level")
	int32 Level = 1;

	// The resource used to control changes to Consciousness. If not specified,
	// DefaultHealthResource in BlobberConfiguration will be used.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Stats")
	UResource* HealthResource = nullptr;

	// The character's current physical state, based off of CurrentHealth
	// and GetDeathThreshold(). Automatically updated when SetCurrentHealth()
	// is called. If the character becomes dead, any status effects marked with
	// RemoveOnDeath will be automatically removed.
	UPROPERTY(BlueprintSetter = SetConsciousness, BlueprintGetter = GetConsciousness, Category = "Status")
	EConsciousness Consciousness = EConsciousness::Alive;

	// Gets all current status effects on the character. Be careful as modifying
	// values directly on the status itself will not trigger any events from
	// the CharacterSheet. Consider using AddStatusEffect with AlwaysOverwrite
	// if you need to update values.
	UPROPERTY(BlueprintGetter = GetStatusEffects, Category = "Status")
	TArray<UStatusEffect*> StatusEffects;

	// All equipment worn by the party member. This map is dynamically
	// populated when requested, so a slot key will not exist until the first
	// item is equipped in that slot. Therefore, if an item slot is empty, this
	// map will either not have that slot as a key, or the value will be null/None.
	// Use GetEquippedItem to access the equipped item, if there is one, and
	// EquipItem to equip or unequip items.
	UPROPERTY(BlueprintGetter = GetEquipment, Category = "Inventory")
	TMap<UEquipSlot*, UItem*> Equipment;

private:
	// All items held by the character.
	UPROPERTY(BlueprintGetter = GetInventory, Category = "Inventory")
	UInventory* Inventory = nullptr;

	// Stores the PartyMember that owns this CharacterSheet, or returns
	// null/None if this is an NPC.
	TWeakObjectPtr<UPartyMember> PartyMember = nullptr;

/// GETTERS AND SETTERS ///
public:
	UFUNCTION(BlueprintGetter)
	int32 GetLevel() const { return Level; }

	// Stores the PartyMember that owns this CharacterSheet, or returns
	// null/None if this is an NPC.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Party Member")
	UPartyMember* GetPartyMember() const { return PartyMember.Get(); }

	UFUNCTION(BlueprintGetter)
	EConsciousness GetConsciousness() const { return Consciousness; }

	UFUNCTION(BlueprintSetter)
	void SetConsciousness(EConsciousness NewConsciousness);

	UFUNCTION(BlueprintGetter)
	const TArray<UStatusEffect*>& GetStatusEffects() const { return StatusEffects; }

	UFUNCTION(BlueprintGetter)
	TMap<UEquipSlot*, UItem*>& GetEquipment() { return Equipment; }
	const TMap<UEquipSlot*, UItem*>& GetEquipment() const { return Equipment; }

	UFUNCTION(BlueprintGetter)
	UInventory* GetInventory() const { return Inventory; }

	// Increase level by 1. All bonuses should be assigned in PartyMember - this
	// is merely a way to increase the numerical value of Level.
	UFUNCTION(BlueprintCallable, Category = "Level")
	void LevelUp() { ++Level; }

/// DELEGATES ///
public:
	// A Resource's current value changed.
	UPROPERTY(BlueprintAssignable)
	FResourceChangedDelegate ResourceChangedDelegate;
	// A Resource's base maximum value changed.
	UPROPERTY(BlueprintAssignable)
	FBaseMaxResourceChangedDelegate BaseMaxResourceChangedDelegate;
	// A base Stat value was changed.
	UPROPERTY(BlueprintAssignable)
	FBaseStatChangedDelegate BaseStatChangedDelegate;
	// A base Skill level was changed.
	UPROPERTY(BlueprintAssignable)
	FBaseSkillLevelChangedDelegate BaseSkillLevelChangedDelegate;
	// A base Skill mastery was changed.
	UPROPERTY(BlueprintAssignable)
	FBaseSkillMasteryChangedDelegate BaseSkillMasteryChangedDelegate;
	// Consciousness state changed.
	UPROPERTY(BlueprintAssignable)
	FConsciousnessChangedDelegate ConsciousnessChangedDelegate;
	// A status was added.
	UPROPERTY(BlueprintAssignable)
	FStatusBeginDelegate StatusBeginDelegate;
	// A status's time remaining or strength was updated.
	UPROPERTY(BlueprintAssignable)
	FStatusUpdatedDelegate StatusUpdatedDelegate;
	// A status is about to be removed.
	UPROPERTY(BlueprintAssignable)
	FStatusEndDelegate StatusEndDelegate;
	// An item was equipped or unequipped.
	UPROPERTY(BlueprintAssignable)
	FEquipmentChangedDelegate EquipmentChangedDelegate;
};
