// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/BaseCharacter.h"
#include "NPCCharacter.generated.h"

class UCharacterSheet;

/**
 * 
 */
UCLASS()
class BLOBBERBASE_API ANPCCharacter : public ABaseCharacter
{
	GENERATED_BODY()
	
public:
	ANPCCharacter(const FObjectInitializer& Initializer);
	
protected:
	UPROPERTY()
	UCharacterSheet* PartyMembers;
};
