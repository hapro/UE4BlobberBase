// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Util/Enums.h"
#include "Util/Structs.h"
#include "Party.generated.h"

class UCharacterSheet;
class UPartyMember;
class USkill;
class UStatusEffect;
class UTimeManager;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FGoldChangedDelegate, UParty*, Party, int32, Gold);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FActivePartyMemberIndexChangedDelegate, UParty*, Party, int32, OldIndex, int32, NewIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPartyStatusBeginDelegate, UParty*, Party, UStatusEffect*, Status);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPartyStatusUpdatedDelegate, UParty*, Party, UStatusEffect*, Status);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPartyStatusEndDelegate, UParty*, Party, UStatusEffect*, Status);

// Non-actor representation of the player. Stores all non-actor data including
// references to all party members, plus any data that may apply to all party
// members. This class is essentially a singleton as only the player has a
// Party and the same Party object is reused in the event of loading a game or
// starting a new game.
UCLASS(BlueprintType, Blueprintable)
class BLOBBERBASE_API UParty : public UObject
{
	GENERATED_BODY()
	
public:
	UParty();

	/// INITIALIZATION ///

	virtual void InitPrivate();

	// Called after all PartyMembers have finished initializing.
	// Any additional setup should be done here.
	UFUNCTION(BlueprintNativeEvent)
	void Init();
	virtual void Init_Implementation() {}

	/// GOLD ///

	// Add or remove gold from the player. If removing more than the player
	// has, it will clamp at 0 without warning. Use HasGold first to make
	// sure the player has enough.
	UFUNCTION(BlueprintCallable, Category = "Gold")
	void SetGold(int32 Amount, EUpdateValueType UpdateType = EUpdateValueType::Add);

	// Returns true if the player has at least Amount of gold.
	UFUNCTION(BlueprintCallable, Category = "Gold")
	bool HasGold(int32 Amount) { return Gold >= Amount; }

	/// EXPERIENCE ///
	// Give the entire party some experience, and then level up each if possible.
	// See PartyMember::GiveExperience for more information. SplitExperience will
	// cause Amount to be divided by the number of party members before giving
	// each member their experience, while GiveIfUnconscious determines whether
	// unconscious/dead people recieve exp (and whether they are factored into
	// the split divisor).
	UFUNCTION(BlueprintCallable, Category = "Level")
	void GiveExperience(int32 Amount, bool bSplitExperience = false, bool bGiveIfUnconscious = false);

	/// SKILLS ///

	// Gets the skill value for the ActivePartyMember, unless it is a party skill,
	// in which case all party members are examined and the most skilled one is
	// chosen. "Most Skilled" means the highest mastery level with skill level
	// used as the tiebreaker. Party skills can be ignored (meaning the
	// ActivePartyMember is always used) by setting IncludePartySkills to false.
	UFUNCTION(BlueprintCallable, Category = "Skills")
	FCharacterSkillInfo GetActivePartyMemberSkill(USkill* Skill, bool bIncludePartySkills = true) const;

	/// STATUS ///

	// Append a new status effect to the character, or update an existing one.
	// Attempting to add a non-party status will fail.
	// Durations <= -1.0f will be considered permanent and will remain until
	// RemoveStatus() is explicitly called. Otherwise, Duration can be a unit of
	// your choosing, although the default recommendation is for it to be minutes
	// using the TimeManager system.
	// Strength is also an arbitrary assignment that the StatusEffect can use
	// however it wants.
	// Overwrite indicates what should happen if the party already has the
	// status effect, as only one status of each type can be applied at once.
	// The function returns true if the status effect was added or updated.
	UFUNCTION(BlueprintCallable, Category = "Status")
	bool AddStatusEffect(TSubclassOf<UStatusEffect> Status, float Duration = -1.0f, int32 Strength = 1,
						 EStatusOverwriteType Overwrite = EStatusOverwriteType::OverwriteIfStrengthAndDurationEqualOrHigher);

	// Retrieve a status effect, or null/None if not affected by the status.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Status")
	UStatusEffect* GetStatusEffect(TSubclassOf<UStatusEffect> Status) const;

	// Returns true if affected by the status, otherwise false.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Status")
	bool HasStatusEffect(TSubclassOf<UStatusEffect> Status) const;

	// Tick all status effects, causing each one to execute its own Tick function.
	// DeltaTime indicates the duration that should be subtracted from each status.
	// By default, it's recommended that this be equivalent to TimeManager's tick
	// value, which will also be called automatically if
	// TimeManager::TickStatusEffectsOnTimerTick is true. Any statuses whose
	// TimeRemaining is <= 0 and are not permanent are removed.
	// Note that statuses are ticked in their order in the StatusEffects array,
	// and it is possible that one status may cause another to be removed before
	// it is ticked.
	UFUNCTION(BlueprintCallable, Category = "Status")
	void TickStatusEffects(float DeltaTime);

	// Callback for the TimeManager which just calls TickStatusEffect(TimeAdvanced).
	UFUNCTION()
	void TickStatusEffectsFromTimeManager(UTimeManager* TimeManager, ETimeUnit TimeUnit, int32 TimeAdvanced);

	// Remove the status effect corresponding to the supplied subclass. If the
	// status doesn't exist, then this will return false. This function does not
	// care whether the status's TimeRemaining is <= 0 - it will always remove it.
	UFUNCTION(BlueprintCallable, Category = "Status")
	bool RemoveStatusEffectFromClass(TSubclassOf<UStatusEffect> Status);

	// Remove the status effect. If the status isn't for the party, then
	// this will return false. This function does not care whether the status's
	// TimeRemaining is <= 0 - it will always remove it.
	UFUNCTION(BlueprintCallable, Category = "Status")
	bool RemoveStatusEffect(UStatusEffect* Status);
	
/// MEMBER VARIABLES ///
protected:
	// The currently-selected party member. When performing actions such as
	// attacking or casting spells, this is the party member who will take the
	// action.
	UPROPERTY(BlueprintGetter = GetActivePartyMemberIndex, BlueprintSetter = SetActivePartyMemberIndex, Category = "Party Member")
	int32 ActivePartyMemberIndex = 0;

	// Amount of gold the player has.
	UPROPERTY(BlueprintGetter = GetGold, Category = "Gold")
	int32 Gold = 0;

	// Gets all current status effects on the character. Be careful as modifying
	// values directly on the status itself will not trigger any events from
	// the CharacterSheet. Consider using AddStatusEffect with AlwaysOverwrite
	// if you need to update values.
	UPROPERTY(BlueprintGetter = GetStatusEffects, Category = "Status")
	TArray<UStatusEffect*> StatusEffects;

private:
	// List of all party members controlled by the player.
	UPROPERTY(BlueprintGetter = GetPartyMembers, Category = "Party Member")
	TArray<UPartyMember*> PartyMembers;

/// GETTERS AND SETTERS ///
public:
	UFUNCTION(BlueprintGetter)
	const TArray<UPartyMember*>& GetPartyMembers() const { return PartyMembers; }

	UFUNCTION(BlueprintSetter)
	void SetActivePartyMemberIndex(int32 PartyMember);

	UFUNCTION(BlueprintGetter)
	int32 GetActivePartyMemberIndex() const { return ActivePartyMemberIndex; }

	// Returns the party member corresponding to ActivePartyMemberIndex.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Party Member")
	UPartyMember* GetActivePartyMember() const { return PartyMembers[ActivePartyMemberIndex]; }

	// Returns the array index of the given PartyMember, or -1 if not found.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Party Member")
	int32 GetIndexOfPartyMember(UPartyMember* PartyMember) const;

	UFUNCTION(BlueprintGetter)
	int32 GetGold() const { return Gold; }

	UFUNCTION(BlueprintGetter)
	const TArray<UStatusEffect*>& GetStatusEffects() const { return StatusEffects; }

/// DELEGATES ///
public:
	// Gold was added or removed.
	UPROPERTY(BlueprintAssignable)
	FGoldChangedDelegate GoldChangedDelegate;
	// The active party member changed.
	UPROPERTY(BlueprintAssignable)
	FActivePartyMemberIndexChangedDelegate ActivePartyMemberIndexChangedDelegate;
	// A status was added.
	UPROPERTY(BlueprintAssignable)
	FPartyStatusBeginDelegate PartyStatusBeginDelegate;
	// A status's time remaining or strength was updated.
	UPROPERTY(BlueprintAssignable)
	FPartyStatusUpdatedDelegate PartyStatusUpdatedDelegate;
	// A status is about to be removed.
	UPROPERTY(BlueprintAssignable)
	FPartyStatusEndDelegate PartyStatusEndDelegate;
};
