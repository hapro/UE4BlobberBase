// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ItemComponent.generated.h"

class UCharacterSheet;
class UDamage;
class UDamageElement;
class UDamageStyle;
class UEquipItemType;
class UEquipSlotType;
class UItem;
class UPartyMember;
class USkill;
class UStat;

// Base Item component class, which stores a reference to the item it is a
// component for. This should be subclassed to provide items with various
// functionalities such as potion effects when drinking, damage for weapons,
// etc.
UCLASS(abstract, DefaultToInstanced, EditInlineNew, BlueprintType, Blueprintable)
class BLOBBERBASE_API UItemComponent : public UObject
{
	GENERATED_BODY()
	
public:
	UItemComponent();

	// The item that this object is a component of.
	UPROPERTY(BlueprintReadOnly, Category = "Item")
	UItem* Item = nullptr;
};

// A component indicating that the item can be consumed by a character.
UCLASS(abstract)
class BLOBBERBASE_API UConsumableComponent : public UItemComponent
{
	GENERATED_BODY()

public:
	UConsumableComponent();

	// Helper function to get the component of an item automatically cased.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Item Components")
	static UConsumableComponent* GetConsumableComponent(UItem* ConsumableItem);

	// Whether or not this item can be consumed by a character. True by default.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Consume")
	bool CanConsume(UCharacterSheet* CharacterSheet) const;
	virtual bool CanConsume_Implementation(UCharacterSheet* CharacterSheet) const { return true; }

	// Causes the character to consume the item and trigger some effect. This
	// function is not override-able - OnConsume should be implemented instead.
	// Note that this function does NOT call CanConsume, and assumes that the
	// caller has already checked that it is valid to consume.
	UFUNCTION(BlueprintCallable, Category = "Consume")
	void Consume(UCharacterSheet* CharacterSheet);

	// Specific implementation details for the item being consumed go here. For
	// instance, gaining health when drinking a potion.
	UFUNCTION(BlueprintNativeEvent)
	void OnConsume(UCharacterSheet* CharacterSheet);
	virtual void OnConsume_Implementation(UCharacterSheet* CharacterSheet) {}
};

// A component indicating that the item can be worn as equipment by a character.
UCLASS()
class BLOBBERBASE_API UEquippableComponent : public UItemComponent
{
	GENERATED_BODY()

public:
	UEquippableComponent();

	// Helper function to get the component of an item automatically cased.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Item Components")
	static UEquippableComponent* GetEquippableComponent(UItem* EquippableItem);

	// Overrideable callback for when this item is equipped.
	UFUNCTION(BlueprintNativeEvent)
	void OnEquip(UCharacterSheet* CharacterSheet);
	virtual void OnEquip_Implementation(UCharacterSheet* CharacterSheet) {}

	// Overrideable callback for when this item is unequipped.
	UFUNCTION(BlueprintNativeEvent)
	void OnUnequip(UCharacterSheet* CharacterSheet);
	virtual void OnUnequip_Implementation(UCharacterSheet* CharacterSheet) {}

/// MEMBER VARIABLES ///
public:
	// What type of item it is, eg Sword, Helmet, Ring
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Equippable")
	UEquipItemType* ItemType = nullptr;

	// Any stats that should be modified (when calling GetStat) while this item
	// is equipped.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Equippable")
	TMap<UStat*, int32> StatModifiers;

	// Any skills that should be modified (when calling GetSkill) while this item
	// is equipped.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Equippable")
	TMap<USkill*, int32> SkillModifiers;
};

// A component that tracks how much damage an item can do.
UCLASS()
class BLOBBERBASE_API UDamageComponent : public UItemComponent
{
	GENERATED_BODY()

public:
	UDamageComponent();

	// Helper function to get the component of an item automatically cased.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Item Components")
	static UDamageComponent* GetDamageComponent(UItem* DamageItem);

	// Generate damage instances for this item, used by a particular CharacterSheet.
	// Damage is assumed to be initialized at this point, and AddDamageInstance
	// should be called for every instance generated within this function. The
	// default implementation creates a default DamageInstance with GetDamage.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Damage")
	void GenerateDamageInstances(UDamage* Damage, UDamageStyle* Style, UCharacterSheet* CharacterSheet);
	virtual void GenerateDamageInstances_Implementation(UDamage* Damage, UDamageStyle* Style, UCharacterSheet* CharacterSheet);

	// Retrieve a damage value for this item, for instance when a melee attack
	// is performed. This function does not have to return the same value every
	// time it is called. The default implementation returns BaseDamage.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintPure, Category = "Damage")
	float GetDamage() const;
	virtual float GetDamage_Implementation() const { return BaseDamage; }

/// MEMBER VARIABLES ///
public:
	// Base damage for the item, although this can be re-used or extended for
	// any purpose. To implement dice rolls, store 2 extra ints for NumDice and
	// DiceSides, with BaseDamage being a constant addition to the dice roll.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
	int32 BaseDamage = 1;

	// The element to use for GenerateDamageInstance. Element can be stored
	// elsewhere if GenerateDamageInstances is overridden, or a second
	// DamageElement could be added, etc.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
	UDamageElement* Element = nullptr;
};
