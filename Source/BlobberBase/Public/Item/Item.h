// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Item/ItemComponent.h"
#include "Item.generated.h"

// Base item class for everything from equippables to consumables to quest items.
// Subclasses should be made and initialized with some default components and
// customized as needed.
UCLASS(BlueprintType, Blueprintable)
class BLOBBERBASE_API UItem : public UObject
{
	GENERATED_BODY()
	
public:
	UItem();
	virtual void PostInitProperties() override;

	UFUNCTION(BlueprintGetter)
	FString GetItemName() const { return ItemName; }

	UFUNCTION(BlueprintGetter)
	FText GetItemDisplayName() const { return ItemDisplayName.IsEmpty() ? FText::FromString(ItemName) : ItemDisplayName; }

	/// PRICES ///

	// Base cost to buy this item. May still be modified by player skills, etc. later.
	// Default: 1
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Item")
	int32 GetBuyPrice() const;
	virtual int32 GetBuyPrice_Implementation() const { return 1; }

	// Base cost to sell this item. May still be modified by player skills, etc. later.
	// Default: BuyPrice / 2
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Item")
	int32 GetSellPrice() const;
	virtual int32 GetSellPrice_Implementation() const { return GetBuyPrice() / 2; }

	// Whether or not this item can be sold.
	// Default: SellPrice > 0
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Item")
	bool CanSell() const;
	virtual bool CanSell_Implementation() const { return GetSellPrice() > 0; }

	// Base cost to identify this item. May still be modified by player skills, etc. later.
	// Default: BuyPrice / 10, or 0 if already ID'd
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Item")
	int32 GetIDPrice() const;
	virtual int32 GetIDPrice_Implementation() const;

	/// COMPONENTS ///

	// Get the component at an index in the array.
	UFUNCTION(BlueprintCallable, Category = "Components")
	UItemComponent* GetComponentAt(int32 Index) const;

	// Get the first component that matches the requested class or a subclass of it.
	UFUNCTION(BlueprintCallable, Category = "Components")
	UItemComponent* GetComponent(TSubclassOf<UItemComponent> ComponentClass) const;

	// Get all components that match the requested class or a subclass of it.
	UFUNCTION(BlueprintCallable, Category = "Components")
	TArray<UItemComponent*> GetComponents(TSubclassOf<UItemComponent> ComponentClass) const;

	// Add a new component of the specified class.
	UFUNCTION(BlueprintCallable, Category = "Components")
	UItemComponent* AddComponent(TSubclassOf<UItemComponent> ComponentClass);

	// Remove a component of the specified class or a subclass of it.
	UFUNCTION(BlueprintCallable, Category = "Components")
	bool RemoveComponentOfType(TSubclassOf<UItemComponent> ComponentClass);

	// Remove all components of the specified class or a subclass of it.
	UFUNCTION(BlueprintCallable, Category = "Components")
	bool RemoveAllComponentsOfType(TSubclassOf<UItemComponent> ComponentClass);

	// Remove a specific component.
	UFUNCTION(BlueprintCallable, Category = "Components")
	bool RemoveComponent(UItemComponent* Component);

	// Remove the component at a specific index in the array.
	UFUNCTION(BlueprintCallable, Category = "Components")
	bool RemoveComponentAt(int32 Index);

	// Checks for a component matching the requested class or a subclass of it.
	UFUNCTION(BlueprintCallable, Category = "Components")
	bool HasComponent(TSubclassOf<UItemComponent> ComponentClass) const;

/// MEMBER VARIABLES
public:
	// The description of the asset to display to users.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (MultiLine = true))
	FText Description;

	// Whether or not this Item is identified. Unidentified items should function
	// normally, but their attributes hidden when examined.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
	bool bIsIdentified = false;

	// How much the item weighs.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
	float Weight = 0.0f;

	// The name of the item to use for code comparisons.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, BlueprintGetter = GetItemName)
	FString ItemName;

	// The name of the item to display to users.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, BlueprintGetter = GetItemDisplayName)
	FText ItemDisplayName;

protected:
	// A list of all the components attached to this item.
	UPROPERTY(EditAnywhere, Instanced, Category = "Components")
	TArray<UItemComponent*> Components;
};
