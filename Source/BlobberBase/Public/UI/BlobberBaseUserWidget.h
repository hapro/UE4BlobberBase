// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Util/Enums.h"
#include "BlobberBaseUserWidget.generated.h"

class UCharacterClass;
class UCharacterSheet;
class UEquipSlot;
class UInventory;
class UItem;
class UMasteryLevel;
class UParty;
class UResource;
class USkill;
class UStat;
class UStatusEffect;
class UTimeManager;

/**
 * 
 */
UCLASS()
class BLOBBERBASE_API UBlobberBaseUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	virtual void NativeConstruct() override;

	/// Time ///

	UPROPERTY(EditDefaultsOnly, Category = "Events")
	bool bAutoconnectTimeManagerEvents = true;

	UFUNCTION(BlueprintCallable, Category = "Events")
	void ConnectTimeManagerEvents(UTimeManager* TimeManager);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Time")
	void OnTimeChanged(UTimeManager* TimeManager);
	virtual void OnTimeChanged_Implementation(UTimeManager* TimeManager) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Time")
	void OnTimerTicked(UTimeManager* TimeManager, ETimeUnit TimeUnit, int32 TimeAdvanced);
	virtual void OnTimerTicked_Implementation(UTimeManager* TimeManager, ETimeUnit TimeUnit, int32 TimeAdvanced) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Time")
	void OnTimeUnitAdvanced(UTimeManager* TimeManager, ETimeUnit TimeUnit, int32 OldValue, int32 NewValue);
	virtual void OnTimeUnitAdvanced_Implementation(UTimeManager* TimeManager, ETimeUnit TimeUnit, int32 OldValue, int32 NewValue) {}

	/// Party ///

	UPROPERTY(EditDefaultsOnly, Category = "Events")
	bool bAutoconnectPlayerPartyEvents = true;

	UFUNCTION(BlueprintCallable, Category = "Events")
	void ConnectPartyEvents(UParty* Party);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Party")
	void OnGoldChanged(UParty* Party, int32 Gold);
	virtual void OnGoldChanged_Implementation(UParty* Party, int32 Gold) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Party")
	void OnActivePartyMemberIndexChanged(UParty* Party, int32 OldIndex, int32 NewIndex);
	virtual void OnActivePartyMemberIndexChanged_Implementation(UParty* Party, int32 OldIndex, int32 NewIndex) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Party")
	void OnPartyStatusBegin(UParty* Party, UStatusEffect* Status);
	virtual void OnPartyStatusBegin_Implementation(UParty* Party, UStatusEffect* Status) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Party")
	void OnPartyStatusUpdated(UParty* Party, UStatusEffect* Status);
	virtual void OnPartyStatusUpdated_Implementation(UParty* Party, UStatusEffect* Status) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Party")
	void OnPartyStatusEnd(UParty* Party, UStatusEffect* Status);
	virtual void OnPartyStatusEnd_Implementation(UParty* Party, UStatusEffect* Status) {}

	/// Party Member ///

	UPROPERTY(EditDefaultsOnly, Category = "Events")
	bool bAutoconnectPlayerPartyMemberEvents = true;

	UFUNCTION(BlueprintCallable, Category = "Events")
	void ConnectPartyMemberEvents(UPartyMember* PartyMember);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Party Member")
	void OnLevelUp(UPartyMember* PartyMember, int32 Level);
	virtual void OnLevelUp_Implementation(UPartyMember* PartyMember, int32 Level) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Party Member")
	void OnCharacterClassChanged(UPartyMember* PartyMember, UCharacterClass* CharacterClass);
	virtual void OnCharacterClassChanged_Implementation(UPartyMember* PartyMember, UCharacterClass* CharacterClass) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Party Member")
	void OnExperienceChanged(UPartyMember* PartyMember, int32 CurrentExperience);
	virtual void OnExperienceChanged_Implementation(UPartyMember* PartyMember, int32 CurrentExperience) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Party Member")
	void OnStatPointsChanged(UPartyMember* PartyMember, int32 CurrentStatPoints);
	virtual void OnStatPointsChanged_Implementation(UPartyMember* PartyMember, int32 CurrentStatPoints) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Party Member")
	void OnSkillPointsChanged(UPartyMember* PartyMember, int32 CurrentSkillPoints);
	virtual void OnSkillPointsChanged_Implementation(UPartyMember* PartyMember, int32 CurrentSkillPoints) {}

	/// Character Sheet ///

	UPROPERTY(EditDefaultsOnly, Category = "Events")
	bool bAutoconnectPlayerCharacterSheetEvents = true;

	UFUNCTION(BlueprintCallable, Category = "Events")
	void ConnectCharacterSheetEvents(UCharacterSheet* CharacterSheet);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Character Sheet")
	void OnResourceChanged(UCharacterSheet* CharacterSheet, UResource* Resource, float CurrentHealth);
	virtual void OnResourceChanged_Implementation(UCharacterSheet* CharacterSheet, UResource* Resource, float CurrentHealth) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Character Sheet")
	void OnBaseMaxResourceChanged(UCharacterSheet* CharacterSheet, UResource* Resource, float CurrentMana);
	virtual void OnBaseMaxResourceChanged_Implementation(UCharacterSheet* CharacterSheet, UResource* Resource, float CurrentMana) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Character Sheet")
	void OnBaseStatChanged(UCharacterSheet* CharacterSheet, UStat* Stat, int32 CurrentValue);
	virtual void OnBaseStatChanged_Implementation(UCharacterSheet* CharacterSheet, UStat* Stat, int32 CurrentValue) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Character Sheet")
	void OnBaseSkillLevelChanged(UCharacterSheet* CharacterSheet, USkill* Skill, int32 CurrentValue);
	virtual void OnBaseSkillLevelChanged_Implementation(UCharacterSheet* CharacterSheet, USkill* Skill, int32 CurrentValue) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Character Sheet")
	void OnBaseSkillMasteryChanged(UCharacterSheet* CharacterSheet, USkill* Skill, UMasteryLevel* CurrentMastery);
	virtual void OnBaseSkillMasteryChanged_Implementation(UCharacterSheet* CharacterSheet, USkill* Skill, UMasteryLevel* CurrentMastery) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Character Sheet")
	void OnStatusBegin(UCharacterSheet* CharacterSheet, UStatusEffect* Status);
	virtual void OnStatusBegin_Implementation(UCharacterSheet* CharacterSheet, UStatusEffect* Status) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Character Sheet")
	void OnStatusUpdated(UCharacterSheet* CharacterSheet, UStatusEffect* Status);
	virtual void OnStatusUpdated_Implementation(UCharacterSheet* CharacterSheet, UStatusEffect* Status) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Character Sheet")
	void OnStatusEnd(UCharacterSheet* CharacterSheet, UStatusEffect* Status);
	virtual void OnStatusEnd_Implementation(UCharacterSheet* CharacterSheet, UStatusEffect* Status) {}

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Character Sheet")
	void OnEquipmentChanged(UCharacterSheet* CharacterSheet, UEquipSlot* EquipSlot, UItem* OldItem, UItem* NewItem);
	virtual void OnEquipmentChanged_Implementation(UCharacterSheet* CharacterSheet, UEquipSlot* EquipSlot, UItem* OldItem, UItem* NewItem) {}

	/// Inventory ///

	UPROPERTY(EditDefaultsOnly, Category = "Events")
	bool bAutoconnectPlayerInventoryEvents = true;

	UFUNCTION(BlueprintCallable, Category = "Events")
	void ConnectInventoryEvents(UInventory* Inventory);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Inventory")
	void OnInventoryChanged(UInventory* Inventory);
	virtual void OnInventoryChanged_Implementation(UInventory* Inventory) {}

protected:
	bool bInitializedEvents = false;
};
